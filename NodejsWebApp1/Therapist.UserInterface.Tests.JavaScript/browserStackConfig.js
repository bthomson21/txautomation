﻿var browserstack = require('browserstack-local');

exports.config = {

    framework: 'jasmine2',
	seleniumAddress: 'http://hub-cloud.browserstack.com/wd/hub',

    capabilities: {
        'browserstask.local':true,
     'browserstack.user': 'benthomson4',
    'browserstack.key': 'Q9yYBfHc6aTRJam7ZQbj',
	'browserstack.debug': 'true',
	'browserstack.networkLogs': 'true',
    'browserName': 'Chrome',
    'device': 'Samsung Galaxy S8',
        'realMobile': 'true',
        'os_version': '7.0'
  },

    specs: [//'./login/invalidLogin.js'
        //'./calendar/calendar.js', 
	    // './unsignedvisits/unsignedVisits.js', 
        // './schedule/schedule.js', 
        // './patientSearch/patientSearch.js',
		// './patientInformation/patientInformation.js',
	    // './patientAppointments/patientAppointments.js',
		// './patientCases/patientCases.js',
		// './visitDoc/visitDoc.js',
        // './caseDetails/caseDetails.js',
        //'./signOff/signedOffWithCoSigner.js',
        './template/template.js' 

    ]
};
