﻿'use strict'

const EC = require('../expectedConditions');

const commonFunctions = require('../common.js');
const commonCalendar = require('./commonCalendar.js');
const commonPatientSearch = require('../patientSearch/commonPatientSearch.js');
const commonPatientCases = require('../patientCases/commonPatientCases.js');
const commonCaseDetails = require('../caseDetails/commonCaseDetails.js');
const commonPatientInformation = require('../patientInformation/commonPatientInformation.js');
const commonPatientAppointments = require('../patientAppointments/commonPatientAppointments.js');
const commonUnsignedVisits = require('../unsignedVisits/commonUnsignedVisits.js');
const commonVisitDoc = require('../visitDoc/commonVisitDoc.js');

commonFunctions.xmlOutputLogger('calendar');

describe('Calendar', function () {
    /*TODO: Verify Calendar drop down content
    */

    it('Enter Schedule View to initial Calendar icon', async function () {
        await commonFunctions.validLogin();
        await browser.wait(EC.elementToBeClickable($(commonCalendar.elements.calendarIconLocationClass)), 5000, 'Failed to enter to Schedule view with Calendar icon visible and browser timed out');
        since("Calendar icon is not present in the Schedule view").expect(await $(commonCalendar.elements.calendarIconLocationClass).isPresent()).toBeTruthy();
    }),

    it('Open Visit Doc using Calendar icon from Schedule view (TX-740, TX-248)', async function () {
        await commonCalendar.openArriveAppointmentFromCalendarIcon("Schedule view");
        await commonFunctions.waitForActivityIndicatorToClose("Visit Doc");
    }),

    it('Open Visit Doc using Calendar icon from Patient Search view', async function () {
        await commonPatientSearch.enterPatientSearchByMenu();
        await commonCalendar.openArriveAppointmentFromCalendarIcon("Patient Search view");
    }),

    it('Open Visit Doc using Calendar icon from Patient Information view', async function () {
        await commonPatientSearch.enterPatientSearchByIcon();
        await commonPatientInformation.enterPatientInformationFromPatientSearch();
        await commonCalendar.openArriveAppointmentFromCalendarIcon("Patient Information view");
    }),

    it('Open Visit Doc using Calendar icon from Patient Cases view (TX-805)', async function () {
        await commonPatientSearch.enterPatientSearchByIcon();
        await commonPatientInformation.enterPatientInformationFromPatientSearch();
        await commonPatientCases.enterPatientCasesFromPatientInformation();
        await commonCalendar.openArriveAppointmentFromCalendarIcon("Patient Cases view");
        await commonPatientInformation.closePatientTab(0);  //TODO: Remove when TX-804 is complete
    }),

    it('Open Visit Doc using Calendar icon from Patient appointment view', async function () {  
        await commonPatientSearch.enterPatientSearchByIcon();
        await commonPatientInformation.enterPatientInformationFromPatientSearch();
        await commonPatientAppointments.enterPatientAppointmentsFromPatientInformation();
        await commonCalendar.openArriveAppointmentFromCalendarIcon("Patient appointment view");
        await commonPatientInformation.closePatientTab(0);  //TODO: Remove when TX-804 is complete
    }),

    it('Open Visit Doc using Calendar icon from Unsigned Visits view', async function () {  
        await commonPatientSearch.enterPatientSearchByIcon();
        await commonUnsignedVisits.enterUnsignedVisitsByIcon();
        await commonCalendar.openArriveAppointmentFromCalendarIcon("Unsigned Visits view");
    }),

    it('Open Visit Doc using Calendar icon from Visit Doc view', async function () {             
        await commonPatientSearch.enterPatientSearchByIcon();
        await commonPatientInformation.enterPatientInformationFromPatientSearch(commonPatientCases.elements.patientFirstName);
        await commonPatientCases.enterPatientCasesFromPatientInformation();
        await commonPatientCases.selectLastCaseFirstArrivedVisit();
        await commonVisitDoc.clickTemplateCancel();
        await commonCalendar.openArriveAppointmentFromCalendarIcon("Visit Doc view");
    })
});