'use strict'

const EC = require('../expectedConditions');

const commonSchedule = require('../schedule/commonSchedule.js');
const commonFunctions = require('../common.js');

module.exports = {

    elements: {
        calendarIconLocationClass: "div.icon-calendar.topbar-calendar-icon",
        calendarPatientListClass: "div.patient_name.ng-binding"
    },

    openArriveAppointmentFromCalendarIcon: async function (calendarIconClickedFromView) {
        await browser.wait(EC.elementToBeClickable($(this.elements.calendarIconLocationClass)), 5000, 'Calendar icon is not clickable after entering ' + calendarIconClickedFromView + ' and browser timed out');
        await $(this.elements.calendarIconLocationClass).click();

        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(this.elements.calendarPatientListClass, commonSchedule.elements.arrivedAppointmentName))), 5000, 'Calendar did not expand on click of Calendar icon from ' + calendarIconClickedFromView + ' and browser timed out');
        await element(by.cssContainingText(this.elements.calendarPatientListClass, commonSchedule.elements.arrivedAppointmentName)).click();

        await browser.wait(EC.elementToBeClickable(element.all(by.cssContainingText("div.md-text.ng-binding", "NECK PAIN")).get(0)), 5000, 'Case Name drop down is not clickable after and browser timed out');
    }
};
