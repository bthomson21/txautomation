﻿'use strict';

const EC = require('../expectedConditions');

const commonFunctions = require('../common.js');
const commonCaseDetails = require('./commonCaseDetails.js');
const commonPatientCases = require('../patientCases/commonPatientCases.js');
const commonPatientSearch = require('../patientSearch/commonPatientSearch.js');
const commonPatientInformation = require('../patientInformation/commonPatientInformation.js');
const commonVisitDoc = require('../visitDoc/commonVisitDoc.js');

commonFunctions.xmlOutputLogger('caseDetails');

describe('Case Details', function () {

    it('Enter Case Details from Visit Doc', async function () {
        await commonFunctions.validLogin();
        await commonPatientSearch.enterPatientSearchByMenu();
        await commonPatientInformation.enterPatientInformationFromPatientSearch(commonPatientCases.elements.patientFirstName);
        await commonPatientCases.enterPatientCasesFromPatientInformation();
        await commonPatientCases.selectLastCaseFirstArrivedVisit();
        await commonVisitDoc.clickTemplateCancel();
        await commonCaseDetails.enterCaseDetailsFromVisitDoc();
    }),

    it('Verify Referral section', async function () {
        since("In the Referral section, the Referral Source is not set to " + commonCaseDetails.referral.referralSource).expect(element.all(by.cssContainingText("div.md-text.ng-binding", commonCaseDetails.referral.referralSource)).get(0).isDisplayed()).toBeTruthy();
        
        let value = await $(commonCaseDetails.elements.referralNameFieldLabel).$(commonCaseDetails.elements.ngClass).getAttribute("value");
        since("Expecting Referral Name to be " + commonCaseDetails.referral.referralName + " but displayed is " + value).expect(commonCaseDetails.referral.referralName).toEqual(value);

        value = await $(commonCaseDetails.elements.numOfVisitsFieldLabel).$(commonCaseDetails.elements.ngClass).getAttribute("value");
        since("Expecting # of Visits to be " + commonCaseDetails.referral.numOfVisits + " but displayed is " + value).expect(commonCaseDetails.referral.numOfVisits).toEqual(value);

        value = await $$(commonCaseDetails.elements.effectiveDateFieldLabel).first().$(commonCaseDetails.elements.datePickerClass).getAttribute("value");
        since("Expecting Effective Date to be " + commonCaseDetails.referral.effectiveDate + " but displayed is " + value).expect(commonCaseDetails.referral.effectiveDate).toEqual(value);

        value = await $(commonCaseDetails.elements.expirationDateFieldLabel).$(commonCaseDetails.elements.datePickerClass).getAttribute("value");
        since("Expecting Expiration Date to be " + commonCaseDetails.referral.experationDate + " but displayed is " + value).expect(commonCaseDetails.referral.experationDate).toEqual(value);
    }),

    it('Verify Plan of Care section', async function () {

        let pocCount = await element.all(by.repeater('poc in d.summaries | orderBy:\'$poc.effectiveDate\' track by $index')).count();
        since("Expecting " + commonCaseDetails.planOfCare.numPlanOfCare + " Plan of Care items, but displayed " + pocCount).expect(commonCaseDetails.planOfCare.numPlanOfCare).toEqual(pocCount.toString());

        let value = await element.all(by.repeater('poc in d.summaries | orderBy:\'$poc.effectiveDate\' track by $index')).first().all(by.tagName('div')).first().$("[class=\"md-datepicker-input md-input\"]").getAttribute("value");
        since("Expecting Effective Date to be " + commonCaseDetails.planOfCare.firstEffectiveDate + " but displayed is " + value).expect(commonCaseDetails.planOfCare.firstEffectiveDate).toEqual(value);
    }),

    it('Verify Drop Down menu changes Cases', async function () {
        await $(commonCaseDetails.elements.caseNameMainDropDown).click();
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(commonFunctions.elements.ngRepeater, commonCaseDetails.elements.firstCaseName))), 5000, 'Case Names drop down menu did not expand and browser timed out');
        await element(by.cssContainingText(commonFunctions.elements.ngRepeater, commonCaseDetails.elements.firstCaseName)).click();

        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText('div.clnt-diagnosis.ng-binding.ng-scope', 'A01.04 - Typhoid arthritis'))), 5000, 'Changing the Case Name did not change the content of the Case Details page and browser timed out');
        since('Changing the Case Name did not change the content of the Case Details page').expect(element(by.cssContainingText('div.clnt-diagnosis.ng-binding.ng-scope', 'A01.04 - Typhoid arthritis')).isPresent()).toBeTruthy();

        await $(commonCaseDetails.elements.caseNameMainDropDown).click();
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(commonFunctions.elements.ngRepeater, commonCaseDetails.elements.thirdCaseName))), 5000, 'Case Names drop down menu did not expand to revert and browser timed out');
        await element(by.cssContainingText(commonFunctions.elements.ngRepeater, commonCaseDetails.elements.thirdCaseName)).click();

        await browser.wait(EC.presenceOf(element(by.cssContainingText(commonCaseDetails.elements.visitLocation, commonCaseDetails.elements.thirdCaseNameFirstVisitDoc))), 5000, 'Failed to change Case Name back to original selection and browser timed out');
    }),

    it('Verify drop down menu displays correct visit doc (TX-227)', async function () {
        await commonPatientCases.enterPatientInformationWithReturnIcon();
        await commonPatientCases.enterPatientCasesFromPatientInformation();
        await element(by.cssContainingText(commonCaseDetails.elements.visitLocation, commonCaseDetails.elements.thirdCaseNameFirstVisitDoc)).click();
        await commonVisitDoc.clickTemplateCancel();

        await commonPatientCases.enterPatientInformationWithReturnIcon();
        await commonPatientCases.enterPatientCasesFromPatientInformation();
        await element(by.cssContainingText(commonCaseDetails.elements.visitLocation, commonCaseDetails.elements.fourthCaseNameFirstVisitDoc)).click();
        await commonVisitDoc.clickTemplateCancel();


        await $(commonCaseDetails.elements.caseNameMainDropDown).click();
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(commonFunctions.elements.ngRepeater, commonCaseDetails.elements.thirdCaseName))), 5000, 'Case Names drop down menu did not expand, select third Case and browser timed out');
        await element(by.cssContainingText(commonFunctions.elements.ngRepeater, commonCaseDetails.elements.thirdCaseName)).click();
        
        since("Changing Case Name, " + commonCaseDetails.elements.thirdCaseName + ", did not display the correct Visit Doc " + commonCaseDetails.elements.thirdCaseNameFirstVisitDoc)
            .expect(element(by.cssContainingText(commonCaseDetails.elements.visitLocation, commonCaseDetails.elements.thirdCaseNameFirstVisitDoc)).isPresent()).toBe(true);
        
        await $(commonCaseDetails.elements.caseNameMainDropDown).click();
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(commonFunctions.elements.ngRepeater, commonCaseDetails.elements.fourthCaseName))), 5000, 'Case Names drop down menu did not expand, select fourth Case and browser timed out');
        await element(by.cssContainingText(commonFunctions.elements.ngRepeater, commonCaseDetails.elements.fourthCaseName)).click();

        since("Changing Case Name, " + commonCaseDetails.elements.fourthCaseName + ", did not display the correct Visit Doc " + commonCaseDetails.elements.fourthCaseNameFirstVisitDoc)
            .expect(element(by.cssContainingText(commonCaseDetails.elements.visitLocation, commonCaseDetails.elements.fourthCaseNameFirstVisitDoc)).isPresent()).toBe(true);

        await $(commonCaseDetails.elements.caseNameMainDropDown).click();
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(commonFunctions.elements.ngRepeater, commonCaseDetails.elements.thirdCaseName))), 5000, 'Case Names drop down menu did not expand, select third and browser timed out');
        await element(by.cssContainingText(commonFunctions.elements.ngRepeater, commonCaseDetails.elements.thirdCaseName)).click();
    }),

    it('Verify Cancel/No/Save in the Save Your Changes Pop Up (TX-181)', async function () {
        let originalOuterText = await $(commonCaseDetails.elements.facilityFieldLabel).getAttribute("outerText");

        const originalFacilityArray = originalOuterText.split(/[\n]/);
        const originalFacility = originalFacilityArray[1];
        const updatedFacility = "Alpine Physical Therapy - South";

        await $(commonCaseDetails.elements.facilityFieldLabel).click();
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(commonCaseDetails.elements.facilityDropDown, updatedFacility))), 5000, 'Facility drop down menu did not expand for intial change and browser timed out');
        await element(by.cssContainingText(commonCaseDetails.elements.facilityDropDown, updatedFacility)).click();
        await commonPatientCases.enterPatientInformationWithReturnIcon();
        await commonFunctions.selectCancelInSaveYourChangesPopUp();

        since("Selecting Cancel in the Save Your Changes Pop up did not remain on the Case Details View").expect(element(by.cssContainingText("label.md-no-float.ng-binding", "Date of onset")).isPresent()).toBeTruthy();
        await commonPatientCases.enterPatientInformationWithReturnIcon();
        await commonFunctions.selectNoInSaveYourChangesPopUp();

        await commonPatientCases.enterPatientCasesFromPatientInformation();
        await commonPatientCases.selectLastCaseFirstArrivedVisit();
        await commonVisitDoc.clickTemplateCancel();
        await commonCaseDetails.enterCaseDetailsFromVisitDoc();

        let originalNoOuterText = await $(commonCaseDetails.elements.facilityFieldLabel).getAttribute("outerText");
        let verifyNoFacilityArray = originalNoOuterText.split(/[\n]/);
        let verifyNoFacility = verifyNoFacilityArray[1];
        since("Selecting Don't Save in the Save Your Changes Pop up saved the changes to the Facility").expect(originalFacility).toEqual(verifyNoFacility);

        await $(commonCaseDetails.elements.facilityFieldLabel).click();
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(commonCaseDetails.elements.facilityDropDown, updatedFacility))), 5000, 'Facility drop down menu did not expand for the second change and browser timed out');
        await element(by.cssContainingText(commonCaseDetails.elements.facilityDropDown, updatedFacility)).click();
        await commonPatientCases.enterPatientInformationWithReturnIcon();
        await commonFunctions.selectSaveInSaveYourChangesPopUp();

        await commonPatientCases.enterPatientCasesFromPatientInformation();
        await commonPatientCases.selectLastCaseFirstArrivedVisit();
        await commonVisitDoc.clickTemplateCancel();
        await commonCaseDetails.enterCaseDetailsFromVisitDoc();

        let originalSaveOuterText = await $(commonCaseDetails.elements.facilityFieldLabel).getAttribute("outerText");
        let verifySaveFacilityArray = originalSaveOuterText.split(/[\n]/);
        let verifySaveFacility = verifySaveFacilityArray[1];
        since("Selecting Save in the Save Your Changes Pop up did not save the changes to the Facility from " + verifySaveFacility + " to " + updatedFacility).expect(updatedFacility).toEqual(verifySaveFacility);

        await $(commonCaseDetails.elements.facilityFieldLabel).click();
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(commonCaseDetails.elements.facilityDropDown, originalFacility))), 5000, 'Facility drop down menu did not expand for the last change and browser timed out');
        await element(by.cssContainingText(commonCaseDetails.elements.facilityDropDown, originalFacility)).click();

        await commonFunctions.clickSaveButton();
    }),

    it("Open Past Visits (TX-741)", async function () {
        await browser.wait(EC.elementToBeClickable($(commonCaseDetails.elements.openPastVisitDropDown)), 5000, 'Open Past Visit drop down is not available to click and browser timed out');
        await $(commonCaseDetails.elements.openPastVisitDropDown).click();

        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(commonFunctions.elements.ngRepeater, commonCaseDetails.elements.thirdCaseNameFirstVisitDoc))), 5000, 'Open Past Visit drop down did not expand on click and browser timed out');
        await element(by.cssContainingText(commonFunctions.elements.ngRepeater, commonCaseDetails.elements.thirdCaseNameFirstVisitDoc)).click();

        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.signOffButtonLocation)), 5000, 'Visit doc did not display on click of Open Past Visit item and browser timed out');

        since("Visit doc did not display").expect($(commonFunctions.elements.signOffButtonLocation).isDisplayed()).toBeTruthy();
    })
});	