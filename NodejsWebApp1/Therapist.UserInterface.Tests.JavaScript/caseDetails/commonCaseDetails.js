﻿'use strict';

const EC = require('../expectedConditions');

module.exports = {

    elements: {
        caseNameDropDownLocation: "div.md-text.ng-binding",
        caseNameLocation: ".ng-pristine.ng-untouched.ng-valid.ng-scope.md-input.ng-not-empty.ng-valid-required",
        caseNameMainDropDown: "[ng-model=\"casesBar.caseId\"]",
        visitLocation: "span.ng-binding.ng-scope",
        saveButtonLocation: "button.primary.md-button.md-ink-ripple",
        firstCaseName: "Case1",
        secondVisitNextVist: "4/01/2020",
        thirdCaseName: "Case3",
        thirdCaseNameFirstVisitDoc: "11/01/2017",
        fourthCaseName: "Case4",
        fourthCaseNameFirstVisitDoc: "11/02/2017",
        facilityFieldLabel: "[field-label=\"Facility\"]",
        facilityDropDown: "[class=\"md-text ng-binding\"]",
        openPastVisitDropDown: "[field-label=\"Open Past Visit\"]",
        lockIconLocation: "span.icon-lock.ng-scope",
        referralNameFieldLabel: "[field-label=\"Referral Name\"]",
        numOfVisitsFieldLabel: "[field-label=\"# of Visits\"]",
        effectiveDateFieldLabel: "[field-label=\"Effective Date\"]",
        expirationDateFieldLabel: "[field-label=\"Expiration Date\"]",
        ngClass: "[ng-class=\"d.inputClass\"]",
        datePickerClass: "[class=\"md-datepicker-input md-input\"]"
    },


    about: {
        dateOfOnset: "11/1/2017",
        supervisor: "Everbox, Mistletoe",
        diagnoses: "002.0 - Typhoid fever",
        facility: "Alpine Physical Therapy - Downtown",
        discipline: "Physical Therapy"
    },

    referral: {
        referralSource: "Referring MD",
        referralName: "Addle, Bluebell T.", 
        numOfVisits: "2",
        effectiveDate: "1/15/2016",
        experationDate: "4/20/2017"
    },

    planOfCare: {
        numPlanOfCare: "2",
        firstEffectiveDate: "1/7/2017",
        firstDays: "70",
        firstApprovalDate: "",
        firstProgressReportDate: "3/18/2017",
        secondEffectiveDate: "1/2/2017",
        secondDays: "90",
        secondApprovalDate: "11/7/2017",
        secondProgressReportDate: "4/2/2017",
    },

    enterCaseDetailsFromVisitDoc: async function () {
        await browser.wait(EC.elementToBeClickable(element.all(by.cssContainingText('span.ng-scope', 'Details')).first()), 5000, 'Case Details Tab is not visible to click and browser timed out');
        await element.all(by.cssContainingText('span.ng-scope', 'Details')).first().click();
        await browser.wait(EC.presenceOf(element(by.cssContainingText('label', 'Next Visit'))), 5000, 'Failed to enter Case Details from Visit Doc and browser timed out');
    }
};