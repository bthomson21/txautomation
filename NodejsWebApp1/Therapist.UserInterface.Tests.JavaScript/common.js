﻿'use strict'
require('jasmine2-custom-message');

const EC = require('./expectedConditions');

const process = require('process');

module.exports = {

    elements: {
        saveYourChangePopUp: "div.clnt-toast-text.ng-binding",
        pageHasErrorPopUp: "div.md-toast-content",
        returnArrowIconLocation: "md-icon.ng-scope.md-font.material-icons.icon.icon-reply",
        saveButtonLocation: "[class=\"icon-floppy ng-scope material-icons\"]",
        signOffButtonLocation: "[class=\"icon-edit ng-scope material-icons\"]",
        ngRepeater: "[ng-repeat=\"item in d.collection\"]",
        rowRepeater: "(rowRenderIndex, row) in rowContainer.renderedRows track by $index",
        columnRepeater: "(colRenderIndex, col) in colContainer.renderedColumns track by col.uid",
        activityIndicator: "[class=\"activityIndicator\"]"
    },

    login: {
        therapistUserName: 'qatherapist.smoketx',
        therapistCoSignNeeded: 'qacosignNeeded.smoketx',
        therapistSupervisorUserName: 'qasupervisor.smoketx',
        therapistUnsignedVisitsUserName: 'qaunsigned.smoketx',
        therapistPatientAppointmentUserName: 'qapatientappointment.smoketx',
        qaSupportUserName: 'QATherapist.support',
        therapistCoSignUserName: 'QAcoSignNeeded.smoketx',
        therapistUserPassword: '1234567890Abc'
    },
    baseUrl: {
        loginPage: process.env.LOGIN_PAGE || 'https://develop.qa.insight.clinicient.com/Security/login',
    },

    reEnterCredentials: async function (userName, userPassword) {
        await element(by.id('md-input-1')).clear();
        await element(by.id('md-input-3')).clear();

        await element(by.id('md-input-1')).click();
        await element(by.id('md-input-1')).sendKeys(userName);

        await element(by.id('md-input-3')).click();
        await element(by.id('md-input-3')).sendKeys(userPassword);

        await $('[ng-reflect-trigger="[object HTMLButtonElement]"]').click();
    },

    enterCredentials: async function (userName, userPassword) {
        await browser.get(this.baseUrl.loginPage);
        await browser.manage().window().maximize();
        await element(by.id('md-input-1')).click();
        await element(by.id('md-input-1')).sendKeys(userName);

        await element(by.id('md-input-3')).click();
        await element(by.id('md-input-3')).sendKeys(userPassword);

        await $('[ng-reflect-trigger="[object HTMLButtonElement]"]').click();
    },

    validLogin: async function (userName = this.login.therapistUserName) {
        await this.enterCredentials(userName, this.login.therapistUserPassword);
        await browser.sleep(1000);
        await browser.refresh();
    },

    clickSaveButton: async function () {
        await $(this.elements.saveButtonLocation).click();
    },

    clickSuccessfullySavedMessage: async function () {
        await browser.wait(EC.elementToBeClickable(element.all(by.cssContainingText("div.clnt-toast-text.ng-binding", "Successfully saved")).get(0)), 5000, "Successfully saved message did not appear and browser timed out");
        await element(by.cssContainingText("div.clnt-toast-text.ng-binding", "Successfully saved")).click();
    },

    saveButtonStatus: async function (location, isEnabled, errorMessage = 'Save button status is not correct') {
        if (isEnabled === "true") {
            isEnabled = null;   //if button is enabled, the disabled attribute is no longer present
        }
        else if (isEnabled === "false") {
            isEnabled = "true";  //if button is disabled, the disabled attribute will be present
        }
        //since(errorMessage).expect(await $(location).getAttribute('disabled')).toBe(isEnabled);  //TODO: Enable after TX-813 is resolved
    },

    createFileNameWithTimeStamp: function (testName) {
        let timeStamp = new Date();
        let logFileName = testName + "_" + (timeStamp.getMonth() + 1) + timeStamp.getDate() + timeStamp.getFullYear() + "_" + timeStamp.getHours() + timeStamp.getMinutes() + timeStamp.getSeconds();

        return logFileName;
    },

    xmlOutputLogger: function (testName) {
        let logFileName = module.exports.createFileNameWithTimeStamp(testName);
        let screenshotReporter = require('./node_modules/protractor-jasmine2-screenshot-reporter');
        jasmine.getEnv().addReporter(new screenshotReporter({ dest: './logFiles', filename: logFileName + ".html" }));
    },

    verifyGridSort: async function (sortRowRepeater, sortColumnRepeater, columnNum, isAscOrder) {
        let sortedColumn = []
        let unSortedColumn = [];

        await browser.wait(EC.elementToBeClickable($$("[class=\"ui-grid-cell-contents ng-binding ng-scope\"]").first()), 5000, 'Patient Search results are not clickable after sort and the browser timed out');
        
        // each() executes methods in parallel
        await element.all(by.repeater(sortRowRepeater)).each(async function (rowFinder, index) {
            let columnText = await rowFinder.all(by.repeater(sortColumnRepeater)).get(columnNum).getText();
            sortedColumn[index] = columnText;
            unSortedColumn[index] = columnText;
        });
        unSortedColumn.sort();
        if (!isAscOrder) {
            unSortedColumn.reverse();
        }
        expect(sortedColumn).toEqual(unSortedColumn);
    },

    logPerformanceData: async function (testName, protractorPerfModule) {
        if (protractorPerfModule.isEnabled) {
            let logFileName = module.exports.createFileNameWithTimeStamp(testName);
            let fs = require('fs');
            fs.writeFile('./logFiles/' + logFileName + '.json', JSON.stringify(await protractorPerfModule.getStats(), null, 2), 'utf-8');
        }
    },

    selectCancelInSaveYourChangesPopUp: async function () {
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(this.elements.saveYourChangePopUp, 'Would you like to save your changes?'))), 5000, 'Pop Up window to Cancel your changes did not display and the browser timed out');
        await element(by.cssContainingText('span.ng-scope', 'cancel')).click();

        since("Cancel button did not close the Save your change Pop Up window").expect(await browser.isElementPresent(element(by.cssContainingText(this.elements.saveYourChangePopUp, 'Would you like to save your changes?')))).toBeFalsy();
    },

    selectNoInSaveYourChangesPopUp: async function () {
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(this.elements.saveYourChangePopUp, 'Would you like to save your changes?'))), 5000, 'Pop Up window to discard your changes did not display and the browser timed out');
        await element(by.cssContainingText('span.ng-scope', 'don\'t save')).click();
    },

    selectSaveInSaveYourChangesPopUp: async function () {
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(this.elements.saveYourChangePopUp, 'Would you like to save your changes?'))), 5000, 'Pop Up window to Save your changes did not display and the browser timed out');
        await element(by.cssContainingText('span.ng-binding.ng-scope', 'save')).click();
    },


    selectCancelInPageHasErrorPopUp: async function () {
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(this.elements.pageHasErrorPopUp, 'Page has errors and can not be saved. Would you like to leave?'))), 5000, 'Pop Up window to Cancel and not leave did not display and the browser timed out');
        await element(by.cssContainingText('span.ng-scope', 'cancel')).click();

        since("Cancel button did not close the Leave errors on page Pop Up window").expect(await browser.isElementPresent(element(by.cssContainingText(this.elements.saveYourChangePopUp, 'Page has errors and can not be saved. Would you like to leave?')))).toBeFalsy();
    },

    selectLeaveInPageHasErrorPopUp: async function () {
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(this.elements.pageHasErrorPopUp, 'Page has errors and can not be saved. Would you like to leave?'))), 5000, 'Pop Up window to leave and discard your changes did not display and the browser timed out');
        await element(by.cssContainingText('span.ng-binding.ng-scope', 'leave')).click();
    },

    waitForActivityIndicatorToClose: async function (activityIndicatorPage) {
        await browser.wait(EC.invisibilityOf($(this.elements.activityIndicator)), 5000, 'Waiting for Activity Indicator from page ' + activityIndicatorPage + ' to close but he browser timed out');
    },

    getModulos: async function (numerator, denominator) {
        let remainder = numerator % denominator;
        let returnValue = Math.floor(remainder >= 0 ? remainder : remainder + denominator);
        return returnValue
    },

    sendEscapeKey: async function () {
        await browser.actions().sendKeys(protractor.Key.ESCAPE).perform();
    }

};