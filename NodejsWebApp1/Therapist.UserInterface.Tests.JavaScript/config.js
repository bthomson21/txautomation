﻿
const request = require("./request/index");

exports.config = {
    SELENIUM_PROMISE_MANAGER: false,
    seleniumAddress: 'http://localhost:4444/wd/hub',
    selenium: 'http://localhost:4444/wd/hub',
    seleniumPort: 4444,

    framework: 'jasmine2',

    multiCapabilities: [
        { browserName: 'chrome', shardTestFiles: true, directConnect: true }
    ],

    afterLaunch: function() {
        return request.cleanup();
    },

    specs: [
        //'./visitDoc/visitDoc.js',
        './template/template.js', 
        //'./calendar/calendar.js',
        //'./caseDetails/caseDetails.js',
        //'./login/invalidLogin.js',
        //'./patientAppointments/patientAppointments.js',
        //'./patientCases/patientCases.js',
        //'./patientInformation/patientInformation.js',
        //'./patientSearch/patientSearch.js',
        //'./schedule/schedule.js', 
        //'./signOff/signedOffWithCoSigner.js',
        //'./signOff/signedOffWithoutCoSigner.js',
        //'./unsignedvisits/unsignedVisits.js'

    ]
};
