﻿"use strict";

class ExpectedConditions extends Object.getPrototypeOf(protractor.ExpectedConditions).constructor {
    constructor(browser) {
        super(browser);
    }

    textToEqual(elementFinder, text) {
        return async function () {
            try {
                return (await elementFinder.getText()) == text;
            }
            catch (error) {
                return false;
            }
        };
    }
};

module.exports = new ExpectedConditions(browser);