﻿'use strict'

const EC = require('../expectedConditions');

const commonFunctions = require('../common.js');
const expectedError = 'We cannot log you in with the credentials supplied. Please try again.';
const invalidCredentials = '[ng-reflect-error="[object Object]"]';

commonFunctions.xmlOutputLogger('invalidLogin');

describe('Tx Login page, valid username, invalid password', function () {

    it('Valid Username with Invalid password (TX-184, TX-274)', async function () {
        await commonFunctions.enterCredentials(commonFunctions.login.therapistUserName, 'invalidPassword');

        await browser.wait(EC.presenceOf($(invalidCredentials)), 5000, 'Error message did not display when entering Valid Username with Invalid password and browser timed out');
        expect((await $(invalidCredentials).getText()).trim()).toEqual(expectedError);
    }),

    it('Invalid Username with valid password (TX-184, TX-274)', async function () {
        await commonFunctions.reEnterCredentials('invalidUserName', commonFunctions.login.therapistUserPassword);

        await browser.wait(EC.presenceOf($(invalidCredentials)), 5000, 'Error message did not display when entering Invalid Username with valid password and browser timed out');
        expect((await $(invalidCredentials).getText()).trim()).toEqual(expectedError);
    }),

    it('SQL Injection (TX-274)', async function () {
        await commonFunctions.reEnterCredentials('\'OR\'\'= \'', '\'OR\'\'= \'')

        await browser.wait(EC.presenceOf($(invalidCredentials)), 5000, 'Error message did not display when entering SQL injection parameters and browser timed out');   
        expect((await $(invalidCredentials).getText()).trim()).toEqual(expectedError);
    }),

    it('Logout (TX-284)', async function () {
        await commonFunctions.reEnterCredentials(commonFunctions.login.therapistUserName, commonFunctions.login.therapistUserPassword);
        await browser.sleep(1000);
        await browser.refresh();

        await browser.wait(EC.presenceOf($('span.icon-down-open')), 5000, 'Schedule page was not displayed after login and browser timed out'); 
        await element(by.css('span.icon-down-open')).click();

        await browser.wait(EC.presenceOf(element(by.cssContainingText('span.ng-scope', 'Log Out'))), 5000, 'Log Out drop down was not clickable and browser timed out');
        await element(by.cssContainingText('span.ng-scope', 'Log Out')).click();
        await browser.sleep(1000);

        await browser.wait(EC.presenceOf($('div.site-footer')), 5000, 'Log in page did not display and browser timed out');  
        since('Log out was not successful').expect(await $('div.site-footer').isPresent()).toBeTruthy();
    })

});
