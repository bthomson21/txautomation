﻿'use strict';

const commonPatientSearch = require('../patientSearch/commonPatientSearch.js');
const commonFunctions = require('../common.js');

module.exports = {

    elements: {
        patientFirstName: "PERSON",
        expectedAppointmentCount: 8,
        numColumns: 6
    },

    firstArrivedVisit: {
        icon: "icon-location",
        status: "Arrived",
        time: "9/27/17 1:15 PM",
        appointmentType: "Standard Visit - 60",
        signature: "",
        case: "Leg"
    },

    signedVisit: {
        icon: "icon-lock",
        status: "Signed",
        time: "9/27/17 11:15 AM",
        appointmentType: "Initial Eval",
        signature: "Snowdrop Pawn",
        case: "Hip"
    },

    secondArrivedVisit: {
        icon: "icon-location",
        status: "Arrived",
        time: "9/20/17 12:00 PM",
        appointmentType: "Runner's Clinic",
        signature: "",
        case: "Hip"
    },

    cancelledVisit: {
        icon: "icon-cancel-circled",
        status: "Cancelled",
        time: "9/6/17 12:30 PM",
        appointmentType: "Standard Visit - 30",
        signature: "",
        case: "Leg"
    },

    initialEvalVisit: {
        icon: " ",
        status: "Initial Eval",
        time: "9/5/17 10:30 AM",
        appointmentType: "Bike Fitting",
        signature: "",
        case: "Leg"
    },

    scheduledVisit: {
        icon: "icon-calendar-empty",
        status: "Scheduled",
        time: "8/29/17 12:30 PM",
        appointmentType: "Work Screen",
        signature: "",
        case: "Hip"
    },

    coSignedVisit: {
        icon: "icon-lock",
        status: "Signed",
        time: "8/18/17 12:30 PM",
        appointmentType: "FDN - 60",
        signature: "Se?saidh Sweetpie; Bartholomew Mac'IlleDhuibh",
        case: "Hip"
    },

    coSignNeededVisit: {
        icon: "icon-lock-open-alt",
        status: "Cosign Needed by Bartholomew Mac'IlleDhuibh",
        time: "8/15/17 12:30 PM",
        appointmentType: "Progress Eval",
        signature: "Se?saidh Sweetpie",
        case: "Leg"
    },

    enterPatientAppointmentsFromPatientInformation: async function () {
        await element.all(by.cssContainingText('span.ng-scope', 'Appointments')).get(0).click();
    },

    verifyGridRowValue: async function (rowNumber, rowData) {
        const numColumns = this.elements.numColumns;
        let rowValue = await element.all(by.repeater(commonFunctions.elements.rowRepeater)).get(rowNumber).all(by.tagName('div')).get(0).getText();
        let rowValueSplit = rowValue.split(/[\n]/);
        since("In Patient Appointment, Status column, expecting " + rowValueSplit[0] + ", but displayed " + rowData.status).expect(rowValueSplit[0]).toEqual(rowData.status);
        since("In Patient Appointment, Time column, expecting " + rowValueSplit[1] + ", but displayed " + rowData.time).expect(rowValueSplit[1]).toEqual(rowData.time);
        since("In Patient Appointment, Appointment Type column, expecting " + rowValueSplit[2] + ", but displayed " + rowData.appointmentType).expect(rowValueSplit[2]).toEqual(rowData.appointmentType);

        if (rowData.signature) {
            since("In Patient Appointment, Case column, expecting " + rowValueSplit[4] + ", but displayed " + rowData.case).expect(rowValueSplit[4]).toEqual(rowData.case);
            since("In Patient Appointment, Signature column, expecting " + rowValueSplit[3] + ", but displayed " + rowData.signature).expect(rowValueSplit[3]).toEqual(rowData.signature);
        }
        else {
            since("In Patient Appointment, Case column, expecting " + rowValueSplit[3] + ", but displayed " + rowData.case).expect(rowValueSplit[3]).toEqual(rowData.case);
        }
    }
};