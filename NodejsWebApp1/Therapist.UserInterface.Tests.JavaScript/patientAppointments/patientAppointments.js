﻿'use strict';

const EC = require('../expectedConditions');

const commonFunctions = require('../common.js');
const commonPatientInformation = require('../patientInformation/commonPatientInformation.js');
const commonPatientAppointments = require('../patientAppointments/commonPatientAppointments.js');
const commonPatientSearch = require('../patientSearch/commonPatientSearch.js');
const commonSchedule = require('../schedule/commonSchedule.js');

commonFunctions.xmlOutputLogger('patientAppointments');


describe('Patient Appointments', function () {

    it('Enter Patient Appointments from Patient Search (TX-347)', async function () {
        await commonFunctions.validLogin(commonFunctions.login.therapistPatientAppointmentUserName);
        await commonPatientSearch.enterPatientSearchByMenu();
        await commonPatientInformation.enterPatientInformationFromPatientSearch(commonPatientAppointments.elements.patientFirstName);
        await commonPatientAppointments.enterPatientAppointmentsFromPatientInformation();

        await browser.wait(EC.visibilityOf($("span.clnt-label.ng-binding")), 5000, 'Failed to enter to Patient Appointment view and browser timed out');
        since("Patient Appointments Tab is not visible").expect($("span.clnt-label.ng-binding").isDisplayed()).toBeTruthy();
    }),

    it('Verify grid content', async function () {
        /* TODO: Verify icons

        */
        let rowCount = await element.all(by.repeater(commonFunctions.elements.rowRepeater)).count();
        since("Expecting " + commonPatientAppointments.elements.expectedAppointmentCount + " appointments, but " + rowCount + " displayed").expect(commonPatientAppointments.elements.expectedAppointmentCount).toEqual(rowCount);

        let numAppointmentLabel = await $('span.clnt-label.ng-binding').getAttribute('innerText');
        since("Expecting label to display " + (commonPatientAppointments.elements.expectedAppointmentCount).toString() + " Appointments, but instead displayed " + numAppointmentLabel).expect(commonPatientAppointments.elements.expectedAppointmentCount + " Appointments").toEqual(numAppointmentLabel);

        await commonPatientAppointments.verifyGridRowValue(0, commonPatientAppointments.firstArrivedVisit);
        await commonPatientAppointments.verifyGridRowValue(1, commonPatientAppointments.signedVisit);
        await commonPatientAppointments.verifyGridRowValue(2, commonPatientAppointments.secondArrivedVisit);
        await commonPatientAppointments.verifyGridRowValue(3, commonPatientAppointments.cancelledVisit);
        await commonPatientAppointments.verifyGridRowValue(4, commonPatientAppointments.initialEvalVisit);
        await commonPatientAppointments.verifyGridRowValue(5, commonPatientAppointments.scheduledVisit);
        await commonPatientAppointments.verifyGridRowValue(6, commonPatientAppointments.coSignedVisit);
        await commonPatientAppointments.verifyGridRowValue(7, commonPatientAppointments.coSignNeededVisit);
    }),

    it('Verify grid sort', async function () {
        /*TODO: Enable sort by Status when TX-434 is done
                Sort by Time/Date
        */

        await element(by.cssContainingText("span.ng-binding", "Appointment Type")).click();
        await browser.wait(EC.presenceOf($('span.clnt-label.ng-binding')), 5000, 'Failed to sort by Appointment Type ASC and browser timed out');
        await commonFunctions.verifyGridSort(commonFunctions.elements.rowRepeater, commonFunctions.elements.columnRepeater, 3, true); 
        await element(by.cssContainingText('span.ng-binding', 'Appointment Type')).click();
        await browser.wait(EC.presenceOf($('span.clnt-label.ng-binding')), 5000, 'Failed to sort by Appointment Type DESC and browser timed out');
        await commonFunctions.verifyGridSort(commonFunctions.elements.rowRepeater, commonFunctions.elements.columnRepeater, 3, false); 
        
        await element(by.cssContainingText('span.ng-binding', 'Case')).click();
        await browser.wait(EC.presenceOf($('span.clnt-label.ng-binding')), 5000, 'Failed to sort by Case ASC and browser timed out');
        await commonFunctions.verifyGridSort(commonFunctions.elements.rowRepeater, commonFunctions.elements.columnRepeater, 5, true);
        await element(by.cssContainingText('span.ng-binding', 'Case')).click();
        await browser.wait(EC.presenceOf($('span.clnt-label.ng-binding')), 5000, 'Failed to sort by Case DESC and browser timed out');
        await commonFunctions.verifyGridSort(commonFunctions.elements.rowRepeater, commonFunctions.elements.columnRepeater, 5, false); 
    })
});