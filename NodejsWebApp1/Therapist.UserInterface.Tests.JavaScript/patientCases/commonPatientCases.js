﻿'use strict';

const EC = require('../expectedConditions');

const commonPatientSearch = require('../patientSearch/commonPatientSearch.js');
const commonFunctions = require('../common.js');

module.exports = {

    elements: {
        patientFirstName: "PATIENT",
        caseNameLocation: ".ng-valid.ng-scope.md-input.ng-not-empty.ng-valid-required",
        detailsTabLocation: "md-tab-item.md-tab.ng-scope.ng-isolate-scope.md-ink-ripple",
        visitLocation: "span.ng-binding.ng-scope",
        saveButtonLocation: "button.md-button.ng-scope.md-ink-ripple.primary",
        nextVisitLocation: "clnt-next-visit",
        nextVisitClickLocation: "[ng-show=\"d.isArrived\"]",
        unsignedVisitLocation: "[class=\"clnt-inputs-block clnt-unsigned-visit\"]",
        unArrivedNextVisit: "4/01/2020",
        arrivedNextVisit: "5/05/2020",
        lastCaseUnsignedVisitFirst: "11/01/2017",
        lastCaseUnsignedVisitSecond: "5/05/2020"
    },

    enterPatientCasesFromPatientInformation: async function () {
        await browser.wait(EC.elementToBeClickable(element.all(by.cssContainingText('span.ng-scope', 'Cases')).first()), 5000, 'Cases Tab failed to display');
        await element.all(by.cssContainingText('span.ng-scope', 'Cases')).first().click();
        await browser.wait(EC.presenceOf($('md-input-container.clnt-input-container.clnt-required.md-input-has-value')), 5000, 'Case Name failed to display and browser timed out');
    },
    
    enterPatientInformationWithReturnIcon: async function () {
        await $(commonFunctions.elements.returnArrowIconLocation).click();
    },

    selectLastCaseFirstArrivedVisit: async function () {
        await element(by.cssContainingText(this.elements.visitLocation, this.elements.lastCaseUnsignedVisitFirst)).click();
    },

    verifyNextVisit: async function (elementIndex, isHidden, expectedText, appointmentType) {
        let nextVisitTextString = await $$(this.elements.nextVisitLocation).get(elementIndex).getAttribute("outerText");
        let  nextVisitText = nextVisitTextString.split("\n");
        nextVisitText[1] = nextVisitText[1].trim();
        since(appointmentType + " appointment, expecting Next Visit to be " + expectedText + ", but actual is " + nextVisitText[1]).expect(nextVisitText[1]).toEqual(expectedText);

        let nextVisitClickable = $$(this.elements.nextVisitClickLocation).get(elementIndex);
        since(appointmentType + " apointment, expecting aria-hidden attribute to be " + isHidden).expect(await nextVisitClickable.getAttribute("aria-hidden")).toEqual(isHidden);
    },

    verifyUnsignedVisit: async function (elementIndex, isHidden, expectedText, appointmentType) {
        let unsignedtVisitTextString = await $$(this.elements.unsignedVisitLocation).get(elementIndex).getAttribute("outerText");
        unsignedtVisitTextString = unsignedtVisitTextString.replace(/(\r\n|\n|\r)/gm, "");
        unsignedtVisitTextString = unsignedtVisitTextString.trim();
        since(appointmentType + " appointment, expecting Unsigned Visit to be " + expectedText + ", but actual is " + unsignedtVisitTextString).expect(unsignedtVisitTextString).toEqual(expectedText);
    }
};