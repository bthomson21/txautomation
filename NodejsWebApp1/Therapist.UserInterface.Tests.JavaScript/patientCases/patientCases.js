﻿'use strict';

const EC = require('../expectedConditions');

const commonFunctions = require('../common.js');
const commonPatientCases = require('./commonPatientCases.js');
const commonPatientSearch = require('../patientSearch/commonPatientSearch.js');
const commonPatientInformation = require('../patientInformation/commonPatientInformation.js');

commonFunctions.xmlOutputLogger('patientCases');

describe('Patient Cases', function () {

    it('Enter Patient Cases from Patient Information', async function () {
        await commonFunctions.validLogin();
        await commonPatientSearch.enterPatientSearchByMenu();
        await commonPatientInformation.enterPatientInformationFromPatientSearch(commonPatientCases.elements.patientFirstName);
        await commonPatientCases.enterPatientCasesFromPatientInformation();
    }),

    it('Verify Next Visits (TX-182)', async function () {
        await commonPatientCases.verifyNextVisit(0, "true", "None Scheduled", "No");
        await commonPatientCases.verifyNextVisit(1, "true", commonPatientCases.elements.unArrivedNextVisit, "Scheduled");
        await commonPatientCases.verifyNextVisit(2, "false", commonPatientCases.elements.arrivedNextVisit, "Arrived"); 
    }),

    it('Verify Unsigned Visits (TX-182)', async function () {
        await commonPatientCases.verifyUnsignedVisit(0, "true", "All visits are signed", "No");
        await commonPatientCases.verifyUnsignedVisit(1, "true", "All visits are signed", "No");
        await commonPatientCases.verifyUnsignedVisit(2, "false", "11/01/2017 5/05/2020", "Arrived");
    }),

    it('Case Name required (TX-182)', async function () {    
        let originalCaseName = await $$(commonPatientCases.elements.caseNameLocation).first().getAttribute("value");
        //await commonFunctions.saveButtonStatus(commonPatientCases.elements.saveButtonLocation, "false", "No modifications, Save button should be disabled");  //TODO: Enable after TX-813 is resolved
        await $$(commonPatientCases.elements.caseNameLocation).first().click();
        await $$(commonPatientCases.elements.caseNameLocation).first().sendKeys(protractor.Key.BACK_SPACE);
        await commonFunctions.saveButtonStatus(commonPatientCases.elements.saveButtonLocation, "true", "Valid modifications, Save button should be enabled");
        await $$(commonPatientCases.elements.caseNameLocation).first().clear();
        //await commonFunctions.saveButtonStatus(commonPatientCases.elements.saveButtonLocation, "false", "Case Name required, Save button should be disabled");  //TODO: Enable after TX-813 is resolved

        await element.all(by.cssContainingText('span.ng-scope', 'Information')).first().click();
        await commonFunctions.selectCancelInPageHasErrorPopUp();

        await element.all(by.cssContainingText('span.ng-scope', 'Information')).first().click();
        await commonFunctions.selectLeaveInPageHasErrorPopUp();

        await commonPatientCases.enterPatientCasesFromPatientInformation();   
    }),

    it('Open Visit Doc from First Case via Full Details (TX-227) ', async function () {
        await element.all(by.cssContainingText('span.ng-scope', 'Full details')).first().click();
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(commonPatientCases.elements.detailsTabLocation, 'Details'))), 5000, 'Failed to enter to Case Details after clicking Full Details and browser timed out');
        since("Case Details tab is not present after clicking on Full Details").expect(await element(by.cssContainingText(commonPatientCases.elements.detailsTabLocation, 'Details')).isDisplayed()).toBeTruthy();
        await commonPatientCases.enterPatientInformationWithReturnIcon();
        await commonPatientCases.enterPatientCasesFromPatientInformation();
    }),

    it('Verify Cancel/No/Save in the Save Your Changes Pop Up (TX-428)', async function () {
        await $$(commonPatientCases.elements.caseNameLocation).first().click();

        let originalCaseName = await $$(commonPatientCases.elements.caseNameLocation).first().getAttribute("value");
        await $$(commonPatientCases.elements.caseNameLocation).first().sendKeys("X");

        await element.all(by.cssContainingText('span.ng-scope', 'Information')).first().click();
        await commonFunctions.selectCancelInSaveYourChangesPopUp();
        since("Selecting Cancel in the Save Your Changes Pop up did not remain on the Patient Cases View").expect(await element(by.cssContainingText(commonPatientCases.elements.visitLocation, commonPatientCases.elements.lastCaseUnsignedVisitFirst)).isPresent()).toBeTruthy();

        await element.all(by.cssContainingText('span.ng-scope', 'Information')).first().click();
        await commonFunctions.selectNoInSaveYourChangesPopUp();

        await browser.wait(EC.presenceOf(element(by.cssContainingText('label.md-no-float.ng-binding', 'First Name'))), 5000, "Selecting Don't Save in the Save Your Changes Pop up did not go to the Patient Information View and browser timed out");
        since("Selecting Don't Save in the Save Your Changes Pop up did not go to the Patient Information View").expect(await element(by.cssContainingText('label.md-no-float.ng-binding', 'First Name')).isPresent()).toBeTruthy();
        await commonPatientInformation.enterPatientCasesFromPatientInformation();

        let caseName = await $$(commonPatientCases.elements.caseNameLocation).first().getAttribute("value");
        since("After selecting Don't Save in the Save Your Changes Pop up, the modified Case name was saved").expect(caseName).toEqual(originalCaseName);

        await $$(commonPatientCases.elements.caseNameLocation).first().click();
        await $$(commonPatientCases.elements.caseNameLocation).first().sendKeys("X");
        await element.all(by.cssContainingText('span.ng-scope', 'Information')).first().click();
        await commonFunctions.selectSaveInSaveYourChangesPopUp();

        await browser.wait(EC.presenceOf(element(by.cssContainingText('label.md-no-float.ng-binding', 'First Name'))), 5000, 'Selecting Save in the Save Your Changes Pop up did not go to the Patient Information View and browser timed out');
        since("Selecting Save in the Save Your Changes Pop up did not go to the Patient Information View").expect(await element(by.cssContainingText('label.md-no-float.ng-binding', 'First Name')).isPresent()).toBeTruthy();
        await commonPatientInformation.enterPatientCasesFromPatientInformation();

        let savedCaseName = await $$(commonPatientCases.elements.caseNameLocation).first().getAttribute("value");
        since("After selecting Save in the Save Your Changes Pop up, the modified Case name was not saved").expect(savedCaseName).toEqual(originalCaseName + "X");

        await $$(commonPatientCases.elements.caseNameLocation).first().click();
        await $$(commonPatientCases.elements.caseNameLocation).first().sendKeys(protractor.Key.BACK_SPACE);
        await commonFunctions.clickSaveButton();
    })
});