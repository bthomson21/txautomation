﻿'use strict'

const EC = require('../expectedConditions');

const commonPatientSearch = require('../patientSearch/commonPatientSearch.js');

module.exports = {

    elements: {
        nameParentLocation: "clnt-input.ng-pristine.ng-untouched.ng-valid.ng-isolate-scope.ng-not-empty",
        nameChildLocation: ".ng-pristine.ng-untouched.ng-scope.md-input.ng-valid.ng-valid-required.ng-not-empty",
        preferredNameParentLocation: "clnt-input",
        preferredNameChildLocation: ".ng-scope.md-input",
        firstName: "[field-label=\"First Name\"]",
        lastName: "[field-label=\"Last Name\"]",
        dateOfBirthLocation: ".md-datepicker-input.md-input",
        ageLocation: "span.clnt-age-span.ng-binding",
        patientTabLocation: "span.ng-binding.ng-scope",
        searchFirstName: "GAILY",
        searchFirstNameFirstScrollPosition: "Criminal",
        patientTab: "[ng-click=\"vm.removePatientTab( state.patient,$event )\"]"
    },

    enterPatientInformationFromPatientSearch: async function (firstName = this.elements.searchFirstName) {
        await browser.wait(EC.elementToBeClickable($(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation)), 5000, 'First Name field in Patient Search failed to display and browser timed out');
        await commonPatientSearch.clearButtonClick();
        await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).click();
        await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).sendKeys(firstName);
        await commonPatientSearch.searchButtonClick();
        await element(by.cssContainingText('.ui-grid-cell-contents.ng-binding.ng-scope', firstName.toUpperCase())).click();
        await browser.wait(EC.presenceOf($$("[field-label=\"First Name\"]").get(0).$(module.exports.elements.nameChildLocation)), 5000, 'Patient Information was not displayed and browser timed out');
    },
    
    enterPatientCasesFromPatientInformation: async function () {
        await element.all(by.cssContainingText('span.ng-scope', 'Cases')).get(0).click();
        await browser.wait(EC.presenceOf($('md-input-container.clnt-input-container.clnt-required.md-input-has-value')), 5000, 'Case Name failed to display and browser timed out');
    },

    enterPatientInformationFromPatientCases: async function () {
        await element.all(by.cssContainingText('span.ng-scope', 'Information')).get(0).click();
    },

    closePatientTab: async function (tabIndex) {
        await $$(this.elements.patientTab).get(tabIndex).click();
    },

    addPhoneNumbers: async function (phoneNumberType, tenDigitPhoneNumber, selectProvider = false) {
        let phoneNumber = tenDigitPhoneNumber.split('-');
        let areaCode = phoneNumber[0];
        let prefix = phoneNumber[1];
        let lineNumber = phoneNumber[2];

        await element(by.cssContainingText('span.ng-scope', 'Add phone number')).click();
        await browser.wait(EC.elementToBeClickable($('clnt-select.ng-pristine.ng-untouched.ng-isolate-scope.ng-empty.ng-invalid.ng-invalid-required')), 5000, 'Add phone number failed and browser timed out');
        await $('clnt-select.ng-pristine.ng-untouched.ng-isolate-scope.ng-empty.ng-invalid.ng-invalid-required').click();
        await browser.wait(EC.elementToBeClickable($('.md-select-menu-container.md-active.md-clickable').element(by.cssContainingText('div.md-text.ng-binding', phoneNumberType))), 5000, 'Pop up to select Phone Number type failed and browser timed out');
        await $('.md-select-menu-container.md-active.md-clickable').element(by.cssContainingText('div.md-text.ng-binding', phoneNumberType)).click();

        await $('[phone-type=\"' + phoneNumberType + '\"]').element(by.name('areaCode')).click();
        await $('[phone-type=\"' + phoneNumberType + '\"]').element(by.name('areaCode')).sendKeys(areaCode);
        await $('[phone-type=\"' + phoneNumberType + '\"]').element(by.name('prefix')).click();
        await $('[phone-type=\"' + phoneNumberType + '\"]').element(by.name('prefix')).sendKeys(prefix);
        await $('[phone-type=\"' + phoneNumberType + '\"]').element(by.name('lineNumber')).click();
        await $('[phone-type=\"' + phoneNumberType + '\"]').element(by.name('lineNumber')).sendKeys(lineNumber);

        if (phoneNumberType === 'Work' && phoneNumber.length === 4) {
            let extension = phoneNumber[3];
            await $('[phone-type=\"' + phoneNumberType + '\"]').element(by.name('extension')).click();
            await $('[phone-type=\"' + phoneNumberType + '\"]').element(by.name('extension')).sendKeys(extension);
        }

        if (selectProvider) {
            await browser.wait(EC.elementToBeClickable($('[ng-model=\"phone.cellProvider.cellProviderID\"]')), 5000, 'Clicking on Provider failed and browser timed out');
            await $('[ng-model=\"phone.cellProvider.cellProviderID\"]').click();
            await browser.wait(EC.elementToBeClickable(element(by.cssContainingText('div.md-text.ng-binding', 'AT&T'))), 5000, 'Clicking on AT&T on Provider drop down failed and browser timed out');
            await element(by.cssContainingText('div.md-text.ng-binding', 'AT&T')).click();
        }
    }
};