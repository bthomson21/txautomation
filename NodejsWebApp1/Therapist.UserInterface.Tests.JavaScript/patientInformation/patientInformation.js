﻿'use strict';

const EC = require('../expectedConditions');

const commonFunctions = require('../common.js');
const commonPatientInformation = require('../patientInformation/commonPatientInformation.js');
const commonPatientSearch = require('../patientSearch/commonPatientSearch.js');
const commonSchedule = require('../schedule/commonSchedule.js');

commonFunctions.xmlOutputLogger('patientInformation');

describe('Patient Information', function () {

    it('Enter Patient Information from Patient Search (TX-347)', async function () {
        await commonFunctions.validLogin();
        await commonPatientSearch.enterPatientSearchByMenu();
        await commonPatientInformation.enterPatientInformationFromPatientSearch();

        since("Patient Tab was not created").expect(element.all(by.cssContainingText(commonPatientInformation.elements.patientTabLocation, commonPatientInformation.elements.searchFirstName.toUpperCase())).first().isDisplayed()).toBeTruthy();
    }),

    it('Save First Name', async function () {
        let firstName = await element.all(by.cssContainingText(commonPatientInformation.elements.nameParentLocation, 'First Name')).first().$(commonPatientInformation.elements.nameChildLocation).getAttribute("value");
        since("First name is a required field").expect(firstName).toBeTruthy();

        let value = await element.all(by.cssContainingText(commonPatientInformation.elements.nameParentLocation, 'First Name')).first().$(commonPatientInformation.elements.nameChildLocation).getAttribute("value");
        firstName = value;

        await element.all(by.cssContainingText(commonPatientInformation.elements.nameParentLocation, 'First Name')).first().$(commonPatientInformation.elements.nameChildLocation).click();
        await element.all(by.cssContainingText(commonPatientInformation.elements.nameParentLocation, 'First Name')).first().$(commonPatientInformation.elements.nameChildLocation).sendKeys("7");
        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.saveButtonLocation)), 5000, 'After changing First Name, Save button is not clickable and browser timed out');
        await commonFunctions.clickSaveButton();
        browser.sleep(1000);  //TODO: Remove after TX-813 is resolved
        await element.all(by.cssContainingText("span.ng-scope", "Information")).first().click();
        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.saveButtonLocation)), 5000, 'After changing First Name and clicking Save, the Save button is not clickable and browser timed out');
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should not be enabled after saving the First Name");  //TODO: Enable after TX-813 is resolved
        await commonPatientInformation.enterPatientCasesFromPatientInformation();
        await commonPatientInformation.enterPatientInformationFromPatientCases();

        value = element.all(by.cssContainingText(commonPatientInformation.elements.nameParentLocation, 'First Name')).first().$(commonPatientInformation.elements.nameChildLocation).getAttribute("value");
        since("Modified First name was not saved").expect(firstName + "7").toEqual(value);

        await element.all(by.cssContainingText(commonPatientInformation.elements.nameParentLocation, 'First Name')).first().$(commonPatientInformation.elements.nameChildLocation).click();
        await element.all(by.cssContainingText(commonPatientInformation.elements.nameParentLocation, 'First Name')).first().$(commonPatientInformation.elements.nameChildLocation).sendKeys(protractor.Key.BACK_SPACE);
        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.saveButtonLocation)), 5000, 'After changing First Name back to the original, Save button is not clickable and browser timed out');
        await commonFunctions.clickSaveButton();
            browser.sleep(1000);  //TODO: Remove after TX-813 is resolved
        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.saveButtonLocation)), 5000, 'After changing First Name back to the original and clicking Save, the Save button is not clickable and browser timed out');
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should not be enabled after saving the First Name back to the original");  //TODO: Enable after TX-813 is resolved
    }),

    it('Save Last Name', async function () {
        let lastName = await element.all(by.cssContainingText(commonPatientInformation.elements.nameParentLocation, 'Last Name')).first().$(commonPatientInformation.elements.nameChildLocation).getAttribute("value");
        since("Last name is a required field").expect(lastName).toBeTruthy();

        let value = await element.all(by.cssContainingText(commonPatientInformation.elements.nameParentLocation, 'Last Name')).first().$(commonPatientInformation.elements.nameChildLocation).getAttribute("value");
        lastName = value;

        await element.all(by.cssContainingText(commonPatientInformation.elements.nameParentLocation, 'Last Name')).first().$(commonPatientInformation.elements.nameChildLocation).click();
        await element.all(by.cssContainingText(commonPatientInformation.elements.nameParentLocation, 'Last Name')).first().$(commonPatientInformation.elements.nameChildLocation).sendKeys("1");
        await browser.wait(EC.elementToBeClickable(element(by.css(commonFunctions.elements.saveButtonLocation))), 5000, 'After changing Last Name, Save button is not clickable and browser timed out');
        await commonFunctions.clickSaveButton();
        browser.sleep(1000);  //TODO: Remove after TX-813 is resolved
        await element.all(by.cssContainingText("span.ng-scope", "Information")).first().click();
        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.saveButtonLocation)), 5000, 'After changing Last Name and clicking Save, the Save button is not clickable and browser timed out');
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should not be enabled after saving the Last Name");  //TODO: Enable after TX-813 is resolved

        await commonPatientInformation.enterPatientCasesFromPatientInformation();
        await commonPatientInformation.enterPatientInformationFromPatientCases();

        value = await element.all(by.cssContainingText(commonPatientInformation.elements.nameParentLocation, 'Last Name')).first().$(commonPatientInformation.elements.nameChildLocation).getAttribute("value");
        since("Modified Last name was not saved").expect(lastName + "1").toEqual(value);
        
        await element.all(by.cssContainingText(commonPatientInformation.elements.nameParentLocation, 'Last Name')).first().$(commonPatientInformation.elements.nameChildLocation).click();
        await element.all(by.cssContainingText(commonPatientInformation.elements.nameParentLocation, 'Last Name')).first().$(commonPatientInformation.elements.nameChildLocation).sendKeys(protractor.Key.BACK_SPACE);
        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.saveButtonLocation)), 5000, 'After changing Last Name back to the original, Save button is not clickable and browser timed out');
        await commonFunctions.clickSaveButton();
            browser.sleep(1000);  //TODO: Remove after TX-813 is resolved
        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.saveButtonLocation)), 5000, 'After changing Last Name back to the original and clicking Save, the Save button is not clickable and browser timed out');
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should not be enabled after saving the Last Name back to the original");  //TODO: Enable after TX-813 is resolved
    }),

    it('Save Preferred Name', async function () {
        await element(by.cssContainingText(commonPatientInformation.elements.preferredNameParentLocation, "Preferred Name")).$(commonPatientInformation.elements.preferredNameChildLocation).click();
        await element(by.cssContainingText(commonPatientInformation.elements.preferredNameParentLocation, "Preferred Name")).$(commonPatientInformation.elements.preferredNameChildLocation).sendKeys("X");
        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.saveButtonLocation)), 5000, 'After changing Preferred Name, Save button is not clickable and browser timed out');
        await commonFunctions.clickSaveButton();
        browser.sleep(1000);  //TODO: Remove after TX-813 is resolved
        await element.all(by.cssContainingText("span.ng-scope", "Information")).first().click();
        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.saveButtonLocation)), 5000, 'After changing Preferred Name and clicking Save, the Save button is not clickable and browser timed out');
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should not be enabled after saving the Preferred Name");  //TODO: Enable after TX-813 is resolved

        await commonPatientInformation.enterPatientCasesFromPatientInformation();
        await commonPatientInformation.enterPatientInformationFromPatientCases();

        let value = await element(by.cssContainingText(commonPatientInformation.elements.preferredNameParentLocation, "Preferred Name")).$(commonPatientInformation.elements.preferredNameChildLocation).getAttribute("value");
        since("Modified Preferred name was not saved").expect("X").toEqual(value);

        await element(by.cssContainingText(commonPatientInformation.elements.preferredNameParentLocation, "Preferred Name")).$(commonPatientInformation.elements.preferredNameChildLocation).click();
        await element(by.cssContainingText(commonPatientInformation.elements.preferredNameParentLocation, "Preferred Name")).$(commonPatientInformation.elements.preferredNameChildLocation).sendKeys(protractor.Key.BACK_SPACE);
        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.saveButtonLocation)), 5000, 'After changing Preferred Name back to the original, Save button is not clickable and browser timed out');
        await commonFunctions.clickSaveButton();
        browser.sleep(1000);  //TODO: Remove after TX-813 is resolved
        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.saveButtonLocation)), 5000, 'After changing Preferred Name back to the original and clicking Save, the Save button is not clickable and browser timed out');
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should not be enabled after saving the Preferred Name back to the original");  //TODO: Enable after TX-813 is resolved
    }),

    it('Save Date of Birth', async function () {
        const newDateOfBirth = "10/10/1945";

        let dateOfBirth = await $(commonPatientInformation.elements.dateOfBirthLocation).getAttribute("value");
        
        since(`Birthdate should differ from ${newDateOfBirth}. Please correct test data`).expect(dateOfBirth).not.toEqual(newDateOfBirth);
        
        since("Birthdate is a required field").expect(dateOfBirth).toBeTruthy();

        await $(commonPatientInformation.elements.dateOfBirthLocation).clear();
        await $(commonPatientInformation.elements.dateOfBirthLocation).click();
        await $(commonPatientInformation.elements.dateOfBirthLocation).sendKeys(newDateOfBirth);
        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.saveButtonLocation)), 5000, 'After changing birthdate, Save button is not clickable and browser timed out');
        await browser.sleep(1000);  //TODO: Remove after TX-813 is resolved
        await commonFunctions.clickSaveButton();
        await element.all(by.cssContainingText("span.ng-scope", "Information")).first().click();
        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.saveButtonLocation)), 5000, 'After changing birthdate and clicking Save, the Save button is not clickable and browser timed out');
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should not be enabled after saving the birthdate");  //TODO: Enable after TX-813 is resolved

        await commonPatientInformation.enterPatientCasesFromPatientInformation();
        await commonPatientInformation.enterPatientInformationFromPatientCases();

        let displayedAge = await $(commonPatientInformation.elements.ageLocation).getAttribute("textContent");
        let currentDate = new Date();
        let newDOB = new Date(newDateOfBirth);
        let years = (currentDate.getFullYear() - newDOB.getFullYear());

        displayedAge = displayedAge.replace(/[()]/g, '');
        if (currentDate.getMonth() < newDOB.getMonth() ||
            currentDate.getMonth() == newDOB.getMonth() && currentDate.getDate() < newDOB.getDate()) {
            years--;
        }
                since("After updating Date of birth, expecting new age " + years.toString() + " to be displayed, instead " + displayedAge + " is displayed").expect(years.toString()).toEqual(displayedAge);
        
        let newValue = await $(commonPatientInformation.elements.dateOfBirthLocation).getAttribute("value");
        since("Modified Date of Birth was not saved").expect(newDateOfBirth).toEqual(newValue);

        await $(commonPatientInformation.elements.dateOfBirthLocation).clear();
            browser.sleep(1000);  //TODO: Remove after TX-813 is resolved
        await $(commonPatientInformation.elements.dateOfBirthLocation).click();
        await $(commonPatientInformation.elements.dateOfBirthLocation).sendKeys(dateOfBirth);

        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.saveButtonLocation)), 5000, 'After changing birthdate, Save button is not clickable and browser timed out');
        await commonFunctions.clickSaveButton();
            browser.sleep(1000);  //TODO: Remove after TX-813 is resolved

        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.saveButtonLocation)), 5000, 'After changing birthdate and clicking Save, the Save button is not clickable and browser timed out');
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should not be enabled after saving the birthdate back to the original");  //TODO: Enable after TX-813 is resolved
    }),

    it('Verify Cancel/No/Save in the Save Your Changes Pop Up (TX-490)', async function () {
        const firstNewDateOfBirth = "3/20/1981";
        const secondNewDateOfBirth = "7/13/1954";

        let dateOfBirth = await $(commonPatientInformation.elements.dateOfBirthLocation).getAttribute("value");

        since(`Birthdate should differ from ${firstNewDateOfBirth}. Please correct test data`).expect(dateOfBirth).not.toEqual(firstNewDateOfBirth);
        since(`Birthdate should differ from ${secondNewDateOfBirth}. Please correct test data`).expect(dateOfBirth).not.toEqual(secondNewDateOfBirth);
        
        await $(commonPatientInformation.elements.dateOfBirthLocation).clear();
        await $(commonPatientInformation.elements.dateOfBirthLocation).click();
        await $(commonPatientInformation.elements.dateOfBirthLocation).sendKeys(firstNewDateOfBirth);
        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.saveButtonLocation)), 5000, 'After changing first birthdate, Save button is not clickable and browser timed out');
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "true", "Save button should be enabled after changing the birthdate");
        await browser.sleep(1000);  //TODO: Remove after TX-813 is resolved
        await commonPatientSearch.enterPatientSearchByIcon();
            browser.sleep(1000);  //TODO: Remove after TX-813 is resolved
        await commonFunctions.selectCancelInSaveYourChangesPopUp();
        since("Selecting Cancel in the Save Your Changes Pop up did not remain on the Patient Information View").expect(element(by.cssContainingText('label.md-no-float.ng-binding', 'First Name')).isPresent()).toBeTruthy();

        await commonPatientSearch.enterPatientSearchByIcon();
        await commonFunctions.selectNoInSaveYourChangesPopUp();

        await browser.wait(EC.presenceOf(element(by.cssContainingText('label.md-no-float.ng-binding', 'First Name'))), 5000, 'Patient Search was not displayed and browser timed out');

        since("Selecting Don't Save in the Save Your Changes Pop up did not go to the Patient Search view").expect(element(by.cssContainingText('label.md-no-float.ng-binding', 'First Name')).isPresent()).toBeTruthy();
        await commonPatientInformation.enterPatientInformationFromPatientSearch();

        //let value = await $(commonPatientInformation.elements.dateOfBirthLocation).getAttribute("value");
        //since("After selecting Don't Save in the Save Your Changes Pop up, the modified Date of Birth was saved").expect(value).toEqual(dateOfBirth);  //TODO: Enable after TX-971 is resolved

        await $(commonPatientInformation.elements.dateOfBirthLocation).clear();
        await $(commonPatientInformation.elements.dateOfBirthLocation).click();
        await $(commonPatientInformation.elements.dateOfBirthLocation).sendKeys(secondNewDateOfBirth);
        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.saveButtonLocation)), 5000, 'After changing second birthdate, Save button is not clickable and browser timed out');
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "true");

        await browser.sleep(1000);  //TODO: Remove after TX-813 is resolved
        await commonPatientSearch.enterPatientSearchByIcon();
        await commonFunctions.selectSaveInSaveYourChangesPopUp();

        since("Selecting Save in the Save Your Changes Pop up did not go to the Patient Search view").expect(element(by.cssContainingText('label.md-no-float.ng-binding', 'First Name')).isPresent()).toBeTruthy();
        await commonPatientInformation.enterPatientInformationFromPatientSearch();

        let value = await $(commonPatientInformation.elements.dateOfBirthLocation).getAttribute("value");
        since("After selecting Save in the Save Your Changes Pop up, the modified Date of Birth was not saved").expect(secondNewDateOfBirth).toEqual(value);

        await $(commonPatientInformation.elements.dateOfBirthLocation).clear();
        await $(commonPatientInformation.elements.dateOfBirthLocation).click();
        await $(commonPatientInformation.elements.dateOfBirthLocation).sendKeys(dateOfBirth);
        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.saveButtonLocation)), 5000, 'After changing birthdate back to the original, Save button is not clickable and browser timed out');
        await browser.sleep(1000);  //TODO: Remove after TX-813 is resolved
        await commonFunctions.clickSaveButton();
        await browser.wait(EC.elementToBeClickable($(commonFunctions.elements.saveButtonLocation)), 5000, 'After changing birthdate back to the original and clicking Save, the Save button is not clickable and browser timed out');
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should not be enabled after saving the birthdate back to the original"); //TODO: Enable after TX-813 is resolved
    }),

    it('Verify required First Name, Last Name and DOB.  Page has errors pop up (TX-618,TX-744))', async function() {

        await $$("[class=\"clnt-input-and-note\"]").first().$(".ng-scope.md-input").click();
        await $$("[class=\"clnt-input-and-note\"]").first().$(".ng-scope.md-input").clear();
        
        await element.all(by.cssContainingText("span.ng-scope", "Information")).first().click();

        since("Removing First Name did not prompt the user with an error message").expect(element(by.cssContainingText("div.ng-binding.ng-scope", "Field is required")).isDisplayed()).toBeTruthy();
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should not be enabled after deleting the First Name");  //TODO: Enable after TX-813 is resolved

        await element.all(by.cssContainingText('span.ng-scope', 'Cases')).first().click();
        await commonFunctions.selectCancelInPageHasErrorPopUp();

        await element.all(by.cssContainingText('span.ng-scope', 'Cases')).first().click();
        await commonFunctions.selectLeaveInPageHasErrorPopUp();

        await browser.wait(EC.elementToBeClickable(element.all(by.cssContainingText("label", "Next Visit")).first()), 5000, 'Clicking on the Cases tab after removing First Name failed and browser timed out');
        await commonPatientInformation.enterPatientInformationFromPatientCases();

        await $$("[class=\"clnt-input-and-note\"]").get(1).$(".ng-scope.md-input").click();
        await $$("[class=\"clnt-input-and-note\"]").get(1).$(".ng-scope.md-input").clear();
        await element.all(by.cssContainingText("span.ng-scope", "Information")).first().click();

        since("Removing Last Name did not prompt the user with an error message").expect(element(by.cssContainingText("div.ng-binding.ng-scope", "Field is required")).isDisplayed()).toBeTruthy();
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should not be enabled after deleting the Last Name");  //TODO: Enable after TX-813 is resolved

        await commonPatientInformation.enterPatientCasesFromPatientInformation();
        await commonFunctions.selectCancelInPageHasErrorPopUp();

        await commonPatientInformation.enterPatientCasesFromPatientInformation();
        await commonFunctions.selectLeaveInPageHasErrorPopUp();

        await browser.wait(EC.elementToBeClickable(element.all(by.cssContainingText("label", "Next Visit")).first()), 5000, 'Clicking on the Cases tab after removing Last Name failed and browser timed out');
        await commonPatientInformation.enterPatientInformationFromPatientCases();

        for(var count = 0; count < 9; count++) {
            await $(commonPatientInformation.elements.dateOfBirthLocation).sendKeys(protractor.Key.BACK_SPACE);
        }
       
        await element.all(by.cssContainingText("span.ng-scope", "Information")).first().click();
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText("div.ng-binding.ng-scope", "Field is required"))), 5000, 'Waiting for error message after removing Date of Birth failed and browser timed out');
        since("Removing Date of Birth did not prompt the user with an error message").expect(element(by.cssContainingText("div.ng-binding.ng-scope", "Field is required")).isDisplayed()).toBeTruthy();
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should not be enabled after deleting the Date of Birth");  //TODO: Enable after TX-813 is resolved

        await commonPatientInformation.enterPatientCasesFromPatientInformation();
        await commonFunctions.selectCancelInPageHasErrorPopUp();

        await commonPatientInformation.enterPatientCasesFromPatientInformation();
        await commonFunctions.selectLeaveInPageHasErrorPopUp();

        await browser.wait(EC.elementToBeClickable(element.all(by.cssContainingText("label", "Next Visit")).first()), 5000, 'Clicking on the Cases tab after removing Date of Birth failed and browser timed out');
        await commonPatientInformation.enterPatientInformationFromPatientCases();
    }),

    it('Add/Remove/Update Phone Numbers (TX-180, TX-350, TX-490, TX-396)', async function() {
        //TODO: Add valid phone number, then click Trash can.  Save button should be disabled.
        //TODO: TX-410. Click trash can on existing Phone numbers.  Save button should be enabled.

        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false");
        //element(by.css('[ng-form=\"phone0\"]')).element(by.css('md-icon.icon-trash.ng-scope.material-icons')).click();
        await $('md-icon.icon-trash.ng-scope.material-icons').click();
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "true", "Save button should be enabled after deleting the existing Home number");
        await commonFunctions.clickSaveButton();

        await commonPatientInformation.addPhoneNumbers('Home', '987-654-3210');
        await commonPatientInformation.addPhoneNumbers('Home', '835-930-9999');
        since('Only one Home number is allowed').expect($('Only one Home number is allowed')).toBeTruthy();
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should be disabled after entering a second Home number");
        await $('[ng-form=\"phone1\"]').$('md-icon.icon-trash.ng-scope.material-icons').click();
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "true", "Save button should be enabled after deleting the second Home number. The first Home number is new and requires a save");
        await commonFunctions.clickSaveButton();
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should be disabled after saving the Home number");

        await commonPatientInformation.addPhoneNumbers('Work', '503-123-47');
        since('Work Phone number must have at least 10 digits').expect($('Invalid phone number.')).toBeTruthy();
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should be disabled after entering an invalid Work number");
        await $('[ng-form=\"phone1\"]').$('md-icon.icon-trash.ng-scope.material-icons').click();
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should be disabled after deleting the invalid Work number. There were no other changes to the page");

        await commonPatientInformation.addPhoneNumbers('Work', '503-123-4567');
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "true", "Save button should be enabled after entering a valid Work Number");
        await commonFunctions.clickSaveButton();
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "true", "Save button should be disabled after saving a valid Work Number");

        await commonPatientInformation.addPhoneNumbers('Mobile', '909-523-87');
        since('Mobile Phone number must have at least 10 digits').expect($('Invalid phone number.')).toBeTruthy();
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should be disabled after entering an invalid Mobile number");
        await $('[ng-form=\"phone2\"]').$('md-icon.icon-trash.ng-scope.material-icons').click();
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should be disabled after deleting the invalid Mobile number. There were no other changes to the page");

        await commonPatientInformation.addPhoneNumbers('Mobile', '909-523-8967');
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "true", "Save button should be enabled after entering a valid Mobile Number without a Provider");
        await commonFunctions.clickSaveButton();
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should be disabled after saving a valid Mobile number without a Provider");
        await $('[ng-form=\"phone2\"]').$('md-icon.icon-trash.ng-scope.material-icons').click();

        await commonPatientInformation.addPhoneNumbers('Mobile', '714-223-1092', true);
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "true", "Save button should be enabled after entering a valid Mobile Number with a Provider");
        await commonFunctions.clickSaveButton();
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should be disabled after saving a valid Mobile number with a Provider");

        await commonPatientInformation.addPhoneNumbers('Fax', '539-987-5432');
        since('Fax Phone number must have at least 10 digits').expect($('Invalid phone number.')).toBeTruthy();
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should be disabled after entering an invalid Fax number");
        await $('[ng-form=\"phone3\"]').$('md-icon.icon-trash.ng-scope.material-icons').click();
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "false", "Save button should be disabled after deleting the invalid Fax number. There were no other changes to the page");

        await commonPatientInformation.addPhoneNumbers('Fax', '525-457-1094');
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "true", "Save button should be enabled after entering a valid Fax Number");
        await commonFunctions.clickSaveButton();
        await commonFunctions.saveButtonStatus(commonFunctions.elements.saveButtonLocation, "true", "Save button should be disabled after saving a valid Fax Number");

        await $('[ng-form=\"phone3\"]').$('md-icon.icon-trash.ng-scope.material-icons').click();
        await $('[ng-form=\"phone2\"]').$('md-icon.icon-trash.ng-scope.material-icons').click();
        await $('[ng-form=\"phone1\"]').$('md-icon.icon-trash.ng-scope.material-icons').click();
        await commonFunctions.clickSaveButton();
        
        ////TODO: Remove below once TX-410 is done//
        //await $('[phone-type=\"Home\"]').element(by.name('lineNumber')).click();
        //await $('[phone-type=\"Home\"]').element(by.name('lineNumber')).sendKeys(protractor.Key.BACK_SPACE);
        //await $('[phone-type=\"Home\"]').element(by.name('lineNumber')).sendKeys("3");
        //await commonFunctions.clickSaveButton();
        //TODO: Remove above once TX-410 is done//
    }).pend("Bypass until TX-492 and TX-813 are resolved "),

    it('Too many patient tabs causes patient icon to become unclickable (TX-395)', async function () {
        await commonPatientSearch.enterPatientSearchByIcon();
        await browser.wait(EC.elementToBeClickable($('button.cancel.md-button.md-ink-ripple')), 5000, 'Clicking on the Patient Search Icon failed and browser timed out');
        await commonPatientSearch.clearButtonClick();
        await browser.wait(EC.elementToBeClickable($(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation)), 5000, 'Clicking on the First Name Search box failed and browser timed out');
        await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).click();
        await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).sendKeys('C');
        await commonPatientSearch.searchButtonClick();
        await commonPatientSearch.enterPatientSearchByIcon();

        await browser.waitForAngularEnabled(false);
        try {
            for (let count = 0; count < 14; count++) {
                await element.all(by.repeater('(rowRenderIndex, row) in rowContainer.renderedRows track by $index')).get(count).click();
                await commonPatientSearch.enterPatientSearchByIcon();
            }
        }
        finally {
            await browser.waitForAngularEnabled(true);
        }
        
        since('Last Patient Name tab should not be visible').expect(await element.all(by.cssContainingText(commonPatientInformation.elements.patientTabLocation, commonPatientInformation.elements.searchFirstName.toUpperCase())).get(0).isDisplayed()).toBeFalsy();

        await $('md-next-button.ng-scope').click();

        await browser.wait(EC.elementToBeClickable(element.all(by.cssContainingText(commonPatientInformation.elements.patientTabLocation, commonPatientInformation.elements.searchFirstName.toUpperCase())).first()), 25000, 'Clicking on Next arrow icon failed and browser timed out');
        since('Clicking on the Next Arrow icon did not display last Patient Name tab').expect(element.all(by.cssContainingText(commonPatientInformation.elements.patientTabLocation, commonPatientInformation.elements.searchFirstName.toUpperCase())).first().isDisplayed()).toBeTruthy();
        await $('md-prev-button.ng-scope').click();

        await browser.wait(EC.elementToBeClickable(element.all(by.cssContainingText(commonPatientInformation.elements.patientTabLocation, commonPatientInformation.elements.searchFirstNameFirstScrollPosition)).first()), 25000, 'Clicking on the Previous arrow icon failed to display first patient and browser timed out');
        since('Clicking on the Previous Arrow icon did not display first Patient Name tab').expect(element.all(by.cssContainingText(commonPatientInformation.elements.patientTabLocation, commonPatientInformation.elements.searchFirstNameFirstScrollPosition)).first().isDisplayed()).toBeTruthy();
        since('Clicking on the Previous Arrow icon did not hide last Patient Name tab').expect(element.all(by.cssContainingText(commonPatientInformation.elements.patientTabLocation, commonPatientInformation.elements.searchFirstName.toUpperCase())).first().isDisplayed()).toBeFalsy();

        await commonPatientSearch.enterPatientSearchByIcon();
        since('With multiple patient tabs, clicking on the Patient Search Icon did not go to Patient Search').expect(element(by.cssContainingText('div.clnt-filter-title.ng-binding', 'Search Patients')).isDisplayed()).toBeTruthy();
    })
});