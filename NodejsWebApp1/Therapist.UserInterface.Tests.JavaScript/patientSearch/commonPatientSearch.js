﻿'use strict';

const EC = require('../expectedConditions');

module.exports = {
    elements: {
        firstNameParentLocation: "[ng-model=\"d.state.patientCriteria.firstName\"]",
        nameChildLocation: ".ng-scope.md-input",
        firstNameColumn: ".ui-grid-cell.ng-scope.ui-grid-disable-selection.ui-grid-coluiGrid-005",
        firstNameColumnGeneric: "div.ui-grid-cell.ng-scope.ui-grid-disable-selection",
        lastNameParentLocation: "[ng-model=\"d.state.patientCriteria.lastName\"]",
        lastNameColumn: ".ui-grid-cell.ng-scope.ui-grid-disable-selection.ui-grid-coluiGrid-006",
        contactNumberColumn: ".ui-grid-cell.ng-scope.ui-grid-disable-selection.ui-grid-coluiGrid-007",
        nextAppointmentColumn: ".ui-grid-cell.ng-scope.ui-grid-disable-selection.ui-grid-coluiGrid-008",
        lastAppointmentColumn: ".ui-grid-cell.ng-scope.ui-grid-disable-selection.ui-grid-coluiGrid-009",
        activePatientSliderClass: 'clnt-three-state-toggle[true-display-text="Active"] .md-thumb',
        middleNextAppointmentSliderClass: "md-slider.ng-pristine.ng-untouched.ng-valid.ng-isolate-scope.ng-not-empty",
        notScheduledNextAppointmentSlider: "md-slider.ng-untouched.ng-valid.ng-isolate-scope.ng-not-empty.ng-dirty.ng-valid-parse.md-min",
        rowRepeater: "(rowRenderIndex, row) in rowContainer.renderedRows track by $index",
        columnRepeater: "(colRenderIndex, col) in colContainer.renderedColumns track by col.uid"
    },
    
    enterPatientSearchByMenu: async function () {
        await browser.wait(EC.elementToBeClickable($('label[for="navmain"]')), 5000, 'Failed to display Main menu and browser timed out');
        await $('label[for="navmain"]').click();
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText('.toplevel', 'Patients'))), 5000, 'Patients menu option failed to be displayed');
        await element(by.cssContainingText('.toplevel', 'Patients')).click();
    },

    enterPatientSearchByIcon: async function () {
        await browser.wait(EC.elementToBeClickable($('md-icon.icon-user.ng-scope.material-icons')), 5000, 'Failed to display Patient Search icon and browser timed out');
        await $('md-icon.icon-user.ng-scope.material-icons').click();
    },

    searchButtonClick: async function () {
        await element(by.cssContainingText("span.ng-scope", "Search")).click();
        await browser.wait(EC.elementToBeClickable($$("[class=\"ui-grid-cell-contents ng-binding ng-scope\"]").first()), 5000, 'Patient Search results are not clickable and the browser timed out');
    },

    clearButtonClick: async function () {
        await $('button.cancel.md-button').click();
    },

    dragDropNextAppointmentSlider: function (xDragDrop) {
        return this.dragDropSlider($$("md-slider").get(0).$("div.md-thumb"), xDragDrop);
    },

    dragDropActivePatientSlider: function(xDragDrop) {
        return this.dragDropSlider($$("md-slider").get(1).$("div.md-thumb"), xDragDrop);
    },

    dragDropSlider: async function(elementLocator, xDragDrop) {
        // Drag'n'drop in sliders is very unreliable. It's not related to Selenium and can be seen during manual testing
        // Thus it's replaced with key navigation
        await browser.actions().mouseMove(elementLocator).perform();
        await browser.actions().mouseMove(elementLocator).click().perform();
        if (xDragDrop < 0) {
            await browser.actions().sendKeys(protractor.Key.LEFT).perform();
        }
        else if (xDragDrop > 0) {
            await browser.actions().sendKeys(protractor.Key.RIGHT).perform();
        }
    },

    verifySearchResultIsEmpty: async function () {
        let actualSearchResultString = element(by.cssContainingText('.clnt-label', 'Please enter your search criteria'));
        expect(await actualSearchResultString.isPresent()).toBeTruthy();
    },

    verifyGridValue: async function (searchCriteria, columnString) {
        let rowCount = await element.all(by.repeater('(rowRenderIndex, row) in rowContainer.renderedRows track by $index')).count();

        let actualSearchResultString = await $('span.clnt-label.ng-binding').getAttribute('innerText');        
        if (rowCount < 50) {
            since("Search Results rows displayed " + actualSearchResultString + "does not match expected " + rowCount)
                .expect(actualSearchResultString).toContain(rowCount);
        }
        else {
            since("Search Results rows displayed 50 does not match expected " + rowCount).expect("50").toContain(rowCount);
        }

        await element.all(by.repeater('(rowRenderIndex, row) in rowContainer.renderedRows track by $index')).each(async function (rows) {
            let cellText = await rows.all(by.tagName('div')).get(0).$(columnString).getText();
            expect(cellText.toUpperCase()).toContain(searchCriteria);
        });
    }
}
