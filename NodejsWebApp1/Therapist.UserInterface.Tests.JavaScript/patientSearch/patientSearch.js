'use strict';

require('jasmine2-custom-message');

const EC = require('../expectedConditions');

const performanceTesting = require('protractor-perf');
const commonFunctions = require('../common.js');
const commonPatientSearch = require('./commonPatientSearch.js');
const performanceTestingLog = new performanceTesting(protractor, browser);

commonFunctions.xmlOutputLogger('patientSearch');

describe('Patient Search', function() {

    // Enter Patient Search (TX-270)
    beforeAll(async function () {
        //await performanceTestingLog.start();
        await commonFunctions.validLogin();
        await commonPatientSearch.enterPatientSearchByMenu();
        // await performanceTestingLog.stop();
        // await commonFunctions.logPerformanceData("EnterPatientSearch", performanceTestingLog);
    });

    describe("initial state", function () {
        
        it('sets Active Patient slider default location (TX-270,TX-275)', async function () {
            expect(await $(commonPatientSearch.elements.activePatientSliderClass).isPresent()).toBeTruthy();
        });

        it('sets Next Appointment Patient slider default location (TX-270,TX-276)', async function () {
            expect(await $(commonPatientSearch.elements.middleNextAppointmentSliderClass).isPresent()).toBeTruthy();
        });

    });

    describe("behaviour", function() {

        afterEach(async function() {
            await commonPatientSearch.clearButtonClick();
        });

        it('allows to search patient by First Name (TX-269,TX-270,TX-271)', async function () {
            let searchFirstName = "Bob";
            let searchFirstNameMiscCaps = "BOb";

            //await performanceTestingLog.start();
            await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).click();
            await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).sendKeys(searchFirstName);

            
            await commonPatientSearch.searchButtonClick();
            await commonPatientSearch.verifyGridValue(searchFirstName.toUpperCase(), commonPatientSearch.elements.firstNameColumn);
            
            await commonPatientSearch.clearButtonClick();
            await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).click();
            await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).sendKeys(searchFirstNameMiscCaps);

            await commonPatientSearch.searchButtonClick();
            await commonPatientSearch.verifyGridValue(searchFirstNameMiscCaps.toUpperCase(), commonPatientSearch.elements.firstNameColumn);
            //await performanceTestingLog.stop();

            //await commonFunctions.logPerformanceData("PatientSearchByFirstName", performanceTestingLog);
        }),

        it('Patient Search by Preferred Name (TX-269,TX-270,TX-162)', async function () {
            let searchFirstName = "MIKE";
            let searchFirstNameMiscCaps = "mIKe";

            //performanceTestingLog.start();
            await commonPatientSearch.clearButtonClick();
            await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).click();
            await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).sendKeys(searchFirstName);
            await commonPatientSearch.searchButtonClick();
            await commonPatientSearch.verifyGridValue(searchFirstName.toUpperCase(), commonPatientSearch.elements.firstNameColumn);

            await commonPatientSearch.clearButtonClick();
            await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).click();
            await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).sendKeys(searchFirstNameMiscCaps);
            await commonPatientSearch.searchButtonClick();
            await commonPatientSearch.verifyGridValue(searchFirstNameMiscCaps.toUpperCase(), commonPatientSearch.elements.firstNameColumn);
            //performanceTestingLog.stop();

            //commonFunctions.logPerformanceData("PatientSearchByPreferredName", performanceTestingLog)
        });

        it('Patient Search by Last Name (TX-269,TX-270,TX-272)', async function () {
            let searchLastName = "SMITH";
            let searchLastNameMiscCaps = "sMiTH";

            await commonPatientSearch.clearButtonClick();
            await $(commonPatientSearch.elements.lastNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).click();
            await $(commonPatientSearch.elements.lastNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).sendKeys(searchLastName);
            await commonPatientSearch.searchButtonClick();
            await commonPatientSearch.verifyGridValue(searchLastName.toUpperCase(), commonPatientSearch.elements.lastNameColumn);

            await commonPatientSearch.clearButtonClick();
            await $(commonPatientSearch.elements.lastNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).click();
            await $(commonPatientSearch.elements.lastNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).sendKeys(searchLastNameMiscCaps);
            await commonPatientSearch.searchButtonClick();
            await commonPatientSearch.verifyGridValue(searchLastNameMiscCaps.toUpperCase(), commonPatientSearch.elements.lastNameColumn);
        });

        it('Patient Search by Contact Number (TX-269,TX-270,TX-273) ', async function () {
            let areaCode = "123";
            let preFix = "239";
            let lineNumber = "3463";

            await commonPatientSearch.clearButtonClick();

            await element(by.name('areaCode')).click();
            await element(by.name('areaCode')).sendKeys(areaCode);

            await element(by.name('prefix')).click();
            await element(by.name('prefix')).sendKeys(preFix);

            await element(by.name('lineNumber')).click();
            await element(by.name('lineNumber')).sendKeys(lineNumber);

            await commonPatientSearch.searchButtonClick();
            await commonPatientSearch.verifyGridValue(areaCode + "-" + preFix + "-" + lineNumber, commonPatientSearch.elements.contactNumberColumn);
        });

        it('Patient column sort (TX-162,TX-270,TX-271,TX-272,TX-273)', async function () {
            let searchFirstName = "k";
    
            await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).click();
            await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).sendKeys(searchFirstName);
            await commonPatientSearch.searchButtonClick();
            await commonPatientSearch.verifyGridValue(searchFirstName.toUpperCase(), commonPatientSearch.elements.firstNameColumn);
            
            await element(by.cssContainingText('span.ng-binding', 'First Name')).click();
            await commonFunctions.verifyGridSort(commonPatientSearch.elements.rowRepeater, commonPatientSearch.elements.columnRepeater, 0, true); 
            
            await element(by.cssContainingText('span.ng-binding', 'First Name')).click();
            await commonFunctions.verifyGridSort(commonPatientSearch.elements.rowRepeater, commonPatientSearch.elements.columnRepeater, 0, false);
    
            await element(by.cssContainingText('span.ng-binding', 'Last Name')).click();
            await commonFunctions.verifyGridSort(commonPatientSearch.elements.rowRepeater, commonPatientSearch.elements.columnRepeater, 1, true);
    
            await element(by.cssContainingText('span.ng-binding', 'Last Name')).click();
            await commonFunctions.verifyGridSort(commonPatientSearch.elements.rowRepeater, commonPatientSearch.elements.columnRepeater, 1, false);
    
            await element(by.cssContainingText('span.ng-binding', 'Contact Number')).click();
            await commonFunctions.verifyGridSort(commonPatientSearch.elements.rowRepeater, commonPatientSearch.elements.columnRepeater, 2, true);
    
            await element(by.cssContainingText('span.ng-binding', 'Contact Number')).click();
            await commonFunctions.verifyGridSort(commonPatientSearch.elements.rowRepeater, commonPatientSearch.elements.columnRepeater, 2, false);
        });
    
        it('Active Patient slider search (TX-269,TX-270, TX-275)', async function () {
            let activePatientSlider = $(commonPatientSearch.elements.activePatientSliderClass);
            let results = $('.clnt-ui-grid');
            let searchFirstName = "q";
            let activeSearchRecords = "2";
            let middleSearchRecords = "27";
            let notSearchRecords = "25";

            await commonPatientSearch.clearButtonClick();

            expect(await activePatientSlider.isPresent()).toBeTruthy();

            await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).click();
            await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).sendKeys(searchFirstName);

            await commonPatientSearch.searchButtonClick();
            await commonPatientSearch.verifyGridValue(searchFirstName.toUpperCase(), commonPatientSearch.elements.firstNameColumn);

            {
                let recordsFound = await $('span.clnt-label.ng-binding').getText();
                let numRecords = recordsFound.split(" ")[0];
                since("Slider bar, set to Active setting, for Active Patients expecting " + activeSearchRecords + " but returned " + numRecords).expect(numRecords).toEqual(activeSearchRecords);
            }

            await commonPatientSearch.dragDropActivePatientSlider(-90);
            
            await commonPatientSearch.searchButtonClick();
            await browser.wait(EC.visibilityOf(results), 5000, 'Results not showing');
            await commonPatientSearch.verifyGridValue(searchFirstName.toUpperCase(), commonPatientSearch.elements.firstNameColumn);

            {
                let recordsFound = await $('span.clnt-label.ng-binding').getText();
                let numRecords = recordsFound.split(" ")[0];
                since("Slider bar, set to middle setting, for Active Patients expecting " + middleSearchRecords + " but returned " + numRecords).expect(numRecords).toEqual(middleSearchRecords);
            }
            
            await commonPatientSearch.dragDropActivePatientSlider(-90);
            
            await commonPatientSearch.searchButtonClick();
            await commonPatientSearch.verifyGridValue(searchFirstName.toUpperCase(), commonPatientSearch.elements.firstNameColumn);
        
            {
                let recordsFound = await $('span.clnt-label.ng-binding').getText();
                let numRecords = recordsFound.split(" ")[0];
                since("Slider bar, set to Not setting, for Active Patients expecting " + notSearchRecords + " but returned " + numRecords).expect(numRecords).toEqual(notSearchRecords);
            }
        });

        it('Next Appointment slider search (TX-269,TX-270, TX-276)', async function () {
            let notScheduledNextAppointmentSlider = $(commonPatientSearch.elements.notScheduledNextAppointmentSlider);
            let middleNextAppointmentSlider = $(commonPatientSearch.elements.middleNextAppointmentSliderClass);
            let searchFirstName = "d";
            let scheduledSearchRecords = "1";
            let middleSearchRecords = "52";
            let notScheduledSearchRecords = "51";

            await commonPatientSearch.clearButtonClick();

            expect(await $(commonPatientSearch.elements.middleNextAppointmentSliderClass).isPresent()).toBeTruthy();

            await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).click();
            await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).sendKeys(searchFirstName);

            await commonPatientSearch.searchButtonClick();
            await commonPatientSearch.verifyGridValue(searchFirstName.toUpperCase(), commonPatientSearch.elements.firstNameColumn);

            {
                let recordsFound = await $('span.clnt-label.ng-binding').getText();
                let numRecords = recordsFound.split(" ")[0];
                since("Slider bar, set to middle setting, for Next Appointment expecting " + middleSearchRecords + " but returned " + numRecords).expect(numRecords).toEqual(middleSearchRecords);
            }

            await commonPatientSearch.dragDropNextAppointmentSlider(-25);
            await commonPatientSearch.searchButtonClick();
            await commonPatientSearch.verifyGridValue(searchFirstName.toUpperCase(), commonPatientSearch.elements.firstNameColumn);

            {
                let recordsFound = await $('span.clnt-label.ng-binding').getText();
                let numRecords = recordsFound.split(" ")[0];
                since("Slider bar, set to Not Scheduled setting, for Next Appointment expecting " + notScheduledSearchRecords + " but returned " + numRecords).expect(numRecords).toEqual(notScheduledSearchRecords);
            }

            await commonPatientSearch.clearButtonClick();
            await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).click();
            await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).sendKeys(searchFirstName);

            await commonPatientSearch.dragDropNextAppointmentSlider(25);
            await commonPatientSearch.searchButtonClick();
            await commonPatientSearch.verifyGridValue(searchFirstName.toUpperCase(), commonPatientSearch.elements.firstNameColumn);

            {
                let recordsFound = await $('span.clnt-label.ng-binding').getText();
                let numRecords = recordsFound.split(" ")[0];
                since("Slider bar, set to Scheduled setting, for Next Appointment expecting " + scheduledSearchRecords + " but returned " + numRecords).expect(numRecords).toEqual(scheduledSearchRecords);
            }
        });

        it('Patient Search with multiple criteria and Clear button functionality (TX-336, TX-278)', async function () {
            let searchFirstName = "Happy";
            let searchLastName = "Smurf";  
            let areaCode = "503";
            let preFix = "626";
            let lineNumber = "2017";
            let activePatientSliderDirty = $('md-slider.ng-untouched.ng-valid.ng-isolate-scope.ng-not-empty.ng-dirty.ng-valid-parse.md-max');
            let middleNextAppointmentSliderDirty = $('md-slider.ng-untouched.ng-valid.ng-isolate-scope.ng-not-empty.ng-dirty');

            await commonPatientSearch.clearButtonClick();
            await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).click();
            await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).sendKeys(searchFirstName);
            await $(commonPatientSearch.elements.lastNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).click();
            await $(commonPatientSearch.elements.lastNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).sendKeys(searchLastName);
            await element(by.name('areaCode')).click();
            await element(by.name('areaCode')).sendKeys(areaCode);
            await element(by.name('prefix')).click();
            await element(by.name('prefix')).sendKeys(preFix);
            await element(by.name('lineNumber')).click();
            await element(by.name('lineNumber')).sendKeys(lineNumber);

            await commonPatientSearch.searchButtonClick();
            await commonPatientSearch.verifyGridValue(searchFirstName.toUpperCase(), commonPatientSearch.elements.firstNameColumn);
            await commonPatientSearch.verifyGridValue(searchLastName.toUpperCase(), commonPatientSearch.elements.lastNameColumn);
            await commonPatientSearch.verifyGridValue(areaCode + "-" + preFix + "-" + lineNumber, commonPatientSearch.elements.contactNumberColumn);


            await commonPatientSearch.dragDropActivePatientSlider(-50);
            await commonPatientSearch.dragDropNextAppointmentSlider(-20);

            await commonPatientSearch.clearButtonClick();

            let firstNameTextValue = await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).getAttribute('value');
            since('First Name in the search field was not cleared after clicking CLEAR button').expect(firstNameTextValue).toBeFalsy();

            let lastNameTextValue = await $(commonPatientSearch.elements.lastNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).getAttribute('value');
            since('Last Name in the search field was not cleared after clicking CLEAR button').expect(lastNameTextValue).toBeFalsy();

            let contactNumbertextValue = await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).getAttribute('value');
            since('Contact Number in the search field was not cleared after clicking CLEAR button').expect(contactNumbertextValue).toBeFalsy();

            since('Active Patients slider did not reset after clicking the CLEAR button').expect(await $('md-slider.ng-untouched.ng-valid.ng-isolate-scope.ng-not-empty.ng-dirty.md-max').isPresent()).toBeTruthy();
            since('Next Appointment slider did not reset after clicking the CLEAR button').expect(await middleNextAppointmentSliderDirty.isPresent()).toBeTruthy();
        });
    });
});