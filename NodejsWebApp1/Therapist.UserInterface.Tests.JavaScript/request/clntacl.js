const sql = require("mssql/msnodesqlv8");

class Clntacl {

    constructor(clntAclConfig, connectionConfig) {
        this.clntAclConfig = Object.assign({}, clntAclConfig);
        this.connectionConfig = Object.assign({}, connectionConfig);
    }

    async open() {
        const config = Object.assign({}, this.connectionConfig, this.clntAclConfig);
        this.pool = new sql.ConnectionPool(config);
        await this.pool.connect(); 
    }

    async getConnectionConfig(database) {
        let queryResult = await this.pool.request().query(`
SELECT SQL_Server AS server
  FROM tblDatabases
  WHERE tblDatabases.DatabaseName = '${database}'
        `);
        if (queryResult.recordset.length === 0) {
            throw new Error(`Database ${database} has no references in clntacl`);
        }
        let connectionConfig = Object.assign({}, this.connectionConfig);
        connectionConfig.server = queryResult.recordset[0].server;
        connectionConfig.database = database;
        return connectionConfig;
    }

    async getTenantId(database) {
        let queryResult = await this.pool.request().query(`
SELECT DatabaseID AS tenantId
  FROM tblDatabases
  WHERE tblDatabases.DatabaseName = '${database}'
        `);
        if (queryResult.recordset.length === 0) {
            throw new Error(`Database ${database} has no references in clntacl`);
        }
        return queryResult.recordset[0].tenantId;
    }

    async getUserId(login) {
		const connectionConfig = {};
        let queryResult = await this.pool.request().query(`
SELECT UserID as userId
  FROM tblSystemUsers
  WHERE tblSystemUsers.UserName + '.' + tblSystemUsers.CustomerAbbreviation = '${login}'
        `);
        if (queryResult.recordset.length === 0) {
            throw new Error(`There is no Insight login ${login}`);
        }
		return queryResult.recordset[0].userId;
    }

    async close() {
        this.pool && await this.pool.close();
    }

}

module.exports = Clntacl;