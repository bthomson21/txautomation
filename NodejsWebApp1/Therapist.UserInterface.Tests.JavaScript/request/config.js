const fs = require("fs");

const defaultConfig = {
	clntacl: {
	    server: ".",
		database: "clntacl"
	},
	connection: {
		options: {
			trustedConnection: true
		}
	}
};

function readConfig(fileName) {
	try {
		return JSON.parse(fs.readFileSync(fileName, "utf8"));
	}
	catch (error) {
		if (error.code !== "ENOENT") {
			throw error;
		}
	}
}

function mergeConfigs(...configs) {
	let merged = {};
	for (let config of configs) {
		if (!config) continue;
		let clntacl = merged.clntacl;
		let password;
		if (config.connection) {
			password = config.connection.password;
			if ((password !== undefined) && (config.connection.keys().length === 1)) {
				delete config.connection;
			}
			else {
				password = undefined;
			}
		}
		Object.assign(merged, config);
		if (clntacl || config.clntacl) {
			merged.clntacl = Object.assign({}, clntacl, config.clntacl);
		}
		if (merged.connection && (password !== undefined)) {
			merged.connection.password = password;
		}
	}
	return merged;
}

const config = mergeConfigs(
	defaultConfig,
	readConfig("database.json"),
	readConfig("database.local.json"));

module.exports = config;