const fs = require("fs");
const sql = require("mssql/msnodesqlv8");
const Clntacl = require("./clntacl");
const config = require("./config");

const defaultPatientSpecs = {
    email: "anton.sitnikov@exactprosystems.com",
    namePrefix: "Miss",
    firstName: "Automaton",
    birthDate: "1970-12-12",
    phone: "5031111112",
    phone2: "503111111302",
    cellPhone: "5031111111",
    address: "708 SW Third Ave.",
    address2: "Suite 400",
    addressCity: "Portland",
    addressState: "OR",
    addressZip: "97204",
    maritalStatus: "Married",
    ssn: "654367484",
	preferredName: "Hanna",
    employmentStatus: "1" // TODO
};

const defaultCaseSpecs = {
	injuryDate: "2018-01-15 00:00:00.000"
};

const defaultAppointmentSpecs = {
	apptStartTime: "2018-01-15 03:00",
	appointmentType: "Standard Visit - 30",
	effectDate: "1899-12-30 00:00:00.000",
	expirationDate: "2100-01-01 00:00:00.000",
	patientRelationshipToGuarantor: "Self",
	guarantorFeeSchedule: "ALLEGIANCE",
	dateValue: "2018-01-15",
    appointmentLength: 30,
    appointmentStatus: "Arrived"
};

const defaultProcedureSpecs = {
    cptCode: "97026",
    durationMins: 30,
    isAccepted: 1
};

class Request {

    constructor(config) {
        this.config = Object.assign({}, config);
    }
	
    async newPatient(patientSpecs) {
        let connection = await this.$connect();
		let patient = await this.$enchancePatient(patientSpecs, connection);
		let queryResult = await connection.request().query(this.$withMetadata(`
DECLARE @NewID INT, @NewDTStamp DATETIME
EXEC pr_Client_INSERT @MetaData, '${patient.firstName}', '', '${patient.lastName}', '${patient.birthDate}', 'F', NULL, @NewID OUTPUT, @NewDTStamp OUTPUT
SELECT @NewID AS patientId, @NewDTStamp AS newDTStamp
		`));
		let patientId = queryResult.recordset[0].patientId;
		await connection.request().query(`
exec sp_executesql N'UPDATE tblClient
SET
	ClientNamePrefix = @P1, ClientEmail = @P2, ClientPhone = @P3, ClientPhone2 = @P4, ClientCellPhone = @P5, ClientAddress = @P6, ClientAddress2 = @P7, ClientAddressCity = @P8, ClientAddressState = @P9, ClientAddressZip = @P10, MaritalStatus = @P11, SocialSecurityNumber = @P12, EmploymentStatus = @P13, CellProviderID = @P14, ReminderTypeID = @P15, ClientPreferredName = @P16
WHERE
	ClientID = @P17',N'@P1 varchar(8000),@P2 varchar(8000),@P3 varchar(8000),@P4 varchar(8000),@P5 varchar(8000),@P6 varchar(8000),@P7 varchar(8000),@P8 varchar(8000),@P9 varchar(8000),@P10 varchar(8000),@P11 varchar(8000),@P12 varchar(8000),@P13 varchar(8000),@P14 tinyint,@P15 tinyint,@P16 varchar(8000), @P17 int','${patient.namePrefix}','${patient.email}','${patient.phone}','${patient.phone2}','${patient.cellPhone}','${patient.address}','${patient.address2}','${patient.addressCity}','${patient.addressState}','${patient.addressZip}','${patient.maritalStatus}','${patient.ssn}','1',1,1,'${patient.preferredName}',${patientId}
		`);
		await connection.request().query(this.$withMetadata(`
EXEC pr_IssueValidationForSubscriber @MetaData, ${patientId}
EXEC pr_CreateDefaultMedicareCaps @MetaData, ${patientId}, ${new Date().getFullYear()}
		`));
		patient.patientId = patientId;
        return patient;
    }

	async newCase(caseSpecs){
        let connection = await this.$connect();
		let casePatient = await this.$enchanceCase(caseSpecs, connection);
        let queryResult = await connection.request().query(this.$withMetadata(`
DECLARE @NewID INT
EXEC pr_Case_INSERT @MetaData, ${casePatient.patientId}, '${casePatient.caseName}', NULL, @NewID OUTPUT
SELECT @NewID AS caseId
        `));
		let caseId = queryResult.recordset[0].caseId;
        casePatient.caseId = caseId;
		await connection.request().query(this.$withMetadata(`
DECLARE @tblDiagnosis dbo.TT_tblDiagnosis
INSERT INTO @tblDiagnosis(ICDCode, ICDVersion, [Description]) VALUES ('A00.0', '10', 'Cholera due to Vibrio cholerae, biovar cholerae')
EXEC pr_tblDiagnosis_SAVE @MetaData, @tblDiagnosis, 0
DECLARE @ParameterTable_tblDiagnosedProblem TT_tblDiagnosedProblem
INSERT INTO @ParameterTable_tblDiagnosedProblem (DiagnosedProblemID,ProblemID,ICDCode,ICDVersion,DiagnosedOrder,DTStamp,ModifiedByUserID,TenantID) VALUES (NULL,${casePatient.caseId},'A00.0','10',1,NULL,NULL,NULL)
EXEC pr_tblDiagnosedProblem_SAVE_Out @MetaData, @ParameterTable_tblDiagnosedProblem
        `));
		
		await connection.request().query(this.$withMetadata(`
DECLARE @ParameterTable_tblProblem TT_tblProblem
INSERT INTO @ParameterTable_tblProblem
(ProblemID,ClientID,ProblemDescription,DateOfFirstVisit,ReferredBy,InjuryDate,PrimaryDiagnosedProblemID,Active,ExternalProblemID,ProblemNote,ADNevrStmt,ADRelInfo,AdDschDe,ADDschRn,ADWorkRl,ADAccident,StaffID,ADProgType,ADAtty,ADSignDate,ADAccState,ADRecvPmt,ADStmtGuar,DischargeDate,FacilityID,EmployerID,DTStamp,ModifiedByUserID,BenefitsVerified,CollectionCompleteDate,DisciplineID,PrimaryCareProviderID,PlaceOfServiceDefaultCode,ReferralSourceID,ReferralSourceStaffID,ReferralSourceClientID,ReferralSourceContactID,AdmissionType,TenantID) 
VALUES (${casePatient.caseId},${casePatient.patientId},'${casePatient.caseName}','${casePatient.injuryDate}',NULL,'${casePatient.injuryDate}',NULL,1,NULL,NULL,0,1,NULL,NULL,0,NULL,NULL,NULL,NULL,'${casePatient.injuryDate}',NULL,0,0,NULL,NULL,NULL,'2017-12-20 15:21:50.708', ${this.userId} ,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
EXEC pr_tblProblem_SAVE_Out @MetaData, @ParameterTable_tblProblem
        `));
		
		await connection.request().query(this.$withMetadata(`
DECLARE @Upserts [dbo].[TT_CaseStatus]
INSERT INTO @Upserts ( ProblemID, CaseStatusTypeID, EffectiveDate, ExpirationDate ) VALUES (${casePatient.caseId}, 0, '${casePatient.injuryDate}', NULL)
EXEC pr_CaseStatusForClient_SAVE @MetaData, @Upserts
        `));

        return casePatient;
    }
	
	async newAppointment(appointmentSpecs) {
        let connection = await this.$connect();
		let appointment = await this.$enchanceAppointment(appointmentSpecs, connection);
		
        let queryResult = await connection.request().query(this.$withMetadata(`
DECLARE @ResourceList dbo.IntTableType INSERT INTO @ResourceList SELECT ResourceID FROM tblResource WHERE ResourceID IN (-1)
DECLARE @StaffList dbo.IntTableType INSERT INTO @StaffList SELECT StaffID FROM tblStaff WHERE StaffID IN (-1)
DECLARE @Appts dbo.TT_Appts
DECLARE @ApptID INT
INSERT INTO @Appts ( ApptStartTime, ClinicID, AppointmentType, ApptStatusName, ApptLength, MainStaffID, PatientId, ProblemID, PlaceOfServiceCode )
	VALUES ('${appointment.apptStartTime}', ${appointment.clinicId}, '${appointment.appointmentType}', NULL, 30, ${appointment.staffId}, ${appointment.case.patientId}, ${appointment.case.caseId}, NULL )
EXEC pr_Appt_INSERT @MetaData, @Appts, NULL, @ResourceList, @StaffList, @ApptID OUTPUT
SELECT @ApptID as appointmentId
        `));
		appointment.appointmentId = queryResult.recordset[0].appointmentId;
        
        if (appointment.appointmentStatus != null) {
            await this.changeAppointmentStatus(appointment, appointment.appointmentStatus);
        }
		
		await connection.request().query(this.$withMetadata(`
DECLARE @DateList dbo.TT_DateList
INSERT INTO @DateList VALUES('${appointment.dateValue}')
DECLARE @StaffList dbo.IntTableType INSERT INTO @StaffList SELECT StaffID FROM tblStaff WHERE StaffID IN (${appointment.staffId},0)
EXEC pr_ScheduleDataSetsScheduleOnly_SELECT @MetaData, @DateList, @StaffList, ${appointment.clinicId}
		`));
		
		await connection.request().query(this.$withMetadata(`
DECLARE @ParameterTable_PayerMix TT_PayerMix
INSERT INTO @ParameterTable_PayerMix (ProblemID,EffectiveDate,PrimarySubscriberID,PrimaryInsurancePlanID,PrimaryMemberID,PrimaryGroupID,PrimaryPolicyID,PrimaryClaimID,PrimaryDeductible,PrimaryCopay,PrimaryMaximumBillableAmount,PrimaryMaximumVisitsAllowed,PrimaryCoinsurance,SecondarySubscriberID,SecondaryInsurancePlanID,SecondaryMemberID,SecondaryGroupID,SecondaryPolicyID,SecondaryClaimID,SecondaryDeductible,SecondaryCopay,SecondaryMaximumBillableAmount,SecondaryMaximumVisitsAllowed,SecondaryCoinsurance,TertiarySubscriberID,TertiaryInsurancePlanID,TertiaryMemberID,TertiaryGroupID,TertiaryPolicyID,TertiaryClaimID,TertiaryDeductible,TertiaryCopay,TertiaryMaximumBillableAmount,TertiaryMaximumVisitsAllowed,TertiaryCoinsurance,DTStamp,ModifiedByUserID,GuarantorClientID,GuarantorCompanyID,GuarantorContactID,PatientRelationshipToGuarantor,GuarantorFeeSchedule,MSPCode,PrimaryDocumentID,PrimaryDocumentTypeID,SecondaryDocumentID,SecondaryDocumentTypeID,TertiaryDocumentID,TertiaryDocumentTypeID,PrimaryDeductibleMet,TenantID,ExpirationDate,PrimaryVOBstatus,PrimaryVOBstatusDetail,SecondaryVOBstatus,SecondaryVOBstatusDetail,TertiaryVOBstatus,TertiaryVOBstatusDetail,PrimaryNetworkType,PrimaryOutOfPocketMet,PrimaryPreauthorizationRequired) 
VALUES (${appointment.case.caseId},'${appointment.effectDate}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,${appointment.case.patientId},NULL,NULL,'${appointment.patientRelationshipToGuarantor}','${appointment.guarantorFeeSchedule}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'${appointment.expirationDate}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0)
EXEC pr_PayerMix_SAVE_Out @MetaData, @ParameterTable_PayerMix
        `));
        
		if (appointment.chartTemplates) {
            const templates = appointment.chartTemplates;
            for (let index = 0; index <templates.length; ++index) {
                await connection.request().query(this.$withMetadata(`
EXEC tx.pr_CopyChartTemplateToVisit @MetaData, ${templates[index].chartTemplateId}, ${appointment.appointmentId}, ${appointment.staffId}
                `));
            }
		
			await connection.request().query(`
UPDATE dbo.VisitService
SET IsAccepted = 1
WHERE VisitID = ${appointment.appointmentId}
			`);
        }
        
        if (appointment.procedures && (appointment.procedures.length > 0)) {
            const procedures = appointment.procedures;
            for (let index = 0; index < procedures.length; ++index) {
                const procedure = procedures[index];
                await connection.request().query(this.$withMetadata(`
EXEC tx.pr_ChartServiceAppend_INSERT @MetaData, @VisitID=${appointment.appointmentId},@TopicItemID=${procedure.topicItemId},@VisitSideAnatomyID=1,@TreatedByStaffID=${appointment.staffId},@ServiceLengthInMinutes=${procedure.durationMins},@Measurement=N'',@Note=NULL,@CPTModifierCode=NULL,@CPTCode=${procedure.cptCode},@Group=N'',@ChartItemTypeID=9,@IsAccepted=${procedure.isAccepted}
                `));
            }
        }
		
		return appointment;
	}	
	
	async changeAppointmentStatus(appointment, appointmentStatus){
        let connection = await this.$connect();
        await connection.request().query(this.$withMetadata(`
DECLARE @ArrivalAndCopayStatus INT
EXEC pr_ApptStatus_UPDATE @MetaData, ${appointment.appointmentId}, '${appointmentStatus}', 1, 0, @ArrivalAndCopayStatus OUTPUT
SELECT @ArrivalAndCopayStatus AS ArrivalAndCopayStatus
        `));
		appointment.appointmentStatus = appointmentStatus;
    }
	
    async cleanup() {
        const promises = [];
        this.pool && promises.push(this.pool.close());
        this.clntacl && promises.push(this.clntacl.close());
        await Promise.all(promises);
    }

    // private methods
    async $connect() {   
        if (!this.pool) {
            if (!this.clntacl) {
                this.clntacl = new Clntacl(this.config.clntacl, this.config.connection);
                try {
                    await this.clntacl.open();
                }
                catch (error) {
                    delete this.clntacl;
                    throw error;
                }
            }

            let connectionConfig;
            [this.userId, this.tenantId, connectionConfig] = await Promise.all([
                this.clntacl.getUserId(this.config.login),
                this.clntacl.getTenantId(this.config.database),
                this.clntacl.getConnectionConfig(this.config.database)
            ]);
            this.pool = new sql.ConnectionPool(connectionConfig);
            try {
                await this.pool.connect();
            }
            catch (error) {
                delete this.pool;
                throw error;
            }
        }
		
        return this.pool;
    }

    $withMetadata(sql) {
        return `
DECLARE @MetaData TT_MetaData
INSERT INTO @MetaData (TenantID, UserID, TimeZoneOffset, UseDaylightSavingsTime) VALUES (${this.tenantId}, ${this.userId}, -6, 1)
        ` + sql;
    }

    async $enchancePatient(patientSpecs, connection) {
        patientSpecs = Object.assign({}, defaultPatientSpecs, patientSpecs);
        if (!patientSpecs.lastName) {
            patientSpecs.lastName = `aTest-${new Date().getTime()}-${Math.floor(Math.random() * 100)}`;
        }
        // TODO get CellProviderID and ReminderTypeID
        return patientSpecs;
    }
	
	async $enchanceCase(caseSpecs, connection){
        caseSpecs = Object.assign({}, defaultCaseSpecs, caseSpecs);
		if(!caseSpecs.caseName) {
			caseSpecs.caseName = `Test-ache-${new Date().getTime()}-${Math.floor(Math.random() * 100)}`;
		}
		
		if(!caseSpecs.patientId) {
			if(!caseSpecs.patient) { 
				throw new Error("Missing patient param");
			}
			else {
				caseSpecs.patientId = caseSpecs.patient.patientId;
			}
		}
        return caseSpecs;
    }
	
	async $enchanceAppointment(appointmentSpecs, connection) {
        let specs = Object.assign({}, defaultAppointmentSpecs, appointmentSpecs);

        if ((specs.case.caseName == null) || (specs.case.caseId == null)) {
            throw new Error("Case retrival isn't implemented");
        }

        if (specs.staffId == null) {
            let userId = specs.staffLogin ? await this.clntacl.getUserId(appointmentSpecs.staffLogin) : this.userId;
            let queryResult = await connection.request().query(`
SELECT StaffID as staffId
FROM dbo.tblUser
WHERE UserID=${userId}
            `);
            specs.staffId = queryResult.recordset[0].staffId;
        }

        if (specs.clinicId == null) {
            let queryResult = await connection.request().query(`
SELECT TOP 1 ClinicId AS clinicId
FROM dbo.tblClinicsForStaff
WHERE StaffId=${specs.staffId}
            `);
            specs.clinicId = queryResult.recordset[0].clinicId;
        }

        if (specs.chartTemplates) {
            let templatesWithoutIds = specs.chartTemplates.filter(template => template.chartTemplateId == null);
            if (templatesWithoutIds.length > 0) {
                let queryResults = await Promise.all(templatesWithoutIds.map(template => connection.request().query(`
SELECT ChartTemplateId AS chartTemplateId
FROM dbo.ChartTemplate
WHERE ChartTemplateCategory='${template.chartTemplateCategory}' AND ChartTemplateName='${template.chartTemplateName}'
                `)));
                for (let index = 0; index < queryResults.length; ++index) {
                    templatesWithoutIds[index].chartTemplateId = queryResults[index].recordset[0].chartTemplateId;
                }
            }
        }

        if (specs.procedures && (specs.procedures.length > 0)) {
            specs.procedures = await Promise.all(specs.procedures.map(procedure => this.$enchanceProcedure(procedure, connection)));
        }
		
		return specs;
    }

	async $enchanceProcedure(procedureSpecs, connection) {
        let specs = Object.assign({}, defaultProcedureSpecs, procedureSpecs);
		let topicItemQueryResult;
		if (specs.topicItemId == null) {
			topicItemQueryResult = await connection.request().query(`
SELECT TOP 1 TopicItemID AS topicItemId
FROM dbo.TopicItem
WHERE CPTCode='${specs.cptCode}'
			`);
        }
        specs.topicItemId = topicItemQueryResult.recordset[0].topicItemId;
		return specs;
    }
}

module.exports = new Request(config);