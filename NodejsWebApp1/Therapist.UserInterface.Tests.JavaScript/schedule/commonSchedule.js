﻿'use strict';

const EC = require('../expectedConditions');

module.exports = {

    elements: {
        dayName: "day_tab",
        weekName: "week_tab",
        monthName: "month_tab",
        todayButtonClass : "div.dhx_cal_today_button",
        previousButtonClass : "div.dhx_cal_prev_button",
        nextButtonClass: "div.dhx_cal_next_button",
        timeBlockInformationText: "For QA Automated Testing",
        timeBlockInformationDayWeekViewClass: "div.dhx_cal_event.staff-schedule-event.event-appt-reserved-working",
        timeBlockInformationMonthViewClass: "div.dhx_cal_event_clear.dhx_cal_event_line_start.dhx_cal_event_line_end.staff-schedule-event.event-appt-reserved-working",
        appointmentName: "GAILY Bronson",
        arrivedAppointmentName: "TREEe SWELTER",
        arrivedAppointmentInformatinDayWeekViewClass: "div.dhx_cal_event.staff-schedule-event.event-appt-arrived",
        appointmentInformationDayWeekViewClass: "div.dhx_cal_event.staff-schedule-event.event-appt-standard",
        appointmentInformationMonthViewClass: "div.dhx_cal_event_clear.dhx_cal_event_line_start.dhx_cal_event_line_end.staff-schedule-event.event-appt-standard"

    },

    enterScheduleView: async function () {
        await browser.wait(EC.elementToBeClickable($('label[for="navmain"]')), 5000, 'Failed to click on main menu and browser timed out');
        await $('label[for="navmain"]').click();
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText('label.toplevel.ng-binding.ng-scope', 'Schedule'))), 5000, 'Failed to enter Schedule View via menu and browser timed out');
        await element(by.cssContainingText('label.toplevel.ng-binding.ng-scope', 'Schedule')).click();
    },

    compareScheduleViewDay: async function (today) {
        let dayViewFullDate = await element(by.css('div.dhx_scale_bar')).getText();
        let dayViewDayOnly = dayViewFullDate.split(" ")[0];
        since("Day: " + today + " does not match Schedule day: " + dayViewDayOnly).expect(today.toString()).toEqual(dayViewDayOnly);            
    },

    compareScheduleViewWeek: async function (whichWeek, startOfWeekDate, endOfWeekDate) {
        let weekViewFullDate = await element(by.css('div.dhx_cal_date')).getText();
        since("Start of " + whichWeek + " week date: " + startOfWeekDate + " can not be matched in the Schedule week: " + weekViewFullDate).expect(weekViewFullDate.match(startOfWeekDate)).not.toBeNull();
        since("End of " + whichWeek + " week date: " + endOfWeekDate + " can not be matched in the Schedule week: " + weekViewFullDate).expect(weekViewFullDate.match(endOfWeekDate)).not.toBeNull();
    },

    compareScheduleViewMonth: async function (month) {
        let monthViewFullDate = await element(by.css('div.dhx_cal_date')).getText();
        let monthViewDayOnly = monthViewFullDate.split(" ")[0];
        since("Month: " + monthViewDayOnly + " does not match Schedule month: " + month).expect(monthViewDayOnly).toEqual(month);
    },

    getModulos: async function (num, mod) {
        let remain = num % mod;
        return Math.floor(remain >= 0 ? remain : remain + mod);
    }

};