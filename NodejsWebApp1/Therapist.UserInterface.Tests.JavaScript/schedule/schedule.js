﻿'use strict'

const EC = require('../expectedConditions');

const commonFunctions = require('../common.js');
const commonPatientSearch = require('../patientSearch/commonPatientSearch.js');
const commonSchedule = require('./commonSchedule.js');
const listOfMonths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
const listOfDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

commonFunctions.xmlOutputLogger('schedule');


describe('Schedule View', function () {

    it('Verify no All Clinics drop down with 1 clinic (TX-163)', async function () {
        await commonFunctions.validLogin(commonFunctions.login.therapistOneClinicUserName);
        await browser.wait(EC.elementToBeClickable(element(by.name(commonSchedule.elements.dayName))), 5000, 'Failed to enter to Schedule view with 1 clinic and browser timed out');
        since('Therapist with 1 clinic has the All clinic drop down present').expect(await element.all(by.cssContainingText('div.md-text.ng-binding', 'All Clinics')).first().isPresent()).toBeFalsy();
        await browser.restart();
    }).pend("Causes jasmine reporter to stop logging"),

    it('Day view is default (TX-281)', async function () {
        await commonFunctions.validLogin();
        await browser.wait(EC.elementToBeClickable(element(by.name(commonSchedule.elements.dayName))), 5000, 'Failed to enter to Schedule view with mulitple clinics and browser timed out');
        expect(await element(by.cssContainingText('div.dhx_cal_tab.dhx_cal_tab_first.active', 'Day')).isPresent()).toBeTruthy();
    }),

    it('User Name displayed (TX-277)', async function () {
        let userName = await element.all(by.id('userGreeting')).getAttribute("textContent");
        since("Username is not displayed correctly in the Schedule View (upper right corner)").expect(userName).toEqual([commonFunctions.login.therapistUserName]);
    }),

    it('All Clinics drop down has multiple clinics', async function () {
        await browser.wait(EC.elementToBeClickable(element.all(by.cssContainingText('div.md-text.ng-binding', 'All Clinics')).first()), 5000, 'All Clinics drop down menu is not available to click and browser timed out');
        await element.all(by.cssContainingText('div.md-text.ng-binding', 'All Clinics')).first().click();
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText('div.md-text.ng-binding', 'Alpine Physical Therapy - North'))), 5000, 'All Clinics drop down menu did not expand and browser timed out');

        since('Alpine Physical Therapy - Downtown is not present in the All Clinics drop down menu').expect(await element(by.cssContainingText('div.md-text.ng-binding', 'Alpine Physical Therapy - Downtown')).isDisplayed()).toBeTruthy();
        since('Alpine Physical Therapy - North is not present in the All Clinics drop down menu').expect(await element(by.cssContainingText('div.md-text.ng-binding', 'Alpine Physical Therapy - North')).isDisplayed()).toBeTruthy();
        since('Alpine Physical Therapy - South is not present in the All Clinics drop down menu').expect(await element(by.cssContainingText('div.md-text.ng-binding', 'Alpine Physical Therapy - South')).isDisplayed()).toBeTruthy();

        since('Clinic drop down does not contain All Clinics plus 3 clinics').expect(await element.all(by.repeater('item in d.collection')).count()).toBe(4);
        await element(by.cssContainingText(commonFunctions.elements.ngRepeater, 'All Clinics')).click();
    }),

    it('All Clinics drop down changes schedule view (TX-348)', async function () {
        await element.all(by.cssContainingText('div.md-text.ng-binding', 'All Clinics')).first().click();
        await browser.wait(EC.elementToBeClickable(element.all(by.cssContainingText('div.md-text.ng-binding', 'Alpine Physical Therapy - South')).first()), 5000, 'All Clinics drop down menu did not expand and browser timed out');
        await element.all(by.cssContainingText(commonFunctions.elements.ngRepeater, 'Alpine Physical Therapy - South')).first().click();

        since('Appointment not associated with this Clinic is displayed in the schedule').expect(await element(by.cssContainingText(commonSchedule.elements.appointmentInformationDayWeekViewClass, commonSchedule.elements.appointmentName)).isPresent()).toBeFalsy();
        await browser.wait(EC.elementToBeClickable(element.all(by.cssContainingText('div.md-text.ng-binding', 'Alpine Physical Therapy - South')).first()), 5000, 'Can not select Alpine Physical Therapy - South from drop down menu and browser timed out');
        await element.all(by.cssContainingText('div.md-text.ng-binding', 'Alpine Physical Therapy - South')).first().click();
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(commonFunctions.elements.ngRepeater, 'All Clinics'))), 5000, 'Can not select All Clinics from drop down menu and browser timed out');
        await element(by.cssContainingText(commonFunctions.elements.ngRepeater, 'All Clinics')).click();
        await browser.wait(EC.elementToBeClickable(element(by.name(commonSchedule.elements.dayName))), 5000, 'After selecting All Clinics the browser timed out');
    }),

    it('verify day next/previous/today tabs work (tx-359)', async function () {
        let currentDate = new Date(new Date().getTime());
        let today = currentDate.getDay();
        let yesterday = await commonSchedule.getModulos(today - 1, 7);
        let tomorrow = await commonSchedule.getModulos(today + 1, 7);
        await commonSchedule.compareScheduleViewDay(listOfDays[today]);
        await $(commonSchedule.elements.previousButtonClass).click();
        await commonSchedule.compareScheduleViewDay(listOfDays[yesterday]);
        await $(commonSchedule.elements.nextButtonClass).click();
        await $(commonSchedule.elements.nextButtonClass).click();
        await commonSchedule.compareScheduleViewDay(listOfDays[tomorrow]);

    }),

    it('Verify Week Next/Previous/Today tabs work (TX-359)', async function () {
        let currentDate = new Date(new Date().getTime());
        let todayDate = currentDate.getDate(); //May 9...will return 9
        let todayDay = currentDate.getDay();  //sunday = 0 ... Saturday = 6
        let hoursInDay = 24;
        let secondsInMinute = 60;
        let minutesInHours = 60;
        let measureOftimeMilliseconds = 1000;

        let endOfWeekDate = new Date();
        endOfWeekDate.setDate(endOfWeekDate.getDate() + (6 - todayDay));
        let beginOfWeekDate = (new Date(currentDate - hoursInDay * minutesInHours * secondsInMinute * measureOftimeMilliseconds * todayDay)).getDate();

        let endOfLastWeekDate = new Date();
        endOfLastWeekDate.setDate((endOfLastWeekDate.getDate() + (6 - todayDay)) - 7);
        let beginOfLastWeekDate = (new Date(currentDate - hoursInDay * minutesInHours * secondsInMinute * measureOftimeMilliseconds * (todayDay + 7))).getDate();

        let endOfNextWeekDate = new Date();
        endOfNextWeekDate.setDate((endOfNextWeekDate.getDate() + (6 - todayDay)) + 7);
        let beginOfNextWeekDate = (new Date(endOfNextWeekDate - hoursInDay * minutesInHours * secondsInMinute * measureOftimeMilliseconds * 6)).getDate();

        await $(commonSchedule.elements.todayButtonClass).click();
        await element(by.name(commonSchedule.elements.weekName)).click();
        since('Week Tab is not the active tab').expect(await element(by.cssContainingText('div.dhx_cal_tab.active', 'Week')).isPresent()).toBeTruthy();

        await commonSchedule.compareScheduleViewWeek("current", "^" + beginOfWeekDate, "– " + endOfWeekDate.getDate());
        await $(commonSchedule.elements.previousButtonClass).click();
        await commonSchedule.compareScheduleViewWeek("last", "^" + beginOfLastWeekDate, "– " + endOfLastWeekDate.getDate());
        await $(commonSchedule.elements.nextButtonClass).click();
        await $(commonSchedule.elements.nextButtonClass).click();
        await commonSchedule.compareScheduleViewWeek("next", "^" + beginOfNextWeekDate, "– " + endOfNextWeekDate.getDate());
    }),

    it('Verify Month Next/Previous/Today tabs work (TX-359)', async function () {
        let currentDate = new Date(new Date().getTime());

        let currentMonth = await listOfMonths[currentDate.getMonth()];
        let lastMonth = await listOfMonths[await commonSchedule.getModulos(currentDate.getMonth() - 1, 12)];
        let nextMonth = await listOfMonths[await commonSchedule.getModulos(currentDate.getMonth() + 1, 12)];

        await element(by.name(commonSchedule.elements.monthName)).click();
        await $(commonSchedule.elements.todayButtonClass).click();
        since('Month Tab is not the active tab').expect(await element(by.cssContainingText('div.dhx_cal_tab.dhx_cal_tab_last.active', 'Month')).isPresent()).toBeTruthy();

        await commonSchedule.compareScheduleViewMonth(currentMonth);
        await $(commonSchedule.elements.previousButtonClass).click();
        await commonSchedule.compareScheduleViewMonth(lastMonth);
        await $(commonSchedule.elements.nextButtonClass).click();
        await $(commonSchedule.elements.nextButtonClass).click();
        await commonSchedule.compareScheduleViewMonth(nextMonth);
        await $(commonSchedule.elements.todayButtonClass).click();
        await element(by.name(commonSchedule.elements.dayName)).click();
    }),

    it('Time Block Information present and read-only (TX-335)', async function () {
        await element(by.cssContainingText(commonSchedule.elements.timeBlockInformationDayWeekViewClass, commonSchedule.elements.timeBlockInformationText)).click();
        await browser.wait(EC.elementToBeClickable(element(by.id('timeBlockDetailsDialog'))), 5000, 'Time Block Information, Pop Up failed to display and browser timed out');

        // FIXME the following check block doesn`t work as intended by author
        since('Description field is not read-only').expect(await $('[field-label=\"Description\"]').getAttribute('ng-disabled')).toBe('vm.isReadonly');
        since('Clinic field is not read-only').expect(await $('[field-label=\"Clinic\"]').getAttribute('ng-disabled')).toBe('vm.isReadonly');
        since('Block Time For field is not read-only').expect(await $('[field-label=\"Block Time For\"]').getAttribute('ng-disabled')).toBe('vm.isReadonly');
        since('Date field is not read-only').expect(await $('[field-label=\"Date\"]').getAttribute('ng-disabled')).toBe('vm.isReadonly');
        since('Start Time field is not read-only').expect(await $('[field-label=\"Start Time\"]').getAttribute('ng-disabled')).toBe('vm.isReadonly');
        since('End Time field is not read-only').expect(await $('[field-label=\"End Time\"]').getAttribute('ng-disabled')).toBe('vm.isReadonly');
        since('Duration field is not read-only').expect(await $('[field-label=\"Duration\"]').getAttribute('ng-disabled')).toBe('vm.isReadonly');

        since('Description field can not be empty').expect(await $('[field-label=\"Description\"]').getText()).not.toBe('');
        since('Clinic field can not be empty').expect(await $('[field-label=\"Clinic\"]').getText()).not.toBe('');
        since('Block Time For field can not be empty').expect(await $('[field-label=\"Block Time For\"]').getText()).not.toBe('');
        since('Date field can not be empty').expect(await $('[field-label=\"Date\"]').getText()).not.toBe('');
        since('Start Time field can not be empty').expect(await $('[field-label=\"Start Time\"]').getText()).not.toBe('');
        since('End Time field can not be empty').expect(await $('[field-label=\"End Time\"]').getText()).not.toBe('');
        since('Duration field can not be empty').expect(await $('[field-label=\"Duration\"]').getText()).not.toBe('');

        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText('span.ng-scope', 'Close'))), 5000, 'Time Block Information Pop Up failed to close and browser timed out');
        await element(by.cssContainingText('span.ng-scope', 'Close')).click();
        await browser.wait(EC.invisibilityOf(element(by.id('timeBlockDetailsDialog'))), 5000, 'Time Block Information Pop Up failed to close and browser timed out');
        since('Time Block Information pop up did not close after clicking the Close button').expect(await (element(by.id('timeBlockDetailsDialog'))).isPresent()).toBe(false);
    }),

    it('Appointment Information present and read-only (TX-335)', async function () {
        await element(by.cssContainingText(commonSchedule.elements.appointmentInformationDayWeekViewClass, commonSchedule.elements.appointmentName)).click();
        await browser.wait(EC.elementToBeClickable(element(by.id('timeBlockDetailsDialog'))), 5000, 'Appointment Information Pop Up failed to display and browser timed out');

        // FIXME the following check block doesn`t work as intended by author
        since('Patient field is not read-only').expect(await $('[field-label=\"Patient\"]').getAttribute('ng-disabled')).toBe('vm.isReadonly');
        since('Status field is not read-only').expect(await $('[field-label=\"Status\"]').getAttribute('ng-disabled')).toBe('vm.isReadonly');
        since('Appointment Type field is not read-only').expect(await $('[field-label=\"Appointment Type\"]').getAttribute('ng-disabled')).toBe('vm.isReadonly');
        since('Date field is not read-only').expect(await $('[field-label=\"Date\"]').getAttribute('ng-disabled')).toBe('vm.isReadonly');
        since('Start field is not read-only').expect(await $('[field-label=\"Start\"]').getAttribute('ng-disabled')).toBe('vm.isReadonly');
        since('End field is not read-only').expect(await $('[field-label=\"End\"]').getAttribute('ng-disabled')).toBe('vm.isReadonly');
        since('Duration field is not read-only').expect(await $('[field-label=\"Duration\"]').getAttribute('ng-disabled')).toBe('vm.isReadonly');
        since('Case field is not read-only').expect(await $('[field-label=\"Case\"]').getAttribute('ng-disabled')).toBe('vm.isReadonly');
        since('Clinic field is not read-only').expect(await $('[field-label=\"Clinic\"]').getAttribute('ng-disabled')).toBe('vm.isReadonly');
        since('Therapist field is not read-only').expect(await $('[field-label=\"Therapist\"]').getAttribute('ng-disabled')).toBe('vm.isReadonly');

        since('Patient field can not be empty').expect(await $('[field-label=\"Patient\"]').getText()).not.toBe('');
        since('Status field can not be empty').expect(await $('[field-label=\"Status\"]').getText()).not.toBe('');
        since('Appointment Type field can not be empty').expect(await $('[field-label=\"Appointment Type\"]').getText()).not.toBe('');
        since('Date field can not be empty').expect(await $('[field-label=\"Date\"]').getText()).not.toBe('');
        since('Start field can not be empty').expect(await $('[field-label=\"Start\"]').getText()).not.toBe('');
        since('End field can not be empty').expect(await $('[field-label=\"End\"]').getText()).not.toBe('');
        since('Duration field can not be empty').expect(await $('[field-label=\"Duration\"]').getText()).not.toBe('');
        since('Case field can not be empty').expect(await $('[field-label=\"Case\"]').getText()).not.toBe('');
        since('Clinic field can not be empty').expect(await $('[field-label=\"Clinic\"]').getText()).not.toBe('');
        since('Therapist field can not be empty').expect(await $('[field-label=\"Therapist\"]').getText()).not.toBe('');

        await element(by.cssContainingText('span.ng-scope', 'Close')).click();
        await browser.wait(EC.invisibilityOf(element(by.id('timeBlockDetailsDialog'))), 5000, 'Appointment Information Pop Up failed to close and browser timed out');
        since('Appointment Information, in Day View, pop up did not close after clicking the Close button').expect(await (element(by.id('timeBlockDetailsDialog'))).isPresent()).toBeFalsy();
    }),

    it('Open Time Block Information from Day/Week/Month view', async function () {
        await element(by.cssContainingText(commonSchedule.elements.timeBlockInformationDayWeekViewClass, commonSchedule.elements.timeBlockInformationText)).click();
        await browser.wait(EC.elementToBeClickable(element(by.id('timeBlockDetailsDialog'))), 5000, 'Time Block Information, in Day View, Pop Up failed to display and browser timed out');
        await element(by.cssContainingText('span.ng-scope', 'Close')).click();
        await browser.wait(EC.invisibilityOf(element(by.id('timeBlockDetailsDialog'))), 5000, 'Time Block Information, in Day View, Pop Up failed to close and browser timed out');
        since('Time Block Information, in Day View, pop up did not close after clicking the Close button').expect(await (element(by.id('timeBlockDetailsDialog'))).isPresent()).toBeFalsy();

        // FIXME the following check block doesn`t work as intended by author
        await element(by.name(commonSchedule.elements.weekName)).click();
        await element.all(by.cssContainingText(commonSchedule.elements.timeBlockInformationDayWeekViewClass, commonSchedule.elements.timeBlockInformationText)).first().click();
        await browser.wait(EC.elementToBeClickable(element(by.id('timeBlockDetailsDialog'))), 5000, 'Time Block Information, in Week View, Pop Up failed to display and browser timed out');
        await element(by.cssContainingText('span.ng-scope', 'Close')).click();
        await browser.wait(EC.invisibilityOf(element(by.id('timeBlockDetailsDialog'))), 5000, 'Time Block Information, in Week View, Pop Up failed to close and browser timed out');
        since('Time Block Information, in Week View, pop up did not close after clicking the Close button').expect(await (element(by.id('timeBlockDetailsDialog'))).isPresent()).toBeFalsy();

        // FIXME the following check block doesn`t work as intended by author
        await element(by.name(commonSchedule.elements.monthName)).click();
        await element.all(by.cssContainingText(commonSchedule.elements.timeBlockInformationMonthViewClass, commonSchedule.elements.timeBlockInformationText)).first().click();
        await browser.wait(EC.elementToBeClickable(element(by.id('timeBlockDetailsDialog'))), 5000, 'Time Block Information, in Month View, Pop Up failed to display and browser timed out');
        await element(by.cssContainingText('span.ng-scope', 'Close')).click();
        await browser.wait(EC.invisibilityOf(element(by.id('timeBlockDetailsDialog'))), 5000, 'Time Block Information, in Month View, Pop Up failed to close and browser timed out');
        since('Time Block Information, in Month View, pop up did not close after clicking the Close button').expect(await (element(by.id('timeBlockDetailsDialog'))).isPresent()).toBeFalsy();

        await element(by.name(commonSchedule.elements.dayName)).click();
    }),

    it('Open Appointment Information from Day/Week/Month view', async function () {
        await element(by.cssContainingText(commonSchedule.elements.appointmentInformationDayWeekViewClass, commonSchedule.elements.appointmentName)).click();
        await browser.wait(EC.elementToBeClickable(element(by.id('timeBlockDetailsDialog'))), 5000, 'Appointment Information, in Day View, Pop Up failed to display and browser timed out');
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText('span.ng-scope', 'Close'))), 5000, 'Close button can not be clicked in Appointment Information, in Day View, and browser timed out');
        await element(by.cssContainingText('span.ng-scope', 'Close')).click();
        await browser.wait(EC.invisibilityOf(element(by.id('timeBlockDetailsDialog'))), 5000, 'Appointment Information, in Day View, Pop Up failed to close and browser timed out');
        since('Appointment Information, in Day View, pop up did not close after clicking the Close button').expect(await (element(by.id('timeBlockDetailsDialog'))).isPresent()).toBeFalsy();

        await element(by.name(commonSchedule.elements.weekName)).click();
        await element.all(by.cssContainingText(commonSchedule.elements.appointmentInformationDayWeekViewClass, commonSchedule.elements.appointmentName)).first().click();
        await browser.wait(EC.elementToBeClickable(element(by.id('timeBlockDetailsDialog'))), 5000, 'Appointment Information, in Week View, Pop Up failed to display and browser timed out');
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText('span.ng-scope', 'Close'))), 5000, 'Close button can not be clicked in Appointment Information, in Week View, and browser timed out');
        await element(by.cssContainingText('span.ng-scope', 'Close')).click();
        await browser.wait(EC.invisibilityOf(element(by.id('timeBlockDetailsDialog'))), 5000, 'Time Block Information, in Week View, Pop Up failed to close and browser timed out');
        since('Appointment Information, in Week View, pop up did not close after clicking the Close button').expect(await (element(by.id('timeBlockDetailsDialog'))).isPresent()).toBeFalsy();

        await element(by.name(commonSchedule.elements.monthName)).click();
        await element.all(by.cssContainingText(commonSchedule.elements.appointmentInformationMonthViewClass, commonSchedule.elements.appointmentName)).first().click();
        await browser.wait(EC.elementToBeClickable(element(by.id('timeBlockDetailsDialog'))), 5000, 'Appointment Information, in Month View, Pop Up failed to display and browser timed out');
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText('span.ng-scope', 'Close'))), 5000, 'Close button can not be clicked in Appointment Information, in Month View, and browser timed out');
        await element(by.cssContainingText('span.ng-scope', 'Close')).click();
        await browser.wait(EC.invisibilityOf(element(by.id('timeBlockDetailsDialog'))), 5000, 'Appointment Information, in Month View, Pop Up failed to close and browser timed out');
        since('Appointment Information, in Month View, pop up did not close after clicking the Close button').expect(await (element(by.id('timeBlockDetailsDialog'))).isPresent()).toBeFalsy();

        await element(by.name(commonSchedule.elements.dayName)).click();
    }),

    it('Visit Doc Close button requires multiple clicks (TX-334, TX-362)', async function () {
        let firstClinicDropDownText = await element.all(by.cssContainingText('div.md-text.ng-binding', 'All Clinics')).getAttribute("textContent");
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText('div.dhx_cal_event.staff-schedule-event.event-appt-standard', commonSchedule.elements.appointmentName))), 5000, 'Appointment is not clickable the first time and browser timed out');
        await element(by.cssContainingText('div.dhx_cal_event.staff-schedule-event.event-appt-standard', commonSchedule.elements.appointmentName)).click();
        await browser.wait(EC.elementToBeClickable(element(by.id('timeBlockDetailsDialog'))), 5000, 'First Appointment Information Pop Up failed to display and browser timed out');
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText('span.ng-scope', 'Close'))), 5000, 'First Appointment Information Close button can not be clicked and browser timed out');
        await element(by.cssContainingText('span.ng-scope', 'Close')).click();

        await browser.wait(EC.invisibilityOf(element(by.id('timeBlockDetailsDialog'))), 5000, 'First Appointment Information Pop Up failed to close and browser timed out');
        since('First Appointment Information pop up did not close after clicking the Close button').expect(await (element(by.id('timeBlockDetailsDialog'))).isPresent()).toBeFalsy();
        await commonPatientSearch.enterPatientSearchByIcon();
        await commonSchedule.enterScheduleView();

        let secondClinicDropDownText = await element.all(by.cssContainingText('div.md-text.ng-binding', 'All Clinics')).getAttribute("textContent");
        since("Clinic drop down text changed").expect(secondClinicDropDownText).toEqual(firstClinicDropDownText);

        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText('div.dhx_cal_event.staff-schedule-event.event-appt-standard', commonSchedule.elements.appointmentName))), 5000, 'Appointment is not clickable the second time and browser timed out');
        await element(by.cssContainingText('div.dhx_cal_event.staff-schedule-event.event-appt-standard', commonSchedule.elements.appointmentName)).click();
        await browser.wait(EC.elementToBeClickable(element(by.id('timeBlockDetailsDialog'))), 5000, 'Second Appointment Information Pop Up failed to display and browser timed out');
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText('span.ng-scope', 'Close'))), 5000, 'Second Appointment Information Close button can not be clicked and browser timed out');
        await element(by.cssContainingText('span.ng-scope', 'Close')).click();

        await browser.wait(EC.invisibilityOf(element(by.id('timeBlockDetailsDialog'))), 5000, 'Second Appointment Information Pop Up failed to close and browser timed out');
        since('Second Appointment Information pop up did not close after clicking the Close button').expect(await (element(by.id('timeBlockDetailsDialog'))).isPresent()).toBeFalsy();
    }),

    it('Not able to open Visit Doc from un-arrived appointment in Appointment Information pop up', async function () {
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(commonSchedule.elements.appointmentInformationDayWeekViewClass, commonSchedule.elements.appointmentName))), 5000, 'Appointment is not clickable and browser timed out');
        await element(by.cssContainingText(commonSchedule.elements.appointmentInformationDayWeekViewClass, commonSchedule.elements.appointmentName)).click();
        await browser.wait(EC.elementToBeClickable(element(by.id('timeBlockDetailsDialog'))), 5000, 'Appointment Information Pop Up failed to display and browser timed out');

        since("Go to visit button is enabled for an un-arrived appointment").expect(await element(by.cssContainingText('span.ng-scope', 'Go to visit')).isDisplayed()).toBeFalsy();
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText('span.ng-scope', 'Close'))), 5000, 'Second Appointment Information Close button can not be clicked and browser timed out');
        await element(by.cssContainingText('span.ng-scope', 'Close')).click();

        await browser.wait(EC.invisibilityOf(element(by.id('timeBlockDetailsDialog'))), 5000, 'Appointment Information Pop Up failed to close and browser timed out');
        since('Appointment Information pop up did not close after clicking the Close button').expect(await (element(by.id('timeBlockDetailsDialog'))).isPresent()).toBeFalsy();
    }),

    it('Open Visit Doc from arrived appointment in Appointment Information pop up', async function () {
        await browser.wait(EC.elementToBeClickable($(commonSchedule.elements.arrivedAppointmentInformatinDayWeekViewClass)), 5000, 'Appointment is not clickable and browser timed out');
        await $(commonSchedule.elements.arrivedAppointmentInformatinDayWeekViewClass).click();

        await browser.wait(EC.elementToBeClickable(element(by.id('timeBlockDetailsDialog'))), 5000, 'Appointment Information Pop Up failed to display and browser timed out');
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText('span.ng-scope', 'Go to visit'))), 5000, 'Appointment Information Go to visit button can not be clicked and browser timed out');

        let appointmentDate = await $$("[class=\"ng-pristine ng-untouched ng-valid ng-scope md-input ng-not-empty ng-valid-required\"]").first().getAttribute("innerText");
        await element(by.cssContainingText('span.ng-scope', 'Go to visit')).click();
        await browser.wait(EC.elementToBeClickable(element.all(by.cssContainingText('span.ng-binding.ng-scope', appointmentDate)).first()), 5000, 'Vist Doc tab for ' + appointmentDate + ' did not display after clicking Go to visit from Appointment Pop Up');
        await expect(await element.all(by.cssContainingText('span.ng-binding.ng-scope', appointmentDate)).first().isDisplayed()).toBeTruthy();
    })
});