﻿'use strict';

const EC = protractor.ExpectedConditions;

require('jasmine2-custom-message');

module.exports = {
    texts: {
        signedOffNotification: 'Signed off',
        unsignedNotification: 'Unsigned'
    },

    elements: {
        signOffIcon: ".icon-edit",
        modalWindowSignOff: '[ng-form="signOffDialogForm"]',
		modalWindowUnsign: '[ng-form="unsignDialogForm"]',
        nextButton: '[ng-click="dialog.nextTab()"]',
        supervisor: '[name="cosign"]',
        cosignerListItem: 'md-select-menu md-option[value="85"]',
        passwordSignField: '[field-type="password"] input',
        signOffButton: '[ng-click="dialog.signOff()"]',
        notification: 'md-toast .clnt-toast-text',
        closeTabButton: '[ng-click="vm.removePatientTab( state.patient,$event )"]',
        patientSearchIcon: '.md-icon-button',
        selectedPatientRow: '.ui-grid-row:nth-child(1)',
        unsignIcon: '[aria-label="Unsign"]',
        reasonField: '[name="reason"] .md-input',
        passwordUnsignField: '[name="password"] .md-input',
        unsignButton: '[ng-click="dialog.unsign()"]',
		unsignedVisitsList: '.ui-grid-row > div > div',
		appointmentsTab: '[ui-view="secondary-bar"] md-tab-item:nth-child(3)',
		appointmentStatus: 'div:nth-child(2) > div'
    },

    waitAndClickNext: async function (button) {
        const modal = $(this.elements.modalWindowSignOff);
        const nextButton = modal.$(button);
        await browser.wait(EC.elementToBeClickable(nextButton), 5000, 'Next button is not clickable and browser timed out');
        await nextButton.click();
    }
}