﻿'use strict';

const EC = require('../expectedConditions');
const request = require("../request/index");

const commonFunctions = require('../common.js');
const commonSignedOff = require('./commonSignedOff.js');
const commonUnsignedVisits = require('../unsignedVisits/commonUnsignedVisits.js');
const commonPatientSearch = require('../patientSearch/commonPatientSearch.js');

const procedureDurationMins = 30;

let visitIdOfSelectedPatient;
let numberOfUnsignedVisitsOriginal;
let numberOfUnsignedVisitsChanged; 
let firstName;
let lastName;
let patientRow;

commonFunctions.xmlOutputLogger('signedOffWithoutCoSigner');

describe('signedOff without co-signer', function () {
	
	beforeAll(async function() {		
        let patient = await request.newPatient();
        firstName = patient.firstName;
        lastName = patient.lastName;
        
        let patientCase = await request.newCase({patientId: patient.patientId});
        
        let appointment = await request.newAppointment({
            case: patientCase,
            staffLogin: commonFunctions.login.therapistUnsignedVisitsUserName,
            procedures: [{
                cptCode: "97026",
                durationMins: procedureDurationMins
            }]
        });
        let appointmentId = appointment.appointmentId;
        
        let additionalPatient = await request.newPatient();
        let additionalPatientCase = await request.newCase({patientId: additionalPatient.patientId});
        let additionalAppointment = await request.newAppointment({
            case: additionalPatientCase,
            staffLogin: commonFunctions.login.therapistUnsignedVisitsUserName
        });
        
		patientRow = $(`[clnt-id="visit-${appointmentId}"]`);
	});

    it('Enter Schedule View to initial Unsigned Visit icon', async function () { 
        await commonFunctions.validLogin(commonFunctions.login.therapistUnsignedVisitsUserName);
         
        await browser.wait(EC.elementToBeClickable($(commonUnsignedVisits.elements.unsignedVisitIconLocationClass)), 5000, 'Failed to enter to Schedule view with Unsigned Visits icon visible and browser timed out');
        since("Unsigned Visits icon is not present in the Schedule view")
            .expect(await $(commonUnsignedVisits.elements.unsignedVisitIconLocationClass).isPresent())
            .toBeTruthy();
    });

    it('Open Unsigned Visits using Unsigned Visits icon from Schedule view', async function () { 
        await commonUnsignedVisits.enterUnsignedVisitsByIcon("Schedule view");

        await browser.wait(EC.elementToBeClickable($(commonUnsignedVisits.elements.unsignedVisitIconLocationClass)), 5000, 'Unsigned Visits icon is not clickable and browser timed out');
        numberOfUnsignedVisitsOriginal = await $(commonUnsignedVisits.elements.unsignedVisitIconLocationClass).getText();
    });
    
    it('Select patient from Unsigned Visits', async function () {
        await patientRow.click();
    });
	
    it('Click on the Sign Off icon', async function () {
        const signOffIcon = $(commonSignedOff.elements.signOffIcon);
        await browser.wait(EC.elementToBeClickable(signOffIcon), 5000, 'Sign Off button is not clickable and browser timed out');
        await signOffIcon.click();
    });

    it('Click on Next button to Faxing', async function () {
        await commonSignedOff.waitAndClickNext(commonSignedOff.elements.nextButton);
    });

    it('Click on Next button to Sing Off', async function () {
        await commonSignedOff.waitAndClickNext(commonSignedOff.elements.nextButton);    
    });
    
    it('Enter password to Sign off', async function () {
        const password = commonFunctions.login.therapistUserPassword;
        const passwordSignField = $(commonSignedOff.elements.modalWindowSignOff).$(commonSignedOff.elements.passwordSignField);
            
        await browser.wait(EC.elementToBeClickable(passwordSignField), 5000, 'Password is not clickable and browser timed out');
        await passwordSignField.click();
        await passwordSignField.sendKeys(password);
    });
    
    it('Click on SignOff button to Sign visit', async function () {
        await commonSignedOff.waitAndClickNext(commonSignedOff.elements.signOffButton);
    });
        
    it('Wait notification "Signed off"', async function () {
        const notification = $(commonSignedOff.elements.notification);

        await browser.waitForAngularEnabled(false);
        try {
            await browser.wait(EC.visibilityOf(notification), 5000, 'Notification didn\'t appear and browser timed out');
            const notificationText = await notification.getAttribute('innerText');
            since('Expecting notification text to be ' + commonSignedOff.texts.signedOffNotification + ', but displayed ' + notificationText).expect(commonSignedOff.texts.signedOffNotification).toBe(notificationText);
            await notification.click();
        }
        finally {
            await browser.waitForAngularEnabled(true);
        }
    });
       
    it('Open Unsigned Visits using Unsigned Visits icon from Visit doc', async function () {
        const unsignedVisitsIcon = $(commonUnsignedVisits.elements.unsignedVisitIconLocationClass);
        await browser.wait(EC.elementToBeClickable(unsignedVisitsIcon), 5000, 'Unsigned Visits icon is not clickable and browser timed out');
        await unsignedVisitsIcon.click();
     });

    it('Compare Original Unsigned Visits count with Changed (Changed = Original - 1)', async function () {
        await browser.wait(
            EC.textToEqual($(commonUnsignedVisits.elements.unsignedVisitIconLocationClass),
                numberOfUnsignedVisitsOriginal - 1),
            5000, `Unsigned visits icon shows wrong information. Expected value is ${numberOfUnsignedVisitsOriginal - 1}`);
        numberOfUnsignedVisitsChanged = await $(commonUnsignedVisits.elements.unsignedVisitIconLocationClass).getText();
    });

    it('Verify selected visit is not displayed', async function () { 
        await commonUnsignedVisits.enterUnsignedVisitsByIcon("Visit document");

        const unsignedVisitsList = commonSignedOff.elements.unsignedVisitsList;
        await browser.wait(EC.elementToBeClickable($(unsignedVisitsList)), 5000, 'Unsigned Visits List is not clickable and browser timed out');

        const rows = $$(unsignedVisitsList);
        const rowsCount = await rows.count();
        for (let i = 0; i < rowsCount; i ++) {
            since('Expecting visit ' + rows.get(i).getAttribute('clnt-id') + ' is not equal to ' + visitIdOfSelectedPatient).expect(await rows.get(i).getAttribute('clnt-id')).not.toEqual(visitIdOfSelectedPatient);
        }
    });
	
    it('Close patient tab', async function () {
        await browser.wait(EC.elementToBeClickable($(commonSignedOff.elements.closeTabButton)), 5000, 'Close patient tab button is not clickable and browser timed out');
        await $(commonSignedOff.elements.closeTabButton).click();
    });

    it('Click on Patient Search icon', async function () {
        await browser.wait(protractor.ExpectedConditions.elementToBeClickable($(commonSignedOff.elements.patientSearchIcon)), 5000, 'Patient search icon is not clickable and browser timed out');
        await $(commonSignedOff.elements.patientSearchIcon).click();
    });
     
    it('Search patient', async function () {
        await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).click();
        await $(commonPatientSearch.elements.firstNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).sendKeys(firstName);
        await $(commonPatientSearch.elements.lastNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).click();
        await $(commonPatientSearch.elements.lastNameParentLocation).$(commonPatientSearch.elements.nameChildLocation).sendKeys(lastName);

        await commonPatientSearch.searchButtonClick();

        await browser.wait(EC.elementToBeClickable($(commonSignedOff.elements.selectedPatientRow)), 5000, 'Selected patient is not clickable and browser timed out');
        await $(commonSignedOff.elements.selectedPatientRow).click();
    });
    
    it('Cross to Appointments tab', async function () {
        const appointmentsTab = commonSignedOff.elements.appointmentsTab;
        await browser.wait(EC.elementToBeClickable($(appointmentsTab)), 5000, 'Appointments tab is not clickable and browser timed out');
        await $(appointmentsTab).click();
    });
    
    it('Check status', async function () {
        let expectingStatus = 'Signed';
		let appointmentStatus = commonSignedOff.elements.appointmentStatus;
        since('Expecting status to be ' + expectingStatus + ', but displayed ' + await patientRow.$(appointmentStatus).getText())
		    .expect(await patientRow.$(appointmentStatus).getText())
            .toEqual(expectingStatus);
    });
    
    it('Select Signed visit', async function () {
        await browser.wait(EC.elementToBeClickable($(commonSignedOff.elements.selectedPatientRow)), 5000, 'Selected Signed visit is not clickable and browser timed out');
        await $(commonSignedOff.elements.selectedPatientRow).click();
    });
    
    it('Click Unsign icon', async function () {
        await browser.wait(EC.elementToBeClickable($(commonSignedOff.elements.unsignIcon)), 5000, 'Unsigned icon is not clickable and browser timed out');
        await $(commonSignedOff.elements.unsignIcon).click();
    });

    it('Enter reason and password to make signed visit -> unsigned', async function () {
        const modalWindowUnsign = $(commonSignedOff.elements.modalWindowUnsign);
        const reasonInput = modalWindowUnsign.$(commonSignedOff.elements.reasonField);
        const passwordInput = modalWindowUnsign.$(commonSignedOff.elements.passwordUnsignField);

        await reasonInput.click();
        await reasonInput.sendKeys('Because');
        await passwordInput.click();
        await passwordInput.sendKeys(commonFunctions.login.therapistUserPassword);

        const unsignButton = modalWindowUnsign.$(commonSignedOff.elements.unsignButton);
        await browser.wait(EC.elementToBeClickable(unsignButton), 5000, 'Unsign button is not clickable and browser timed out');
        await unsignButton.click();
    });

    it('Wait notification "Unsigned"', async function () {
        const notification = $(commonSignedOff.elements.notification);

        await browser.waitForAngularEnabled(false);
        try {
            await browser.wait(EC.visibilityOf(notification), 5000, 'Notification didn\'t appear and browser timed out');
            const notificationText = await notification.getAttribute('innerText');
            since('Expecting notification text to be ' + commonSignedOff.texts.unsignedNotification + ', but displayed ' + notificationText).expect(commonSignedOff.texts.unsignedNotification).toBe(notificationText);
            await notification.click();
        }
        finally {
            await browser.waitForAngularEnabled(true);
        }
    });

    it('Compare Original Unsigned Visits count with Changed (Changed = Original)', async function () {
        await browser.wait(
            EC.textToEqual($(commonUnsignedVisits.elements.unsignedVisitIconLocationClass),
                numberOfUnsignedVisitsOriginal),
            5000, `Unsigned visits icon shows wrong information. Expected value is ${numberOfUnsignedVisitsOriginal}`);
    });
});