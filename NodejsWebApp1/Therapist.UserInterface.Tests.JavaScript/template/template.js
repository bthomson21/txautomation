﻿'use strict';

const EC = require('../expectedConditions');

require('jasmine2-custom-message');

const performanceTesting = require('protractor-perf');
const commonFunctions = require('../common.js');
const commonVisitDoc = require('../visitDoc/commonVisitDoc.js');
const commonPatientSearch = require('../patientSearch/commonPatientSearch.js');
const commonPatientInformation = require('../patientInformation/commonPatientInformation.js');
const commonPatientCases = require('../patientCases/commonPatientCases.js');


commonFunctions.xmlOutputLogger('Template');

describe('Adding chart template to an existing area', function () {
    it('Enter Visit Doc from Patient Cases (TX-337)', async function () {
        await commonFunctions.validLogin();
        await commonPatientSearch.enterPatientSearchByMenu();
        await commonPatientInformation.enterPatientInformationFromPatientSearch(commonPatientCases.elements.patientFirstName);
       
        await commonPatientCases.enterPatientCasesFromPatientInformation();
        
        await commonPatientCases.selectLastCaseFirstArrivedVisit();
       
        await commonVisitDoc.clickTemplateCancel();
    }),
    it('Add a Template', async function () {
      
        await commonVisitDoc.addArea("Subjective");
        await commonVisitDoc.addTemplates();
        await commonVisitDoc.clickTreeContent("SmokeTest");
        await commonVisitDoc.clickClinicalContentCheckBox("1SubjectiveItem");
        await commonVisitDoc.clickApply();
        await browser.sleep(1000);

        await commonVisitDoc.verifyAreaExists("Subjective");
        await commonVisitDoc.expandArea("Subjective");
        await browser.wait(EC.visibilityOf(element(by.cssContainingText("label.ng-binding.flex-nogrow", "Verbal Pain Rating at Present"))), 5000, "waiting for template information to be added before I assert");
        await commonVisitDoc.verifyInfoTitle("Verbal Pain Rating at Present");
       
        await commonVisitDoc.removeArea("Subjective");
        await commonFunctions.clickSaveButton();

        await commonFunctions.clickSuccessfullySavedMessage();
        })
    //    it('Check Template Order', async function () {
    //      browser.sleep(10000);
    //    //logElement: await element(by.id("clnt-removing-areas-confirmation-dialog")).element(by.cssContainingText("span.ng-scope", "Yes"));

    //    //console.log("TIMER2 ?????????? " + logElement);

    //    //await commonVisitDoc.addArea("Subjective");
    //    await commonVisitDoc.addTemplates();
    //    await commonVisitDoc.clickTreeContent("SmokeTest");
    //    await commonVisitDoc.clickClinicalContentCheckBox("1SubjectiveItem");
    //    await commonVisitDoc.clickApply();
    //    await browser.wait(EC.visibilityOf(element(by.cssContainingText("label.ng-binding.flex-nogrow", "Verbal Pain Rating at Present"))), 5000, "waiting for template information to be added before I assert");
    //    await commonVisitDoc.verifyInfoTitleBefore("Patient goals with physical therapy include:", "Verbal Pain Rating at Present");
    //    await commonVisitDoc.removeArea("Subjective");
    //    await commonFunctions.clickSaveButton();
    //    await commonFunctions.clickSuccessfullySavedMessage();
    //})
    //it('Adding chart template to an existing area', async function () {
    //    await commonVisitDoc.addArea("Subjective");
    //    await commonVisitDoc.addTemplates();
    //    await commonVisitDoc.clickTreeContent("SmokeTest");
    //    await commonVisitDoc.clickClinicalContentCheckBox("1SubjectiveItem");
    //    await commonVisitDoc.clickApply();
    //    await commonVisitDoc.clickAreaItemDropDown("Subjective", "Patient goals with physical therapy include", true);
    //    await commonVisitDoc.verifyCheckBoxDropdown("alleviate pain.");      
    //    })
    });
