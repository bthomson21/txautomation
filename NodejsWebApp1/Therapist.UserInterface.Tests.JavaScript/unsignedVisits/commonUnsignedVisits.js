﻿'use strict';

const EC = require('../expectedConditions');

const commonSchedule = require('../schedule/commonSchedule.js');
const commonFunctions = require('../common.js');

module.exports = {

    elements: {
        unsignedVisitIconLocationClass: "[class=\"primary icon-lock-open-alt unsigned-visits md-button md-ink-ripple\"]",
        expectedUnsignedVisitsCount: 4
    },

    cosignAppointment: {
        time: "9/25/17 2:00 PM",
        patient: "t1, september",
        appointmentType: "Standard Visit - 60"
    },

    runnersClinic: {
        time: "8/25/17 8:00 AM",
        patient: "SUNNY, VODKA",
        appointmentType: "Runner's Clinic"
    },

    initialEval: {
        time: "8/22/17 8:15 AM", 
        patient: "MONGOLIAN, GRILL",
        appointmentType: "Initial Eval"
    },

    standardVisit: {
        time: "8/21/17 9:45 AM",
        patient: "BAILEY, ANDCREAM",
        appointmentType: "Standard Visit - 60"
    },
     
    enterUnsignedVisitsByMenu: async function () {
        await browser.wait(EC.elementToBeClickable($('label[for="navmain"]')), 5000, 'Failed to display Main menu and browser timed out');
        await $('label[for="navmain"]').click();
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(".toplevel", "Unsigned Visits"))), 5000, "Unsigned Visits menu option failed to be displayed and browser timed out");
        await element(by.cssContainingText(".toplevel", "Unsigned Visits")).click();
    },

    enterUnsignedVisitsByIcon: async function (unsignedVisitsIconClickedFromView) {
        await browser.wait(EC.elementToBeClickable($(this.elements.unsignedVisitIconLocationClass)), 5000, 'Unsigned Visits icon is not clickable and browser timed out');
        await $(this.elements.unsignedVisitIconLocationClass).click();

        await browser.wait(EC.elementToBeClickable($("span.clnt-label.ng-binding")), 5000, 'Unsigned Visits is not visible after clicking the Unsigned Visit icon from ' + unsignedVisitsIconClickedFromView + ' and browser timed out');
        since("Unsigned Visits did not display after click of Unsigned Visit icon from " + unsignedVisitsIconClickedFromView)
            .expect(await $("span.clnt-label.ng-binding").isPresent()).toBeTruthy();
    },

    verifyGridRowValue: async function (rowNumber, rowData) {
        let rowValue = await element.all(by.repeater(commonFunctions.elements.rowRepeater)).get(rowNumber).all(by.tagName('div')).first().getText();
        since("In Unsigned Visits, Time column, expecting " + rowValue.split(/[\n]/)[0] + ", but displayed " + rowData.time)
            .expect(rowValue.split(/[\n]/)[0]).toEqual(rowData.time);
        since("In Unsigned Visits, Patient column, expecting " + rowValue.split(/[\n]/)[1] + ", but displayed " + rowData.patient)
            .expect(rowValue.split(/[\n]/)[1]).toEqual(rowData.patient);
        since("In Unsigned Visits, Appointment Type column, expecting " + rowValue.split(/[\n]/)[2] + ", but displayed " + rowData.appointmentType)
            .expect(rowValue.split(/[\n]/)[2]).toEqual(rowData.appointmentType);
    }

}