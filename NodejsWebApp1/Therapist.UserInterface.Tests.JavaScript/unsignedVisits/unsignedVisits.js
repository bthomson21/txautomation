﻿'use strict';

const EC = require('../expectedConditions');

const commonFunctions = require('../common.js');
const commonUnsignedVisits = require('./commonUnsignedVisits.js');
const commonPatientSearch = require('../patientSearch/commonPatientSearch.js');
const commonPatientCases = require('../patientCases/commonPatientCases.js');
const commonCaseDetails = require('../caseDetails/commonCaseDetails.js');
const commonPatientInformation = require('../patientInformation/commonPatientInformation.js');
const commonPatientAppointments = require('../patientAppointments/commonPatientAppointments.js');
const commonVisitDoc = require('../visitDoc/commonVisitDoc.js');

commonFunctions.xmlOutputLogger('unsignedVisits');

describe('Unsigned Visits', function () {
    /*TODO:
    */

    it('Enter Schedule View to initial Unsigned Visit icon', async function () {
        await commonFunctions.validLogin(commonFunctions.login.therapistUnsignedVisitsUserName);
        await browser.wait(EC.elementToBeClickable($(commonUnsignedVisits.elements.unsignedVisitIconLocationClass)), 5000, 'Failed to enter to Schedule view with Unsigned Visits icon visible and browser timed out');
        since("Unsigned Visits icon is not present in the Schedule view").expect($(commonUnsignedVisits.elements.unsignedVisitIconLocationClass).isPresent()).toBeTruthy();
    }),

    it('Open Unsigned Visits using Unsigned Visits icon from Schedule view', async function () {
        await commonUnsignedVisits.enterUnsignedVisitsByIcon("Schedule view");
    }),

    it('Open Unsigned Visits using Unsigned Visits icon from Patient Search view', async function () {
        await commonPatientSearch.enterPatientSearchByMenu();
        await commonUnsignedVisits.enterUnsignedVisitsByIcon("Patient Search view");
    }),

    it('Open Unsigned Visits using Unsigned Visits icon from Patient Information view', async function () {
        await commonPatientSearch.enterPatientSearchByIcon();
        await commonPatientInformation.enterPatientInformationFromPatientSearch();
        await commonUnsignedVisits.enterUnsignedVisitsByIcon("Patient Information view");
    }),

    it('Open Unsigned Visits using Unsigned Visits icon from Patient Cases view', async function () {
        await commonPatientSearch.enterPatientSearchByIcon();
        await commonPatientInformation.enterPatientInformationFromPatientSearch();
        await commonPatientCases.enterPatientCasesFromPatientInformation();
        await commonUnsignedVisits.enterUnsignedVisitsByIcon("Patient Cases view");
        await commonPatientInformation.closePatientTab(0);  //TODO: Remove when TX-804 is complete
    }),

    it('Open Unsigned Visits using Unsigned Visits icon from Patient appointment view', async function () {  
        await commonPatientSearch.enterPatientSearchByIcon();
        await commonPatientInformation.enterPatientInformationFromPatientSearch();
        await commonPatientAppointments.enterPatientAppointmentsFromPatientInformation();
        await commonUnsignedVisits.enterUnsignedVisitsByIcon("Patient appointment view");
        await commonPatientInformation.closePatientTab(0);  //TODO: Remove when TX-804 is complete
    }),

    it('Open Unsigned Visits using Unsigned Visits icon from Case Details view', async function () {  
        await commonPatientSearch.enterPatientSearchByIcon();
        await commonPatientInformation.enterPatientInformationFromPatientSearch(commonPatientCases.elements.patientFirstName);
        await commonPatientCases.enterPatientCasesFromPatientInformation();
        await commonPatientCases.selectLastCaseFirstArrivedVisit();
        await commonVisitDoc.clickTemplateCancel();
        await commonCaseDetails.enterCaseDetailsFromVisitDoc();
        await commonUnsignedVisits.enterUnsignedVisitsByIcon("Case Details view");
        await commonPatientInformation.closePatientTab(0);  //TODO: Remove when TX-804 is complete
    }),

    it('Open Unsigned Visits using Unsigned Visits icon from Visit Doc view', async function () {     
        await commonPatientSearch.enterPatientSearchByIcon();
        await commonPatientInformation.enterPatientInformationFromPatientSearch(commonPatientCases.elements.patientFirstName);
        await commonPatientCases.enterPatientCasesFromPatientInformation();
        await commonPatientCases.selectLastCaseFirstArrivedVisit();
        await commonVisitDoc.clickTemplateCancel();
        await commonUnsignedVisits.enterUnsignedVisitsByIcon("Visit Doc view");        
    }),

    it('Verify grid content', async function () {
        await commonUnsignedVisits.enterUnsignedVisitsByIcon();

        let rowCount = await element.all(by.repeater(commonFunctions.elements.rowRepeater)).count();
        since("Expecting " + commonUnsignedVisits.elements.expectedUnsignedVisitsCount + " Unsigned Visits, but " + rowCount + " displayed").expect(rowCount).toEqual(commonUnsignedVisits.elements.expectedUnsignedVisitsCount)
        
        let numUnsignedVisitsLabel = await $('span.clnt-label.ng-binding').getAttribute('innerText');
        since("Expecting label to display " + (commonUnsignedVisits.elements.expectedUnsignedVisitsCount).toString() + " Unsigned visits, but instead displayed " + numUnsignedVisitsLabel).expect(commonUnsignedVisits.elements.expectedUnsignedVisitsCount + " Unsigned visits").toEqual(numUnsignedVisitsLabel)
        
        await commonUnsignedVisits.verifyGridRowValue(0, commonUnsignedVisits.cosignAppointment);
        await commonUnsignedVisits.verifyGridRowValue(1, commonUnsignedVisits.runnersClinic);
        await commonUnsignedVisits.verifyGridRowValue(2, commonUnsignedVisits.initialEval);
        await commonUnsignedVisits.verifyGridRowValue(3, commonUnsignedVisits.standardVisit);
    }),

    it('Verify grid sort', async function () {
        /*TODO: Enable sort by Time/Date
        */
    
        await commonUnsignedVisits.enterUnsignedVisitsByMenu();

        await element(by.cssContainingText('span.ng-binding', 'Patient')).click();
        await browser.wait(EC.visibilityOf($('span.clnt-label.ng-binding')), 5000, 'Failed to sort by Patient ASC and browser timed out');
        await commonFunctions.verifyGridSort(commonFunctions.elements.rowRepeater, commonFunctions.elements.columnRepeater, 1, true);
        await element(by.cssContainingText('span.ng-binding', 'Patient')).click();
        await browser.wait(EC.visibilityOf($('span.clnt-label.ng-binding')), 5000, 'Failed to sort by Patient DESC and browser timed out');
        await commonFunctions.verifyGridSort(commonFunctions.elements.rowRepeater, commonFunctions.elements.columnRepeater, 1, false);

        await element(by.cssContainingText("span.ng-binding", "Appointment Type")).click();
        await browser.wait(EC.visibilityOf($('span.clnt-label.ng-binding')), 5000, 'Failed to sort by Appointment Type ASC and browser timed out');
        await commonFunctions.verifyGridSort(commonFunctions.elements.rowRepeater, commonFunctions.elements.columnRepeater, 2, true); 
        await element(by.cssContainingText('span.ng-binding', 'Appointment Type')).click();
        await browser.wait(EC.visibilityOf($('span.clnt-label.ng-binding')), 5000, 'Failed to sort by Appointment Type DESC and browser timed out');
        await commonFunctions.verifyGridSort(commonFunctions.elements.rowRepeater, commonFunctions.elements.columnRepeater, 2, false);  
    }),

    it('Open Unsigned visits from Unsigned visit view', async function () {
        await commonUnsignedVisits.enterUnsignedVisitsByIcon();

        await element(by.cssContainingText('.ui-grid-cell-contents.ng-binding.ng-scope', commonUnsignedVisits.runnersClinic.patient)).click();
        await browser.wait(EC.presenceOf($(commonFunctions.elements.signOffButtonLocation)), 5000, 'Visit doc was not displayed and browser timed out');

        since("Visit doc was not displayed after clicking on " + commonUnsignedVisits.runnersClinic.patient + " in the Unsigned visit grid").expect($(commonFunctions.elements.signOffButtonLocation).isPresent()).toBeTruthy();
    })
 })