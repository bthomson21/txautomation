﻿'use strict'
const commonFunctions = require('../common.js');
const EC = require('../expectedConditions');

module.exports = {

    elements: {
        vistiTabCloseXLocation: "[ng-click=\"caseDetailsBar.removeVisitNotesTab(visitNotes, $event)\"]",
        addAreaIconClass: "icon-plus ng-scope material-icons",
        dialogContentClass: "[class=\"ng-binding ng-scope\"]",
        rgbaOrange: "rgba(255, 133, 0, 1)",
        rbgaBlack: "rgba(38, 55, 70, 1)"
    },

    returnToPatientInformationFromVisitDoc: async function () {
        await $('md-icon.ng-scope.md-font.material-icons.icon.icon-reply').click();
        await browser.wait(EC.presenceOf($('button.primary.md-button.md-ink-ripple')), 5000, 'Failed to return to Patient Information from Visit Doc and browser timed out');
    },

    closeVisitTab: async function (tabIndex) {
        await $$(this.elements.vistiTabCloseXLocation).get(tabIndex).click();
    },

    searchClinicalContent: async function (searchItem) {
        await browser.wait(EC.elementToBeClickable(element.all(by.cssContainingText("span.ng-scope", "Search")).first()), 5000, "Can't click on the Search tab in the Clinical Content dialog and browser timed out");
        await element.all(by.cssContainingText("span.ng-scope", "Search")).first().click();
        await browser.wait(EC.visibilityOf(element(by.cssContainingText("label.md-no-float.ng-binding", "Search by Name"))), 5000, "Clicking on the Search tab did not display the Search view and browser timed out");

        await element(by.css("[field-label=\"Search by Name\"]")).element(by.css("[type=\"text\"]")).click();
        await element(by.css("[field-label=\"Search by Name\"]")).element(by.css("[type=\"text\"]")).sendKeys(searchItem)

        await browser.wait(EC.elementToBeClickable(element(by.id("addChartItemsDialog")).element(by.css("[ng-model=\"dialog.chartItemIdsMap[item.itemMetadataId]\"]"))), 5000, searchItem + " checkbox can not be clicked and browser timed out");
        await element(by.id("addChartItemsDialog")).element(by.css("[ng-model=\"dialog.chartItemIdsMap[item.itemMetadataId]\"]")).click();

        await this.clickApply();
    },

    addArea: async function (area) {
        await element(by.className(this.elements.addAreaIconClass)).click();

        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText("span.ng-binding.ng-scope", area))), 5000, "Clicking on the Add Area Plus icon did not open the ADD CHART AREA dialog and browser timed out");
        await element(by.cssContainingText("span.ng-binding.ng-scope", area)).click();
        await this.clickApply();
        await this.verifyAreaExists(area);

        await browser.wait(EC.elementToBeClickable(element(by.css("[heading=\"" + area + "\"]")).element(by.css("clnt-summary-item"))), 5000, "Expanding " + area + " area did not display the Summary item and browser timed out");
        await expect(element(by.css("[heading=\"" + area + "\"]")).element(by.css("clnt-summary-item")).isDisplayed()).toBeTruthy();
    },

    removeArea: async function (area) {
        await element(by.className(this.elements.addAreaIconClass)).click();

        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText("span.ng-binding.ng-scope", area))), 5000, "Clicking on the Add Area Plus icon did not open the ADD CHART AREA dialog and browser timed out");
        await element(by.cssContainingText("span.ng-binding.ng-scope", area)).click();
        await this.clickApply();
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText("span.ng-scope", "Yes"))), 5000, "Unchecking " + area + "did not display Confirm Areas to Remove dialog and browser timed out");
        await element(by.cssContainingText("span.ng-scope", "Yes")).click();
    },

    verifyAreaExists: async function (area) {
        await browser.wait(EC.visibilityOf(element(by.cssContainingText("span.ng-binding", area))), 5000, "Clicking Apply did not add the " + area + " area and browser timed out");
        since(area + " area was not added to the Visit doc").expect(element(by.cssContainingText("span.ng-binding", area)).isPresent()).toBeTruthy();
    },

    verifyAreaDoesNotExists: async function (area) {
        await browser.wait(EC.invisibilityOf(element(by.cssContainingText("span.ng-binding", area))), 5000, "Waiting for " + area + " area to be removed and browser timed out");
        since(area + " area was not removed from the Visit doc").expect(element(by.cssContainingText("span.ng-binding", area)).isPresent()).toBeFalsy();
    },

    expandArea: async function (area) {   
        await browser.wait(EC.elementToBeClickable(element(by.css("[heading=\"" + area + "\"]")).element(by.css("[ng-show=\"collapsed\"]"))), 5000, "Expand icon for " + area + " area is not clickable and browser timed out");
        await element(by.css("[heading=\"" + area + "\"]")).element(by.css("[ng-show=\"collapsed\"]")).click();
        await browser.wait(EC.elementToBeClickable(element(by.css("[heading=\"" + area + "\"]")).all(by.css("[ng-show=\"!collapsed\"]")).get(0)), 5000, "After clicking expand icon for " + area + " area, the collapse icon did not appear and browser timed out");
        since(area + " did not expand after clicking the expand icon").expect(element(by.css("[heading=\"" + area + "\"]")).all(by.css("[ng-show=\"!collapsed\"]")).get(0).isDisplayed()).toBeTruthy();
    },

    expandAllArea: async function () {
        await browser.wait(EC.elementToBeClickable(element(by.css("[class=\"icon-resize-full ng-scope material-icons\"]"))), 5000, "Expand All icon is not clickable and browser timed out");
        await element(by.css("[class=\"icon-resize-full ng-scope material-icons\"]")).click();
        await browser.wait(EC.elementToBeClickable(element(by.css("[class=\"icon-resize-normal ng-scope material-icons\"]"))), 5000, "After clicking Expand All icon the Collapse All icon did not appear and browser timed out");
        since("Collapse All icon is not visible after clicking Expand All icon").expect(element(by.css("[class=\"icon-resize-normal ng-scope material-icons\"]")).isDisplayed()).toBeTruthy();
    },

    collapseArea: async function (area) {
        await browser.wait(EC.elementToBeClickable(element(by.css("[heading=\"" + area + "\"]")).element(by.css("[ng-show=\"!collapsed\"]"))), 5000, "Collapse icon for " + area + " area is not clickable and browser timed out");
        await element(by.css("[heading=\"" + area + "\"]")).element(by.css("[ng-show=\"!collapsed\"]")).click();
        await browser.wait(EC.elementToBeClickable(element(by.css("[heading=\"" + area + "\"]")).all(by.css("[ng-show=\"collapsed\"]")).get(0)), 5000, "After clicking collapse icon for " + area + " area, the expand icon did not appear and browser timed out");
        since(area + " did not collapse after clicking the collapse icon").expect(element(by.css("[heading=\"" + area + "\"]")).all(by.css("[ng-show=\"collapsed\"]")).get(0).isDisplayed()).toBeTruthy();
    },

    collapsedAllArea: async function () {
        await browser.wait(EC.elementToBeClickable(element(by.css("[class=\"icon-resize-normal ng-scope material-icons\"]"))), 5000, "After clicking Expand All icon the Collapse All icon did not appear and browser timed out");
        await element(by.css("[class=\"icon-resize-normal ng-scope material-icons\"]")).click();
        await browser.wait(EC.elementToBeClickable(element(by.css("[class=\"icon-resize-full ng-scope material-icons\"]"))), 5000, "Expand All icon is not clickable and browser timed out");
        since("Exapnd All icon is not visible after clicking Collapse All icon").expect(element(by.css("[class=\"icon-resize-full ng-scope material-icons\"]")).isDisplayed()).toBeTruthy();
    },

    addSummaryItemText: async function (area, text) {
        await element(by.css("[heading=\"" + area + "\"]")).element(by.css("clnt-summary-item")).click();
        await element(by.css("[heading=\"" + area + "\"]")).element(by.css("textarea")).sendKeys(text)
    },

    addChartItems: async function (area) {
        await browser.wait(EC.elementToBeClickable(element(by.css("[heading=\"" + area + "\"]")).element(by.cssContainingText("span.ng-scope", "Add"))), 5000, "ADD icon for " + area + " area is not clickable and browser timed out");
        await element(by.css("[heading=\"" + area + "\"]")).element(by.cssContainingText("span.ng-scope", "Add")).click();
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText("span.ng-scope", "Cancel"))), 5000, "Clicking ADD did not display the ADD CLINICAL CONTENT dialog and browser timed out");
    },

    addTemplates: async function () {
        await browser.wait(EC.elementToBeClickable(element(by.css("[class=\"icon-doc-text ng-scope material-icons\"]"))), 5000, "Add Template icon is not clickable and browser timed out");
        await element(by.css("[class=\"icon-doc-text ng-scope material-icons\"]")).click();
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText("span.ng-scope", "Cancel"))), 5000, "Clicking Add Template icon did not display the Pick a Template dialog and browser timed out");
    },

    clickTemplateCancel: async function () {
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText("span.ng-scope", "Cancel"))), 5000, "Unable to click Cancel in the Pick a Template dialog and the browser timed out");
        await element(by.cssContainingText("span.ng-scope", "Cancel")).click();
    },

    clickTreeContent: async function (item) {
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText("clnt-category.ng-binding.ng-scope", item))), 5000, "Unable to click Cancel in the Pick a Template dialog and the browser timed out");

        let topicTreeItems = await element.all(by.repeater("category in d.categories track by d.getCategoryId(category)")).getAttribute("innerText");

        topicTreeItems.forEach(async function (individualItem) {
                if (item.startsWith(individualItem) && item.endsWith(individualItem)) {
                    let topicTreeItemIndex = topicTreeItems.indexOf(item);
                    await element.all(by.repeater("category in d.categories track by d.getCategoryId(category)")).get(topicTreeItemIndex).click();
                }
        });
        browser.ignoreSynchronization = true;
    },

    clickClinicalContentCheckBox: async function (item) {
        await browser.wait(EC.elementToBeClickable(element.all(by.css("[class=\"md-container md-ink-ripple\"]")).get(0)), 5000, item  + " item is not available to click in the topic tree and browser timed out");
        let topicTreeCheckboxItems = await element.all(by.repeater("item in d.items track by d.getItemId(item)")).getAttribute("innerText")
            for (let topicTreeCheckboxItemIndex in topicTreeCheckboxItems) {
                let individualCheckboxItem = topicTreeCheckboxItems[topicTreeCheckboxItemIndex].split(/[\n]/)[0]

                if (topicTreeCheckboxItems.hasOwnProperty(topicTreeCheckboxItemIndex)) {
                    if (item.startsWith(individualCheckboxItem) && item.endsWith(individualCheckboxItem)) {
                        await element.all(by.repeater("item in d.items track by d.getItemId(item)")).get(topicTreeCheckboxItemIndex).element(by.css("md-checkbox")).click();
                    }
                }
            } 
    },

    clickApply: async function () {
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText("span.ng-scope", "Apply"))), 5000, "Apply button is not clickable and browser timed out");
        let disabledAttribute =  await element(by.cssContainingText("span.ng-scope", "Apply")).getAttribute("disabled")
            if (disabledAttribute == null) {
                await element(by.cssContainingText("span.ng-scope", "Apply")).click();
            }
        await browser.wait(EC.invisibilityOf(element(by.cssContainingText("span.ng-scope", "Apply"))), 5000, "After clicking Apply, the dialog was not closed and browser timed out");
    },

    clickAreaItemDropDown: async function (area, areaItem, isMultiSelect) {
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText("clnt-visit-area", area)).element(by.cssContainingText("[ng-model=\"d.chartItem\"]", areaItem)).element(by.css("[aria-multiselectable=\"" + isMultiSelect + "\"]"))), 5000, area + "->" + areaItem + " drop down menu is not clickable and browser timed out");
        await element(by.cssContainingText("clnt-visit-area", area)).element(by.cssContainingText("[ng-model=\"d.chartItem\"]", areaItem)).element(by.css("[aria-multiselectable=\"" + isMultiSelect + "\"]")).click();
    },

    clickItemSelectOptionsDropDown: async function (areaItem) {
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(commonFunctions.elements.ngRepeater, areaItem))), 5000, areaItem + " item was not clickable in the Select Options drop down and browser timed out");
        await element(by.cssContainingText(commonFunctions.elements.ngRepeater, areaItem)).click();
    },

    validateAreaRemoved: async function (area) {
        await browser.wait(EC.invisibilityOf(element(by.cssContainingText("span.ng-binding", area))), 5000, "After Save, the empty " + area + " Area was not removed and browser timed out");
        since("After Save, the empty " + area + " Area was not removed").expect(browser.isElementPresent(element(by.cssContainingText("span.ng-binding", area)))).toBeFalsy();
    },

    verifyAreaItemAdded: async function (areaItem) {
        await browser.wait(EC.presenceOf(element(by.cssContainingText("[class=\"ng-binding flex-nogrow\"]", areaItem))), 5000, "Area item " + areaItem + " was not added and browser timed out");
        since("After clicking Apply, the area item " + areaItem + " was not added").expect(browser.isElementPresent(element(by.cssContainingText("[class=\"ng-binding flex-nogrow\"]", areaItem)))).toBeTruthy();
    },

    validateSelectOptionsDropDownResultsText: async function (area, areaItem, expectedResultText) {
        let actualResultText = await element(by.cssContainingText("clnt-visit-area", area)).element(by.cssContainingText("[ng-model=\"d.chartItem\"]", areaItem)).element(by.css("[ng-if=\"d.getFieldType() === 'textarea'\"]")).getAttribute('value')
        since("Expecting [" + expectedResultText + "] Actual [" + actualResultText + "]").expect(actualResultText).toEqual(expectedResultText);
    },

    clickCheckBox: async function (area, areaItem, isChecked) {
        await browser.wait(EC.presenceOf(element(by.cssContainingText("clnt-visit-area", area)).element(by.cssContainingText("[ng-model=\"d.chartItem\"]", areaItem)).element(by.css("[type=\"checkbox\"]"))), 5000, areaItem + " checkbox is not clickable and browser timed out");
        await element(by.cssContainingText("clnt-visit-area", area)).element(by.cssContainingText("[ng-model=\"d.chartItem\"]", areaItem)).element(by.css("[type=\"checkbox\"]")).click();
        this.verifyCheckBox(area, areaItem, isChecked)
    },

    verifyCheckBox: async function (area, areaItem, isChecked) {
        let checked = await element(by.cssContainingText("clnt-visit-area", area)).element(by.cssContainingText("[ng-model=\"d.chartItem\"]", areaItem)).element(by.css("[type=\"checkbox\"]")).getAttribute("aria-checked");
        since("Expecting " + area + "->" + areaItem + " checkbox to be " + isChecked + ", but actually " + checked).expect(isChecked.toString()).toBe(checked.toString())
    },

    verifyInfoTitle: async function (areaItem) {
        let areaIteamText = await element(by.cssContainingText("label.ng-binding.flex-nogrow", areaItem)).getAttribute("textContent");
        since("Expecting [" + areaItem + "] " + "Actual [" + areaIteamText + "]").expect("Actual [" + areaIteamText.toString() + " ]").toBe("Actual [" + areaItem + " ]");
    },
    verifyInfoTitleBefore: async function (before, after) {
        let itemPosition = await element.all(by.css("label.ng-binding.flex-nogrow")).then(function (items) {
            expect(items[0].getAttribute("textContent")).toBe(before);
            expect(items[1].getAttribute("textContent")).toBe(after);
        });
    },
    verifyCheckBoxDropdown: async function (areaItem) {
        await browser.wait(EC.elementToBeClickable(element(by.cssContainingText(commonFunctions.elements.ngRepeater, areaItem))), 5000, areaItem + " item was not clickable in the Select Options drop down and browser timed out");
        let areaIteamText = await element(by.cssContainingText(commonFunctions.elements.ngRepeater, areaItem)).getAttribute("textContent");
        since("Expecting " + areaItem + " ]").expect("Actual [ " + areaIteamText + " ] ").toBe("Actual [ " + areaItem + " ] ");
    },
    verifyFontColor: async function (area, areaItem, rgbColor) {
        let color = await element(by.cssContainingText("clnt-visit-area", area)).element(by.cssContainingText("[ng-model=\"d.chartItem\"]", areaItem)).all(by.css("[clnt-view-value-change=\"d.updateModel($viewValue)\"]")).first().getCssValue('color');
        since("Expecting " + area + "->" + areaItem + " color to be [" + rgbColor + "], but instead is [" + color + "]").expect(color).toEqual(rgbColor);
    }
};