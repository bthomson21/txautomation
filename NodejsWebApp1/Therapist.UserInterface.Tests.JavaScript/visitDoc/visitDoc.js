﻿'use strict';

const EC = require('../expectedConditions');

require('jasmine2-custom-message');

const performanceTesting = require('protractor-perf');
const commonFunctions = require('../common.js');
const commonVisitDoc = require('./commonVisitDoc.js');
const commonPatientSearch = require('../patientSearch/commonPatientSearch.js');
const commonPatientInformation = require('../patientInformation/commonPatientInformation.js');
const commonPatientCases = require('../patientCases/commonPatientCases.js');
const commonCaseDetails = require('../caseDetails/commonCaseDetails.js');

commonFunctions.xmlOutputLogger('visitDoc');

describe('Visit Doc', function () {
    it('Enter Visit Doc from Patient Cases (TX-337)', async function () {
        await commonFunctions.validLogin();
        await commonPatientSearch.enterPatientSearchByMenu();
        await commonPatientInformation.enterPatientInformationFromPatientSearch(commonPatientCases.elements.patientFirstName);
        await commonPatientCases.enterPatientCasesFromPatientInformation();
        await commonPatientCases.selectLastCaseFirstArrivedVisit();
        await commonVisitDoc.clickTemplateCancel();
    }),

    it("Add Procedure and PQRS", async function () {
        await commonVisitDoc.addArea("Procedures");
        await commonVisitDoc.addChartItems("Procedures");
        await commonVisitDoc.searchClinicalContent("OT Evaluation, Low Complexity");

        await commonVisitDoc.verifyAreaItemAdded("OT Evaluation, Low Complexity");

        await commonVisitDoc.verifyCheckBox("Procedures", "OT Evaluation, Low Complexity", true);
        await commonVisitDoc.verifyFontColor("Procedures", "OT Evaluation, Low Complexity", commonVisitDoc.elements.rbgaBlack)

        await commonFunctions.clickSaveButton();
        await commonVisitDoc.verifyAreaExists("Quality Measures");


        await commonVisitDoc.expandAllArea();
        await commonVisitDoc.clickAreaItemDropDown("Quality Measures", "Pain Assessment and Follow-Up", false);

        since("Quality Measures->Pain Assessment and Follow-Up drop down does not contain any items").expect(await element(by.cssContainingText("div.md-text.ng-binding", "Pain assessment documented as negative, no follow-up plan required")).isPresent()).toBeTruthy();
        await commonFunctions.sendEscapeKey();

        await commonVisitDoc.clickCheckBox("Procedures", "OT Evaluation, Low Complexity", false)
        await commonVisitDoc.verifyFontColor("Procedures", "OT Evaluation, Low Complexity", commonVisitDoc.elements.rgbaOrange)

        await commonVisitDoc.removeArea("Procedures")
        await commonFunctions.clickSaveButton();
        await commonVisitDoc.verifyAreaDoesNotExists("Quality Measures")

    }, 200000),

    it("Add Subjective, add area items with special characters, remove Area items and empty Area is removed, validate font color", async function () {
        await commonVisitDoc.addArea("Subjective");
        await commonVisitDoc.addChartItems("Subjective");
        await commonVisitDoc.clickTreeContent("Medical History");
        await commonVisitDoc.clickTreeContent("Fluid Intake");
        await commonVisitDoc.clickClinicalContentCheckBox("Beer");
        await commonVisitDoc.clickApply();
        await commonVisitDoc.verifyAreaItemAdded("Beer");

        await commonVisitDoc.verifyCheckBox("Subjective", "Beer", false);

        await commonVisitDoc.clickAreaItemDropDown("Subjective", "Beer", true)
        await commonVisitDoc.clickItemSelectOptionsDropDown("2-3 Daily");
        await commonVisitDoc.clickItemSelectOptionsDropDown("In*f/re@q/ue\\nt");
        await commonVisitDoc.clickItemSelectOptionsDropDown("1-2 Daily");
        await commonFunctions.sendEscapeKey();
        await commonVisitDoc.verifyFontColor("Subjective", "Beer", commonVisitDoc.elements.rbgaBlack)

        await commonVisitDoc.validateSelectOptionsDropDownResultsText("Subjective", "Beer", "2-3 Daily. In*f/re@q/ue\\nt. 1-2 Daily.");

        await commonVisitDoc.clickAreaItemDropDown("Subjective", "Beer", true)
        await commonVisitDoc.clickItemSelectOptionsDropDown("2-3 Daily");
        await commonFunctions.sendEscapeKey();
        await commonVisitDoc.validateSelectOptionsDropDownResultsText("Subjective", "Beer", "In*f/re@q/ue\\nt. 1-2 Daily.");

        await commonVisitDoc.clickCheckBox("Subjective", "Beer", false);
        await commonVisitDoc.verifyFontColor("Subjective", "Beer", commonVisitDoc.elements.rgbaOrange)

        await commonVisitDoc.addChartItems("Subjective");
        await commonVisitDoc.clickTreeContent("Medical History");
        await commonVisitDoc.clickTreeContent("Fluid Intake");
        await commonVisitDoc.clickClinicalContentCheckBox("Beer");
        await commonVisitDoc.clickApply();
        await commonFunctions.clickSaveButton();
        await commonFunctions.clickSuccessfullySavedMessage();

    }, 200000),

    it('Open and close multiple Visit Docs.  (TX-353, TX-354)', async function () {
        await commonPatientCases.enterPatientInformationWithReturnIcon();
        await commonPatientCases.enterPatientCasesFromPatientInformation();

        await element.all(by.cssContainingText(commonPatientCases.elements.visitLocation, commonPatientCases.elements.arrivedNextVisit)).first().click();
        await commonVisitDoc.clickTemplateCancel();
        await browser.wait(EC.elementToBeClickable(element.all(by.cssContainingText(commonPatientCases.elements.visitLocation, '5/5/2020' )).first()), 5000, 'Clicking on the First Visit Doc did not create a Visit doc Tab and browser timed out');
        since('First Visit doc did not create a Visit Doc Tab').expect(element.all(by.cssContainingText(commonPatientCases.elements.visitLocation, '5/5/2020' )).first().isDisplayed()).toBeTruthy();

        await commonPatientCases.enterPatientInformationWithReturnIcon();
        await commonPatientInformation.enterPatientCasesFromPatientInformation();
        await element(by.cssContainingText(commonPatientCases.elements.visitLocation, commonPatientCases.elements.lastCaseUnsignedVisitFirst)).click();
        await commonVisitDoc.clickTemplateCancel();
        await browser.wait(EC.elementToBeClickable(element.all(by.cssContainingText(commonPatientCases.elements.visitLocation, '11/1/2017')).first()), 5000, 'Clicking on the second Visit Doc did not create a Visit doc Tab and browser timed out');
        since('Second Visit doc did not create a Visit Doc Tab').expect(element.all(by.cssContainingText(commonPatientCases.elements.visitLocation, '11/1/2017')).first().isDisplayed()).toBeTruthy();

        await commonVisitDoc.closeVisitTab(0);
        await commonVisitDoc.closeVisitTab(0);

        let isDetailsTabActive = await $$('md-tab-item.md-tab.ng-scope.ng-isolate-scope.md-ink-ripple').get(1).getAttribute('aria-selected');
        since('Expecting Details tab active attribute to be ' + isDetailsTabActive + ' after closing all visit tabs').expect(isDetailsTabActive).toBeTruthy();
    })

});

